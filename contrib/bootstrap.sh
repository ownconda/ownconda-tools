#!/bin/bash
#
# Bootstrap the ownconda stack inside the current working directory.
#
# This scripts builds all Conda packages for the ownconda tools, conda-build
# and their dependencies and uploads them into the "stable" channel.
#
# With these packages, you can create an ownconda distribution and the Docker
# images.

uid=$(id -u)
gid=$(id -g)

docker run --rm -i -v ~/Projects/ownconda:/ownconda centos:centos7 /bin/bash -s << EOF
# Install build dependencies
yum install -y deltarpm epel-release wget bzip2 sudo
yum upgrade -y
yum groupinstall -y 'Development Tools'
yum install -y centos-release-scl
yum-config-manager --enable rhel-server-rhscl-7-rpms
yum install -y devtoolset-7
yum install -y rh-perl526
source /opt/rh/devtoolset-7/enable

# Install miniconda, conda-build and (via pip) ownconda-tools
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh -O /tmp/miniconda.sh
bash /tmp/miniconda.sh -p /tmp/mc -b
source /tmp/mc/bin/activate
conda install --yes 'conda-build<3.14'
cd /ownconda
pip install ./ownconda-tools

# Configure miniconda
cat <<IEOF >/tmp/mc/.condarc
channels:
  - file:///tmp/mc/conda-bld
default_channels:
  - file:///tmp/mc/conda-bld
add_pip_as_python_dependency: False
anaconda_upload: False
conda-build:
  filename_hashing: false
IEOF

# Build a list with all packages we need to build
ownconda dep-graph --out=txt --print-path --implicit --requirements conda-build external-recipes > build.txt
ownconda dep-graph --out=txt --print-path --implicit --requirements own-conda-tools external-recipes ownconda-tools >> build.txt
sort -u build.txt > build.txt.sorted
mv -f build.txt.sorted build.txt

# Build packages and make resulting artifacts be owned by the current user
cat build.txt | xargs ownconda build --quiet
rm build.txt
chown -R $uid:$gid artifacts

# Upload packages
ownconda upload
EOF
