import time

from gitlab.exceptions import GitlabCreateError
from own_conda_tools.click_util import Action


def task(project):
    branches = {b.name for b in project.branches.list()}
    assert 'master' in branches

    with Action('Running pipeline for "master"') as action:
        pipeline = project.pipelines.create({'ref': 'master'})
        while True:
            time.sleep(15)
            action.progress()
            pipeline.refresh()
            if pipeline.finished_at is not None:
                if pipeline.status != 'success':
                    raise RuntimeError('Pipeline failed')
                break
