#!/bin/env python
#
# Clone all listed projects and run their pipeline.
#
# By default, the script when a pipeline fails.  Pass "-i" to ignore errors
# and run all pipelines.
import functools
import json
import os
import pathlib
import shutil
import subprocess
import sys

import click


ROOT_DIR = pathlib.Path.home() /'Projects' / 'gitlab-testing'
CONDA_ROOT = ROOT_DIR / '.conda'
INSTALLER_URL = 'https://forge.services.own/conda/stable/ownconda-dev.sh'
INSTALLER_LOC = ROOT_DIR / 'ownconda-dev.sh'
PROJECTS_PREFIX = ROOT_DIR
CONDA_CHANNELS = 'staging'
# CONDA_CHANNELS = 'testing,staging'
# CONDA_CHANNELS = 'experimental,staging'

# You can get a list of projects via "ownconda gitlab print-project"
PROJECTS = """
    ownconda/ownconda-tools
""".strip().splitlines()
PROJECTS = [p for p in (row.partition('#')[0].strip() for row in PROJECTS) if p]


@click.command()
@click.option(
    '--ignore-errors',
    '-i',
    is_flag=True,
    default=False,
    help='Do not exit when an error occurs.  Print the number of errors at the end.',
)
@click.option(
    '--skip-setup',
    '-s',
    is_flag=True,
    default=False,
    help='Skip clean setup and use existing Conda env',
)
@click.option(
    '--branch',
    '-b',
    default='develop',
    show_default=True,
    help='Git branch to check out',
)
@click.option(
    '--ci-opts',
    default='--quiet --with-tests',
    show_default=True,
)
@click.option(
    '--channels',
    '-c',
    default=CONDA_CHANNELS,
    show_default=True,
    help='Additional Conda channels to use',
)
def cli(ignore_errors, skip_setup, branch, ci_opts, channels):
    """Clone all listed projects and run their pipeline."""
    # Delete CONDA_ variables and update PATH
    env = getenv(channels)
    if not skip_setup:
        setup(env)

    run = functools.partial(subprocess.run, env=env, shell=True)

    conda_rev = json.loads(
        run('conda list --revisions --json', capture_output=True, text=True).stdout
    )[-1]['rev']

    failed_pipelines = []
    for project in PROJECTS:
        # Run pipeline
        rc = run(
            f'ownconda ci {ci_opts} '
            f'--git-branch={branch} --git-update {project} {PROJECTS_PREFIX}'
        ).returncode

        # Clean-up
        # "ownconda ci" automatically deletes the artifacts
        run(f'rm -rf {CONDA_ROOT / "conda-bld"}')
        run(f'conda install --quiet --yes --rev {conda_rev}', capture_output=True)

        if rc != 0:
            if ignore_errors:
                failed_pipelines.append(project)
            else:
                sys.exit(rc)

    if failed_pipelines:
        error = functools.partial(click.secho, fg='red', bold=True, err=True)
        error(f'{len(failed_pipelines)}/{len(PROJECTS)} projects failed:')
        for p in failed_pipelines:
            error(f'- {p}')


def getenv(channels):
    env = {k: v for k, v in os.environ.copy().items() if not k.startswith('CONDA_')}
    env['PATH'] = ':'.join([
        str(CONDA_ROOT / 'bin'),
        str(CONDA_ROOT / 'condabin'),
        '/usr/lib64/ccache',
        '/usr/local/bin',
        '/usr/bin',
        '/usr/local/sbin',
        '/usr/sbin',
    ])
    env['CONDA_CHANNELS'] = channels
    return env


def setup(env):
    if ROOT_DIR.is_dir():
        shutil.rmtree(ROOT_DIR)
    ROOT_DIR.mkdir()

    run = functools.partial(subprocess.run, env=env, shell=True, check=True)
    run(f'wget {INSTALLER_URL} -O {INSTALLER_LOC}')
    run(f'bash {INSTALLER_LOC} -p {CONDA_ROOT} -b')
    run(f'conda update --quiet --yes --all own-conda-tools')


if __name__ == '__main__':
    cli()  # pylint: disable=no-value-for-parameter
