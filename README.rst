==============
ownconda-tools
==============

.. image:: https://gitlab.com/ownconda/ownconda-tools/badges/master/build.svg
   :alt: build status
   :target: https://gitlab.com/ownconda/ownconda-tools/commits/master

.. image:: https://gitlab.com/ownconda/ownconda-tools/badges/master/coverage.svg
   :alt: coverage report
   :target: https://gitlab.com/ownconda/ownconda-tools/commits/master

Tools for creating and maintaining our Conda packages.


Documentation
=============

You can find the documentation at
https://forge.services.own/docs/ownconda/ownconda-tools
