# pylint: disable=redefined-outer-name
import pytest

from own_conda_tools import version


VERSION_DATA = [
    {
        'package': {'version': '1.2.3'},
        'build': {'number': 1},
    },
    {
        'version': '1.2.3',
        'build_number': 1,
    },
]


@pytest.fixture(params=VERSION_DATA)
def data(request):
    return request.param


def test_format_version(data):
    v = version.format_version(data)
    assert v == '1.2.3-1'


def test_version_tuple(data):
    v = version.version_tuple(data)
    assert v == (version.parse('1.2.3'), 1)
