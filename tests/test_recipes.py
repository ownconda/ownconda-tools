import networkx as nx
import pytest

from own_conda_tools import recipes


def test_load_and_dump():
    yaml_str = (
        'pkg-a: pkg-b\n'
        '# a comment\n'
        'bacon:\n'
        '  - 1  # another comment\n'
    )
    yaml = recipes.load_yaml(yaml_str)
    assert yaml['pkg-a'] == 'pkg-b'
    assert yaml['bacon'] == [1]

    dumped_yaml = recipes.dump_yaml(yaml)
    assert dumped_yaml == yaml_str


@pytest.mark.parametrize('paths', [
    '.',
    ['pkg-a', 'pkg-b'],
    ['pkg-a/conda', 'pkg-b/conda/client', 'pkg-b/conda/server'],
])
def test_load_recipes(paths, recipe_dir):
    """Test loading recipes.  It should not matter where we start to search."""
    if isinstance(paths, str):
        paths = recipe_dir / paths
    else:
        paths = [recipe_dir / p for p in paths]

    all_recipes = sorted(recipes.load_recipes(paths), key=lambda i: i[1])

    names = {r[0]['package']['name'] for r in all_recipes}
    assert names == {'pkg-a', 'pkg-b-client', 'pkg-b-server'}

    paths = [r[1] for r in all_recipes]
    assert paths == [
        recipe_dir / 'pkg-a/conda/meta.yaml',
        recipe_dir / 'pkg-b/conda/client/meta.yaml',
        recipe_dir / 'pkg-b/conda/server/meta.yaml',
    ]


@pytest.mark.parametrize('ignore_versions, collect, e_nodes, e_edges', [
    (False, recipes.ALL, {
        'pkg-b-client': {'primary': True},
        'pkg-b-server': {'primary': True},
        'pkg-a': {'primary': True},
        'pip >=3.5': {},
        'pytest': {},
        'requests': {},
    }, {
        ('pkg-b-client', 'requests'): {},
        ('pkg-a', 'pkg-b-client'): {},
        ('pkg-a', 'pip >=3.5'): {},
        ('pkg-a', 'pytest'): {},
    }),
    (True, recipes.ALL, {
        'pkg-b-client': {'primary': True},
        'pkg-b-server': {'primary': True},
        'pkg-a': {'primary': True},
        'pip': {},
        'pytest': {},
        'requests': {},
    }, {
        ('pkg-b-client', 'requests'): {},
        ('pkg-a', 'pkg-b-client'): {},
        ('pkg-a', 'pip'): {},
        ('pkg-a', 'pytest'): {},
    }),
    (True, recipes.BUILD, {
        'pkg-b-client': {'primary': True},
        'pkg-b-server': {'primary': True},
        'pkg-a': {'primary': True},
        'pip': {},
    }, {
        ('pkg-a', 'pip'): {},
    }),
    (True, recipes.RUN, {
        'pkg-b-client': {'primary': True},
        'pkg-b-server': {'primary': True},
        'pkg-a': {'primary': True},
        'requests': {},
    }, {
        ('pkg-b-client', 'requests'): {},
        ('pkg-a', 'pkg-b-client'): {},
    }),
    (True, recipes.TEST, {
        'pkg-b-client': {'primary': True},
        'pkg-b-server': {'primary': True},
        'pkg-a': {'primary': True},
        'pytest': {},
    }, {
        ('pkg-a', 'pytest'): {},
    }),
], indirect=['e_nodes'])
def test_get_dep_graph(recipe_dir, ignore_versions, collect, e_nodes, e_edges):
    recipe_meta = recipes.load_recipes([recipe_dir])
    dg = recipes.get_dep_graph(recipe_meta, ignore_versions, collect)
    assert dict(dg.nodes) == e_nodes
    assert dict(dg.edges) == e_edges


def test_sort_graph():
    dg = nx.DiGraph()
    dg.add_edges_from([('a', 'b'), ('b', 'c'), ('a', 'd')])
    sorted_pkgs = recipes.sort_graph(dg)
    assert sorted_pkgs in [
        ['d', 'c', 'b', 'a'],
        ['c', 'd', 'b', 'a'],
        ['c', 'b', 'd', 'a'],
    ]


def test_sort_graph_cycles():
    dg = nx.DiGraph()
    dg.add_edges_from([('a', 'b'), ('b', 'a')])
    exc_info = pytest.raises(nx.NetworkXUnfeasible, recipes.sort_graph, dg)
    assert str(exc_info.value) in [
        "Graph contains one or more cycles:\n- ['a', 'b']",
        "Graph contains one or more cycles:\n- ['b', 'a']",
    ]
