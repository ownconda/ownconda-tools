import pendulum

from own_conda_tools.commands import prune_index


PKGS = [
    {
        'internal': True,
        'name': 'spam',
        'version': '1.2.3',
        'build': '0',
        'build_number': 0,
        'filename': 'spam-1.2.3-0.tar.bz2',
        'datetime': pendulum.parse('2017-12-01'),
    },
    {
        'internal': True,
        'name': 'spam',
        'version': '1.2.3',
        'build': '1',
        'build_number': 1,
        'filename': 'spam-1.2.3-1.tar.bz2',
        'datetime': pendulum.parse('2018-04-06'),
    },
    {
        'internal': True,
        'name': 'spam',
        'version': '1.2.3',
        'build': '2',
        'build_number': 2,
        'filename': 'spam-1.2.3-2.tar.bz2',
        'datetime': pendulum.parse('2018-04-08'),
    },
    {
        'internal': True,
        'name': 'spam',
        'version': '2.1.0',
        'build': '0',
        'build_number': 0,
        'filename': 'spam-2.1.0-0.tar.bz2',
        'datetime': pendulum.parse('2018-07-01'),
    },
    {
        'internal': True,
        'name': 'eggs',
        'version': '1.2.3',
        'build': 'py36_0',
        'build_number': 0,
        'filename': 'eggs-1.2.3-py36_0.tar.bz2',
        'datetime': pendulum.parse('2017-12-01'),
    },
    {
        'internal': True,
        'name': 'eggs',
        'version': '1.2.3',
        'build': 'py37_0',
        'build_number': 0,
        'filename': 'eggs-1.2.3-py37_0.tar.bz2',
        'datetime': pendulum.parse('2017-12-01'),
    },
    {
        'internal': True,
        'name': 'eggs',
        'version': '1.2.3',
        'build': 'py36_1',
        'build_number': 1,
        'filename': 'eggs-1.2.3-py36_1.tar.bz2',
        'datetime': pendulum.parse('2018-04-06'),
    },
    {
        'internal': True,
        'name': 'eggs',
        'version': '1.2.3',
        'build': 'py37_1',
        'build_number': 1,
        'filename': 'eggs-1.2.3-py37_1.tar.bz2',
        'datetime': pendulum.parse('2018-04-06'),
    },
    {
        'internal': True,
        'name': 'eggs',
        'version': '1.2.3',
        'build': 'py36_2',
        'build_number': 2,
        'filename': 'eggs-1.2.3-py36_2.tar.bz2',
        'datetime': pendulum.parse('2018-04-08'),
    },
    {
        'internal': True,
        'name': 'eggs',
        'version': '1.2.3',
        'build': 'py37_2',
        'build_number': 2,
        'filename': 'eggs-1.2.3-py37_2.tar.bz2',
        'datetime': pendulum.parse('2018-04-08'),
    },
    {
        'internal': True,
        'name': 'eggs',
        'version': '2.1.0',
        'build': 'py36_0',
        'build_number': 0,
        'filename': 'eggs-2.1.0-py36_0.tar.bz2',
        'datetime': pendulum.parse('2018-07-01'),
    },
    {
        'internal': True,
        'name': 'eggs',
        'version': '2.1.0',
        'build': 'py37_0',
        'build_number': 0,
        'filename': 'eggs-2.1.0-py37_0.tar.bz2',
        'datetime': pendulum.parse('2018-07-01'),
    },
    {
        'internal': True,
        'name': 'bacon',
        'version': '0.1.0',
        'build': '0',
        'build_number': 0,
        'filename': 'bacon-0.1.0-py36_0.tar.bz2',
        'datetime': pendulum.parse('2017-12-01'),
    },
    {
        'internal': True,
        'name': 'bacon',
        'version': '0.1.0',
        'build': '1',
        'build_number': 0,
        'filename': 'bacon-0.1.0-py37_0.tar.bz2',
        'datetime': pendulum.parse('2017-12-01'),
    },
    {
        "internal": False,
        "build": "py36_0",
        "build_number": 0,
        "depends": [
            "c-blosc",
            "python 3.6*"
        ],
        "license": "http://www.opensource.org/licenses/mit-license.php",
        "md5": "68e357ea4b3c2f063f4608d4f020e52b",
        "name": "blosc",
        "sha256": "01614a55199b563b6cae31afc17e3b297e39866fcaf05c6634b7c24c5813494a",
        "size": 38750,
        "subdir": "linux-64",
        "version": "1.5.0",
        "filename": "blosc-1.5.0-py36_0.tar.bz2",
        "datetime": pendulum.from_timestamp(0)
    },
    {
        "internal": False,
        "build": "py36_1",
        "build_number": 1,
        "depends": [
            "c-blosc",
            "python 3.6*"
        ],
        "license": "http://www.opensource.org/licenses/mit-license.php",
        "md5": "928e71146e78ece0dbd433bed2e50159",
        "name": "blosc",
        "sha256": "c52570a29fee98cd25ca2a8fe6cfccb07eadf9a227167db79ca06218e7447b27",
        "size": 38999,
        "subdir": "linux-64",
        "version": "1.5.1",
        "filename": "blosc-1.5.1-py36_1.tar.bz2",
        "datetime": pendulum.from_timestamp(0)
    },
    {
        "internal": False,
        "build": "py37_2",
        "build_number": 2,
        "depends": [
            "c-blosc",
            "numpy",
            "python >=3.7,<3.8.0a0"
        ],
        "license": "http://www.opensource.org/licenses/mit-license.php",
        "md5": "053f4b9178fca063ab6ea8e0880c2441",
        "name": "blosc",
        "sha256": "72e977a41bc5222bfcad74f7ef794f52739b555c60715b3d747566f3f72c420a",
        "size": 26793,
        "subdir": "linux-64",
        "timestamp": 1533403685556,
        "version": "1.5.1",
        "filename": "blosc-1.5.1-py37_2.tar.bz2",
        "datetime": pendulum.from_timestamp(1533403685556 // 1000)
    },
]
PRUNE_BEFORE = pendulum.parse('2018-01-01')
KEEP_AFTER = pendulum.parse('2018-07-01')
EXPECTED_PRUNE_BUILDS = {
    'spam-1.2.3-0.tar.bz2',
    'spam-1.2.3-1.tar.bz2',
    'eggs-1.2.3-py36_0.tar.bz2',
    'eggs-1.2.3-py37_0.tar.bz2',
    'eggs-1.2.3-py36_1.tar.bz2',
    'eggs-1.2.3-py37_1.tar.bz2',
}
EXPECTED_PRUNE_OUTDATED = {
    'spam-1.2.3-0.tar.bz2',
    'spam-1.2.3-1.tar.bz2',
    'eggs-1.2.3-py36_0.tar.bz2',
    'eggs-1.2.3-py37_0.tar.bz2',
    'eggs-1.2.3-py36_1.tar.bz2',
    'eggs-1.2.3-py37_1.tar.bz2',
    'blosc-1.5.0-py36_0.tar.bz2',
}


def test_prune_builds():
    fnames = prune_index.prune_builds(PKGS)
    assert fnames == EXPECTED_PRUNE_BUILDS


def test_prune_outdated():
    fnames = prune_index.prune_outdated(PKGS, KEEP_AFTER, PRUNE_BEFORE)
    assert fnames == EXPECTED_PRUNE_OUTDATED
