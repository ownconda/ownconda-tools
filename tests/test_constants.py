import os
import pathlib
import sys

import pytest

from own_conda_tools import constants


CONDA_EXE = constants.locate_conda()
CONDA_INFO = constants.get_conda_info(CONDA_EXE)


def test_locate_conda():
    assert constants.locate_conda().endswith('/bin/conda')


def test_locate_conda_from_env(monkeypatch, tmp_path):
    expected = str(tmp_path / 'conda')
    monkeypatch.setenv('CONDA_EXE', expected)
    monkeypatch.setattr(os.path, 'isfile', lambda f: True)
    path = constants.locate_conda()
    assert path == expected


def test_locate_conda_from_prefix(monkeypatch):
    monkeypatch.delenv('CONDA_EXE', raising=False)
    monkeypatch.setattr(os.path, 'isfile', lambda f: True)
    path = constants.locate_conda()
    assert path == os.path.join(sys.prefix, 'bin', 'conda')


def test_locate_conda_not_found(monkeypatch):
    """An error must be raised if conda cannot be found."""
    monkeypatch.setenv('CONDA_EXE', '')
    monkeypatch.setattr(sys, 'prefix', '/spam')
    pytest.raises(RuntimeError, constants.locate_conda)


def test_conda_info():
    info = CONDA_INFO
    for key in ['default_prefix', 'envs_dirs', 'platform']:
        assert key in info


def test_in_pipeline(monkeypatch):
    monkeypatch.setenv('CI_PROJECT_DIR', '1')
    assert constants.in_pipeline()


def test_not_in_pipeline(monkeypatch):
    monkeypatch.delenv('CI_PROJECT_DIR', raising=False)
    assert not constants.in_pipeline()


def test_in_test():
    """Assert that the original :func:`in_test()` function was replaced by
    a lambda that returns ``True``.

    """
    patched_func = constants.in_test
    assert patched_func.__name__ == '<lambda>'
    assert constants.in_test()


def test_not_in_test(monkeypatch):
    """Assert that the original :func:`in_test()` function returns ``False``.

    """
    # We need to undo the monkeypatching of "constants.in_test":
    # 1. Get back the original function
    patched_func = constants.in_test
    mod, name, orig_func = [
        (mod, name, orig_func)
        for mod, name, orig_func in monkeypatch._setattr  # pylint: disable=protected-access
        if mod is constants and name == 'in_test'
    ][0]

    # 2. Undo by monkeypatching the monkeypatch (monkey-monkeypatch-patch):
    with monkeypatch.context() as mp:
        mp.setattr(mod, name, orig_func)
        assert constants.in_test is orig_func
        assert not constants.in_test()

    # 3. Make sure we undit our monkeymonkeypatchpatch
    assert constants.in_test is patched_func


@pytest.mark.parametrize('setenv, prefix', [
    (False, None),
    (False, 'spam'),
    (True, None),
    (True, 'spam'),
])
def test_artifacts_dir(setenv, prefix, tmp_path, monkeypatch):
    if setenv:
        monkeypatch.setenv('CI_PROJECT_DIR', str(tmp_path))
    else:
        monkeypatch.delenv('CI_PROJECT_DIR', raising=False)

    path = constants.artifacts_dir(prefix)
    if prefix:
        assert path == pathlib.Path(prefix, 'artifacts').resolve()
    elif setenv:
        assert path == tmp_path / 'artifacts'
    else:
        assert path == pathlib.Path('artifacts').resolve()


def test_resource_dir():
    cd = constants.resource_dir()
    assert cd == pathlib.Path(constants.__file__).parent / 'resources'


def test_resource_file():
    cf = constants.resource_file('pylintrc')
    assert cf == pathlib.Path(constants.__file__).parent / 'resources' / 'pylintrc'
