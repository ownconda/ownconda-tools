# pylint: disable=invalid-name,protected-access,redefined-outer-name
import io
import json
import shlex
import socket
import sys

import pytest

from own_conda_tools import constants, recipes, util


CONDA_EXE = constants.locate_conda()
CONDA_INFO = constants.get_conda_info(CONDA_EXE)


@pytest.fixture
def conda_dir(tmp_path):
    """Create a directory containing a Conda meta.yaml and return the
    direcotry path as well as a list with all dependency names."""
    py = constants.PYTHON_VERSION
    meta_yaml = tmp_path / 'meta.yaml'
    meta_yaml.write_text(
        f'requirements:\n'
        f'  build:\n'
        f'    - python {py}*\n'
        f'  run:\n'
        f'    - click >=6.6\n'
        f'    - conda\n'
        f'    - conda-build\n'
        f'test:\n'
        f'  requires:\n'
        f'    - pytest\n'
    )
    return tmp_path, ['click>=6.6', 'conda', 'conda-build', 'pytest', f'python={py}*']


@pytest.fixture
def artifacts_dir(tmp_path):
    """Create a local fake build channel with a "repodata.json" file."""
    pkg_data = {
        'info': {},
        'packages': {
            'spam-1.2-0.tar.bz2': {
                'name': 'spam',
            },
            'eggs-3.2-0.tar.bz2': {
                'name': 'eggs',
            },
            'eggs-3.0-0.tar.bz2': {
                'name': 'eggs',
            },
        },
    }
    pkg_dir = tmp_path / CONDA_INFO['platform']
    pkg_dir.mkdir()
    pkg_dir.joinpath('repodata.json').write_text(json.dumps(pkg_data))

    return tmp_path


@pytest.fixture
def runcmd(monkeypatch):
    class ExecutedCommand:
        def __init__(self):
            self.cmds = []
            self.cmd = None

        def run(self, cmd, **kwargs):
            self.cmds.append(cmd)
            self.cmd = cmd
            for k, v in kwargs.items():
                setattr(self, k, v)

    executed_command = ExecutedCommand()

    monkeypatch.setattr(util, 'run', executed_command.run)

    return executed_command


@pytest.fixture
def with_testenv(monkeypatch):
    """Pretend to be in a Conda env."""
    monkeypatch.setenv('CONDA_DEFAULT_ENV', 'testenv')


@pytest.mark.parametrize('cmd', [
    "spam 'eggs bacon'",
    ['spam', 'eggs bacon'],
])
def test_echo_cmd(cmd, capsys):
    util.echo_cmd(cmd)
    out, err = capsys.readouterr()
    assert out == ''
    assert err == "spam 'eggs bacon'\n"


def test_echo_cmd_with_cwd(capsys):
    util.echo_cmd('spam eggs', cwd='bacon')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'cd bacon && spam eggs\n'


@pytest.mark.parametrize('cmd, env, check, expected', [
    ['exit(0)', None, False, 0],
    ['exit(1)', None, False, 1],
    ['import os; exit(int(os.getenv("R")))', {'R': '42'}, False, 42],
    ['exit(0)', None, True, 0],
    ['exit(1)', None, True, SystemExit],
    ['import os; exit(int(os.getenv("R")))', {'R': '42'}, True, SystemExit],
])
def test_run(cmd, env, check, expected, capsys):
    cmd = [sys.executable, '-c', cmd]
    if isinstance(expected, int):
        retcode = util.run(cmd, env=env, check=check).returncode
        assert retcode == expected
    else:
        # Check-calls with non-zero return codes must raise an Exception:
        pytest.raises(expected, util.run, cmd, env=env, check=check)

    # Assert cmd is printed to stdout:
    out, err = capsys.readouterr()
    assert out == ''
    cmdstr = ' '.join(map(shlex.quote, cmd))
    if isinstance(expected, int):
        assert err == f'{cmdstr}\n'
    else:
        cmdstr = ' '.join(map(shlex.quote, cmd))
        exit_status = env["R"] if env is not None else 1
        assert f'Command "{cmdstr}" returned non-zero exit status {exit_status}' in err


@pytest.mark.parametrize('error', [True, False])
def test_run_quiet(error, capsys):
    script = (
        f'import sys\n'
        f'print("ohai")\n'
        f'print("cya", file=sys.stderr)\n'
        f'sys.exit({int(error)})\n'
    )
    cmd = [sys.executable, '-c', script]
    result = util.run(cmd, check=False, quiet=True)
    out, err = capsys.readouterr()

    cmd[-1] = f"'{cmd[-1]}'"
    cmdstr = ' '.join(cmd) + '\n'
    if error:
        assert result.returncode == 1
        assert result.stdout == 'ohai\n'
        assert result.stderr == 'cya\n'
        assert out == 'STDOUT:\nohai\n\nSTDERR:\ncya\n\n'
        assert err == cmdstr
    else:
        assert result.returncode == 0
        assert result.stdout == 'ohai\n'
        assert result.stderr == 'cya\n'
        assert out == ''
        assert err == cmdstr


def test_run_interactive(capsys, monkeypatch):
    monkeypatch.setattr(sys, 'stdin', io.StringIO('y\nn\n'))
    script = (
        f'import sys\n'
        f'print("ohai")\n'
        f'print("cya", file=sys.stderr)\n'
        f'sys.exit(1)\n'
    )
    cmd = [sys.executable, '-c', script]
    with pytest.raises(SystemExit):
        util.run(cmd, check=True, quiet=True, interactive=True)
    out, err = capsys.readouterr()

    cmd[-1] = f"'{cmd[-1]}'"
    cmdstr = ' '.join(cmd)
    output = 'STDOUT:\nohai\n\nSTDERR:\ncya\n\nRetry last command? [Y/n]: ' * 2
    errmsg = f'Command "{cmdstr}" returned non-zero exit status 1\n' * 2
    assert out == output
    assert err == f'{cmdstr}\n{errmsg}'


def test_get_unsued_tcp_port():
    port = util.get_unused_tcp_port()
    assert isinstance(port, int)
    try:
        sock = socket.socket()
        sock.bind(('127.0.0.1', port))
    finally:
        sock.close()


def test_get_pkg_paths(artifacts_dir):
    packages = util._get_pkg_paths(CONDA_INFO['platform'], artifacts_dir)
    assert packages == {'spam', 'eggs'}


def test_install_dependencies(conda_dir, runcmd, with_testenv):  # pylint: disable=unused-argument
    path, deps = conda_dir

    util.install_dependencies(
        path, conda_exe=CONDA_EXE, dep_type=recipes.ALL, env_name='testenv'
    )

    # We didn't install into *root*, so conda package must be removed
    deps.remove('conda')
    deps.remove('conda-build')
    assert runcmd.cmds[0] == [
        CONDA_EXE, 'update', '--all',
        '--quiet', '--yes',
        '--name=testenv']
    assert runcmd.cmds[1] == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
        '--name=testenv',
    ] + deps


def test_install_dependencies_path_list(conda_dir, runcmd, with_testenv):  # pylint: disable=unused-argument
    """install_dependencies() can take a path string or list."""
    path, deps = conda_dir

    util.install_dependencies(
        [path], conda_exe=CONDA_EXE, dep_type=recipes.ALL, env_name='testenv'
    )

    # We didn't install into *root*, so conda package must be removed
    deps.remove('conda')
    deps.remove('conda-build')
    assert runcmd.cmd == [  # We are only interested in the last command
        CONDA_EXE, 'install',
        '--quiet', '--yes',
        '--name=testenv',
    ] + deps


def test_install_dependencies_python(conda_dir, runcmd, with_testenv):  # pylint: disable=unused-argument
    """Test corretly handlling python version pinning."""
    path, deps = conda_dir

    util.install_dependencies(
        path,
        conda_exe=CONDA_EXE,
        dep_type=recipes.ALL,
        python=constants.PYTHON_VERSION,
        env_name='testenv',
    )

    # We didn't install into *root*, so conda package must be removed
    deps.remove('conda')
    deps.remove('conda-build')
    deps.remove(f'python={constants.PYTHON_VERSION}*')
    deps.append(f'python={constants.PYTHON_VERSION}')
    assert runcmd.cmds[0] == [
        CONDA_EXE, 'update', '--all',
        '--quiet', '--yes',
        '--name=testenv']
    assert runcmd.cmds[1] == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
        '--name=testenv',
    ] + deps


@pytest.mark.parametrize('explicit', [True, False])
def test_install_dependencies_root_env(
        explicit, conda_dir, runcmd, monkeypatch):
    if explicit:
        env_name = 'root'
        monkeypatch.setenv('CONDA_DEFAULT_ENV', 'testenv')
    else:
        env_name = None
        monkeypatch.delenv('CONDA_DEFAULT_ENV', raising=False)

    path, deps = conda_dir
    util.install_dependencies(
        path, conda_exe=CONDA_EXE, dep_type=recipes.ALL, env_name=env_name
    )

    # We installed into *root*, so conda packages should still be there
    assert 'conda' in deps
    assert 'conda-build' in deps
    assert runcmd.cmd == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
    ] + (['--name=root'] if explicit else []) + deps


def test_install_dependencies_with_extras(conda_dir, runcmd, with_testenv):  # pylint: disable=unused-argument
    path, deps = conda_dir
    util.install_dependencies(
        path, conda_exe=CONDA_EXE, dep_type=recipes.ALL, env_name='testenv',
        extras={'a', 'b'},
    )

    # We didn't install into *root*, so conda package must be removed
    deps.remove('conda')
    deps.remove('conda-build')
    assert runcmd.cmd == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
        '--name=testenv',
        'a', 'b',  # Extras
    ] + deps


def test_install_dependencies_with_extra_channels(
        conda_dir, runcmd, artifacts_dir, with_testenv):  # pylint: disable=unused-argument
    path, deps = conda_dir
    util.install_dependencies(
        path, dep_type=recipes.ALL, conda_exe=CONDA_EXE, env_name='testenv',
        extra_channels=['--channel=staging'],
    )

    # We didn't install into *root*, so conda package must be removed
    deps.remove('conda')
    deps.remove('conda-build')
    assert runcmd.cmds[0] == [
        CONDA_EXE, 'update', '--all',
        '--quiet', '--yes',
        '--name=testenv',
        '--channel=staging',
    ]
    assert runcmd.cmds[1] == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
        '--name=testenv',
        '--channel=staging',
    ] + deps


def test_install_package(artifacts_dir, conda_dir, runcmd, with_testenv):  # pylint: disable=unused-argument
    path, deps = conda_dir
    util.install_packages(
        artifacts_dir, path, conda_exe=CONDA_EXE, platform=CONDA_INFO['platform']
    )
    # We didn't install into *root*, so conda package must be removed
    deps.remove('conda')
    deps.remove('conda-build')
    assert runcmd.cmds[0] == [
        CONDA_EXE, 'update', '--all',
        '--quiet', '--yes',
    ]
    assert runcmd.cmds[1] == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
    ] + deps
    assert runcmd.cmds[2] == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
        '--channel', str(artifacts_dir),
        'eggs', 'spam',
    ]


def test_install_packages_with_extra_channels(
        artifacts_dir, conda_dir, runcmd, with_testenv):  # pylint: disable=unused-argument
    path, deps = conda_dir
    util.install_packages(
        artifacts_dir, path, conda_exe=CONDA_EXE, platform=CONDA_INFO['platform'],
        extra_channels=['--channel=staging'],
    )
    # We didn't install into *root*, so conda package must be removed
    deps.remove('conda')
    deps.remove('conda-build')
    assert runcmd.cmds[0] == [
        CONDA_EXE, 'update', '--all',
        '--quiet', '--yes',
        '--channel=staging',
    ]
    assert runcmd.cmds[1] == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
        '--channel=staging',
    ] + deps
    assert runcmd.cmds[2] == [
        CONDA_EXE, 'install',
        '--quiet', '--yes',
        '--channel', str(artifacts_dir),
        '--channel=staging',
        'eggs', 'spam',
    ]
