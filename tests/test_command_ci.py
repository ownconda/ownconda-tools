import functools
import re
import socket
import subprocess

import pytest
import ruamel.yaml
import trio

from own_conda_tools import util
from own_conda_tools.commands import ci


GITLAB_CI_YAML = r"""image: forge.services.own/centos7-ownconda-develop

before_script:
  - conda update --quiet --yes --all own-conda-tools

stages:
  - prepare
  - build
  - test
  - upload
  - docs

prepare:
  stage: prepare
  script:
    - test -f swagger-codegen-cli.jar || wget -O swagger-codegen-cli.jar ...

build:
  stage: build
  script:
    - ownconda build conda

test:
  stage: test
  script:
    - ownconda test
  coverage: '/^TOTAL.*\s+(\d+\%)\s*$/'

lint:
  stage: test
  script:
    # "script" may just be a single command, not a list of commands:
    ownconda lint src/* tests

security:
  stage: test
  script:
    - ownconda sec-check -d B310 -d B321 -d B402 -d B604 src/* tests

upload:
  stage: upload
  script:
    - ownconda upload

make_docs:
  stage: docs
  script:
    - ownconda make-docs
"""
STEPS_BUILD = [
    {'stage': '', 'step': 'before_script', 'script': [
        'conda update --quiet --yes --all own-conda-tools',
    ]},
    {'stage': 'prepare', 'step': 'prepare', 'script': [
        'test -f swagger-codegen-cli.jar || wget -O swagger-codegen-cli.jar ...',
    ]},
    {'stage': 'build', 'step': 'build', 'script': [
        'ownconda build conda',
    ]},
]
STEPS_TEST = STEPS_BUILD + [
    {'stage': 'test', 'step': 'test', 'script': [
        'ownconda test',
    ], 'coverage': r'/^TOTAL.*\s+(\d+\%)\s*$/'},
    {'stage': 'test', 'step': 'lint', 'script': [
        'ownconda lint src/* tests',
    ]},
    {'stage': 'test', 'step': 'security', 'script': [
        'ownconda sec-check -d B310 -d B321 -d B402 -d B604 src/* tests',
    ]},
]

SERVICE_CONFIGS = [
    (  # Simple mariadb
        """
        services:
            - mariadb:latest
        variables:
            MYSQL_DATABASE: test
            MYSQL_ROOT_PASSWORD: rootmysql
        """,
        {
            'MYSQL_DATABASE': 'test',
            'MYSQL_ROOT_PASSWORD': 'rootmysql',
            'MYSQL_HOST': '127.0.0.1',
            'MYSQL_PORT': '1337',
            'DB_URL': 'mysql://root:rootmysql@127.0.0.1:1337/test',
        },
        [{
            'image': 'mariadb:latest',
            'container_name': 'ownconda_ci_service_mariadb',
            'container_port': '3306',
            'host_port': '1337',
        }],
    ),
    (  # Multiple (known) services with aliases
        """
        services:
            - name: forge.services.own/centos7-keycloak:latest
              alias: keycloak
            - name: mariadb:latest
              alias: le_db
        variables:
            MYSQL_DATABASE: le_db
            MYSQL_ALLOW_EMPTY_PASSWORD: "yes"
            KEYCLOAK_HOST: keycloak
            KEYCLOAK_PORT: 8080
        """,
        {
            'MYSQL_DATABASE': 'le_db',
            'MYSQL_ALLOW_EMPTY_PASSWORD': 'yes',
            'MYSQL_HOST': '127.0.0.1',
            'MYSQL_PORT': '1337',
            'DB_URL': 'mysql://root:@127.0.0.1:1337/le_db',
            'KEYCLOAK_HOST': '127.0.0.1',
            'KEYCLOAK_PORT': '1337',
        },
        [{
            'image': 'forge.services.own/centos7-keycloak:latest',
            'container_name': 'ownconda_ci_service_keycloak',
            'container_port': '8080',
            'host_port': '1337',
        }, {
            'image': 'mariadb:latest',
            'container_name': 'ownconda_ci_service_le_db',
            'container_port': '3306',
            'host_port': '1337',
        }],
    ),
    (  # Multiple unkown services
        """
        services:
            - redis
            - memcached:alpine
        variables: {}
        """,
        {},
        [{
            'image': 'redis',
            'container_name': 'ownconda_ci_service_redis',
            'container_port': None,
            'host_port': None,
        }, {
            'image': 'memcached:alpine',
            'container_name': 'ownconda_ci_service_memcached',
            'container_port': None,
            'host_port': None,
        }],
    ),
]
SERVICE_CONFIGS = [(ruamel.yaml.safe_load(y), v, s) for y, v, s in SERVICE_CONFIGS]


@pytest.mark.parametrize('include_tests, expected', [
    (False, STEPS_BUILD),
    (True, STEPS_TEST),
])
def test_parse_gitlab_ci_yaml(include_tests, expected):
    commands = ci.parse_gitlab_ci_yaml(GITLAB_CI_YAML, include_tests)
    assert commands == expected


@pytest.mark.parametrize('gitlab_ci_yaml', [
    r"""stages:
  - build
  - test
  - upload

build:
  stage: build
  script:
    - ownconda build conda

upload:
  stage: upload
  script:
    - ownconda upload
""",
    r"""stages:
  - build
  - upload

build:
  stage: build
  script:
    - ownconda build conda

upload:
  stage: upload
  script:
    - ownconda upload
""",
])
def test_parse_missing_tests(gitlab_ci_yaml):
    commands = ci.parse_gitlab_ci_yaml(gitlab_ci_yaml, include_tests=True)
    assert commands == [{
        'stage': 'build',
        'step': 'build',
        'script': ['ownconda build conda'],
    }]


@pytest.mark.parametrize('step, expected_vars, expected_services', SERVICE_CONFIGS)
def test_collect_services(step, expected_vars, expected_services, monkeypatch):
    monkeypatch.setattr(util, 'get_unused_tcp_port', lambda: 1337)
    variables = step['variables']
    services, variables = ci.collect_services(step['services'], variables)
    assert variables == expected_vars
    assert services == expected_services


def test_collect_and_start_services(monkeypatch):
    monkeypatch.setattr(util, 'get_unused_tcp_port', lambda: 1337)

    started_services = []
    stopped_services = []

    def start_mock(**kwargs):
        started_services.append(kwargs)

    def stop_mock(name):
        stopped_services.append(name)

    monkeypatch.setattr(ci, 'start_service', start_mock)
    monkeypatch.setattr(ci, 'stop_service', stop_mock)

    step = ruamel.yaml.safe_load("""
        services:
            - mariadb:latest
            - name: forge.services.own/centos7-keycloak:latest
              alias: keycloak
        variables:
            MYSQL_DATABASE: test
            MYSQL_ROOT_PASSWORD: rootmysql
            KEYCLOAK_HOST: keycloak
            KEYCLOAK_PORT: 8080
        """)

    services, variables = ci.collect_services(**step)
    with ci.start_services(services, variables):
        assert started_services == [
            {
                'image': 'mariadb:latest',
                'container_name': 'ownconda_ci_service_mariadb',
                'container_port': '3306',
                'host_port': '1337',
                'variables': variables,
            },
            {
                'image': 'forge.services.own/centos7-keycloak:latest',
                'container_name': 'ownconda_ci_service_keycloak',
                'container_port': '8080',
                'host_port': '1337',
                'variables': variables,
            },
        ]
        assert stopped_services == []
    assert stopped_services == [
        'ownconda_ci_service_mariadb',
        'ownconda_ci_service_keycloak',
    ]


def test_expandvars():
    env = {
        'A': 'spam',
        'B': '$A eggs${A}eggs',
        'C': '$B',
        'D': '\\$B',
        'E': '$Aeggs',
    }
    env = ci.expandvars(env)
    assert env == {
        'A': 'spam',
        'B': 'spam eggsspameggs',
        'C': 'spam eggsspameggs',
        'D': '\\$B',
        'E': '',
    }


def test_start_service(tmp_path):
    try:
        subprocess.run(['docker', '--version'], check=True)
    except (FileNotFoundError, subprocess.CalledProcessError):
        pytest.skip('Cannot run Docker on this platform')

    name = '-'.join(tmp_path.parts[-3:])
    port = util.get_unused_tcp_port()
    ci.start_service(
        image='mariadb:latest',
        container_name=name,
        container_port='3306',
        host_port=str(port),
        variables={'MYSQL_DATABASE': 'test', 'MYSQL_ROOT_PASSWORD': 'rootmysql'},
    )
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect(('127.0.0.1', int(port)))
    sock.close()

    ci.stop_service(name)

    with pytest.raises(ConnectionRefusedError):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect(('127.0.0.1', int(port)))


def test_get_gitlab_vars(tmp_path):
    subprocess.run('git init', shell=True, cwd=tmp_path)
    subprocess.run('git checkout -b "Spam#23"', shell=True, cwd=tmp_path)
    gl_vars = ci.get_gitlab_vars(tmp_path)
    assert gl_vars in [
        {  # fedora (newer git)
            'CI_COMMIT_REF_NAME': 'Spam#23',
            'CI_COMMIT_REF_SLUG': 'spam-23',
            'CI_COMMMIT_SHA': '(initial)',
            'CI_PROJECT_DIR': str(tmp_path),
        },
        {  # centos7
            'CI_COMMIT_REF_NAME': 'Initial commit on Spam#23',
            'CI_COMMIT_REF_SLUG': 'initial-commit-on-spam-23',
            'CI_PROJECT_DIR': str(tmp_path),
        },
    ]


def test_slugify():
    assert ci.slugify('-Ohai 124/branch-') == 'ohai-124-branch'
    assert ci.slugify(
        '-Ohai 124-branch-Ohai 124-branch-Ohai 124-branch-Ohai 124-branch-'
    ) == 'ohai-124-branch-ohai-124-branch-ohai-124-branch-ohai-124-branc'


@pytest.mark.parametrize('quiet', [True, False])
def test_run_cmds(quiet, tmp_path, capfd):
    ansi_escape = re.compile(r'\x1B\[[0-?]*[ -/]*[@-~]')
    cmds = [
        # All command should run in a single session
        'export G="ohai"',
        'echo "$G stdout"',
        '>&2 echo "$G stderr"',
        # Check passing cwd and env
        'pwd',
        'echo $E',
    ]
    env = {'E': 'ohai env'}

    expected_stdout = (
        f'$ export G="ohai"\n'
        f'$ echo "$G stdout"\n'
        f'ohai stdout\n'
        f'$ >&2 echo "$G stderr"\n'
        f'$ pwd\n'
        f'{tmp_path}\n'
        f'$ echo $E\n'
        f'ohai env\n'
    )
    expected_stderr = f'ohai stderr\n'

    result = trio.run(functools.partial(
        ci.run_cmds, cmds, capture_output=quiet, cwd=tmp_path, env=env
    ))

    if quiet:
        assert ansi_escape.sub('', result.stdout) == expected_stdout
        assert ansi_escape.sub('', result.stderr) == expected_stderr
        assert capfd.readouterr() == ('', '')
    else:
        assert result.stdout is None
        assert result.stderr is None
        out, err = capfd.readouterr()
        assert ansi_escape.sub('', out) == expected_stdout
        assert ansi_escape.sub('', err) == expected_stderr
