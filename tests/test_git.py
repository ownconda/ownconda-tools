# pylint: disable=redefined-outer-name
import functools
import re
import subprocess

import pytest

from own_conda_tools import git


@pytest.fixture
def gitrepo(tmp_path):
    git.git('init', 'src', cwd=tmp_path)
    repo = tmp_path / 'src'
    git.git('config', '--local', 'user.email', 'ci@owncompany.com', cwd=repo)
    git.git('config', '--local', 'user.name', 'CI', cwd=repo)
    return repo


def test_git(tmp_path):
    result = git.git('init', 'repo', cwd=tmp_path)
    assert result.returncode == 0
    assert tmp_path.joinpath('repo', '.git').is_dir()


def test_git_check_error(tmp_path):
    with pytest.raises(subprocess.CalledProcessError):
        git.git('spam', cwd=tmp_path)


def test_git_nocheck_error(tmp_path):
    result = git.git('spam', cwd=tmp_path, check=False)
    assert result.returncode == 1


def test_current_branch(gitrepo):
    assert git.current_branch(gitrepo) == 'master'
    git.git('checkout', '-b', 'spam', cwd=gitrepo)
    gitrepo.joinpath('file').write_text('')
    git.git('add', 'file', cwd=gitrepo)
    git.git('commit', '-m', 'commit', cwd=gitrepo)
    assert git.current_branch(gitrepo) == 'spam'


def test_try_checkout(gitrepo, capsys):
    git.try_checkout(gitrepo, 'master')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'Checking out "master" ... OK\n'


def test_try_checkout_none(gitrepo, capsys):
    git.try_checkout(gitrepo, branch=None)
    out, err = capsys.readouterr()
    assert out == ''
    assert err == ''


def test_try_checkout_not_found(gitrepo, capsys):
    git.try_checkout(gitrepo, 'develop')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'Checking out "develop" ... branch not found, using "master"\n'


def test_try_checkout_no_master(gitrepo, capsys):
    git.git('checkout', '-b', 'spam', cwd=gitrepo)
    gitrepo.joinpath('file').write_text('')
    git.git('add', 'file', cwd=gitrepo)
    git.git('commit', '-m', 'commit', cwd=gitrepo)
    git.try_checkout(gitrepo, 'develop')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'Checking out "develop" ... branch not found, using "spam"\n'


def test_update_clone(gitrepo, capsys):
    dest = gitrepo.parent.joinpath('clone')
    git.update(gitrepo, dest, 'master')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == (
        f'Cloning "{gitrepo}" into "{dest}" ... OK\nChecking out "master" ... OK\n'
    )


def test_update_pull(gitrepo, capsys):
    # Prepare src repo so that the clonging and pulling works
    gitrepo.joinpath('README').write_text('spam')
    git.git('add', '.', cwd=gitrepo)
    git.git('commit', '-m', 'commit', cwd=gitrepo)
    dest = gitrepo.parent.joinpath('clone')

    git.git('clone', str(gitrepo), dest.name, cwd=dest.parent)

    git.update(gitrepo, dest, 'master')
    out, err = capsys.readouterr()
    assert out == ''
    assert err == f'Checking out "master" ... OK\nPulling repo "{dest}" ... OK\n'

    git.update(gitrepo, dest, 'master', rebase=True)
    out, err = capsys.readouterr()
    assert out == ''
    assert err == f'Checking out "master" ... OK\nPulling repo "{dest}" ... OK\n'


def test_git_describe(gitrepo):
    g = functools.partial(git.git, cwd=gitrepo)
    g('init')
    gitrepo.joinpath('file').write_text('spam')
    g('add', 'file')
    g('commit', '-m', 'add file')

    data = git.describe(gitrepo)
    assert len(data) == 1
    assert re.match(r'[0-9a-f]{7}', data['GIT_DESCRIBE_HASH'])
    g('checkout', data['GIT_DESCRIBE_HASH'])

    g('tag', '1')
    gitrepo.joinpath('file').write_text('eggs')
    g('add', 'file')
    g('commit', '-m', 'update file')
    gitrepo.joinpath('file').write_text('bacon')
    g('add', 'file')
    g('commit', '-m', 'update file')

    data = git.describe(gitrepo)
    assert len(data) == 3
    assert data['GIT_DESCRIBE_TAG'] == '1'
    assert data['GIT_DESCRIBE_NUMBER'] == '2'
    assert re.match(r'[0-9a-f]{6,8}', data['GIT_DESCRIBE_HASH'])
    g('checkout', data['GIT_DESCRIBE_HASH'])
