import pathlib

import click
import pytest

from own_conda_tools.commands import upload


BASE_DIR = pathlib.Path(__file__).parent.parent


@pytest.mark.parametrize('ci, cwd', [
    (None, BASE_DIR),
    (None, 'tmp'),
    ('https://gitlab.com/ownconda/ownconda-tools', BASE_DIR),
    ('https://gitlab.com/ownconda/ownconda-tools/', BASE_DIR),
    ('https://gitlab.com/ownconda/ownconda-tools', 'tmp'),
])
def test_assert_upload_allowed(ci, cwd, monkeypatch, tmp_path):
    if ci is None:
        monkeypatch.delenv('CI_PROJECT_URL', raising=False)
    else:
        monkeypatch.setenv('CI_PROJECT_URL', ci)
    monkeypatch.chdir(tmp_path if cwd == 'tmp' else cwd)

    upload.assert_upload_allowed()


@pytest.mark.parametrize('ci, cwd', [
    ('http://gitlab.com/ownconda/ownconda-tools', BASE_DIR),
    ('https://gitlab.com/ownconda/another_project', BASE_DIR),
])
def test_assert_upload_allowed_fail(ci, cwd, monkeypatch, tmp_path):
    if ci is None:
        monkeypatch.delenv('CI_PROJECT_URL', raising=False)
    else:
        monkeypatch.setenv('CI_PROJECT_URL', ci)
    monkeypatch.chdir(tmp_path if cwd == 'tmp' else cwd)
    with pytest.raises(click.UsageError):
        upload.assert_upload_allowed()
