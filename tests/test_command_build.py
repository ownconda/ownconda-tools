import itertools
import pathlib

import pytest

from own_conda_tools import constants
from own_conda_tools.commands import build


ROOTS = [
    [''],
    ['pkg-a', 'pkg-b'],
]
CHANNELS = [
    None,
    f'{constants.DEFAULT_CHANNEL}',
]


def test_get_build_dir_env(monkeypatch):
    """Retrieve build dir when $CONDA_BLD_PATH is set"""
    monkeypatch.setenv('CONDA_BLD_PATH', 'spam')
    path = build.get_build_dir(pathlib.Path('/eggs'))
    assert path == pathlib.Path('spam')


def test_get_build_dir_no_env():
    """Get default build dir w/o env var."""
    path = build.get_build_dir(pathlib.Path('/eggs'))
    assert path.name == 'conda-bld'


@pytest.mark.parametrize('root, channel', itertools.product(ROOTS, CHANNELS))
def test_build(
        root, channel, invoke, recipe_dir, expected_packages, monkeypatch, tmp_path
):
    if channel is not None:
        channel = tmp_path / channel
        channel.joinpath('noarch').mkdir(parents=True)
        import subprocess
        subprocess.run(['conda', 'index', str(channel / 'noarch')])
        channel = channel.as_uri()
    build_dir = tmp_path / 'conda-bld'
    monkeypatch.setenv('CI_PROJECT_DIR', str(recipe_dir))
    monkeypatch.setenv('CONDA_BLD_PATH', str(build_dir))

    cmd = ['build']
    if channel:
        cmd.append(f'--channel={channel}')
    root = [str(recipe_dir.joinpath(r)) for r in root]
    cmd.extend(root)
    invoke(cmd)

    assert build_dir.joinpath('noarch', 'repodata.json').is_file()
    assert tmp_path.joinpath('artifacts', 'noarch', 'repodata.json').is_file()
    for pkg in expected_packages:
        assert build_dir.joinpath('linux-64', pkg).is_file()
        assert tmp_path.joinpath('artifacts', 'linux-64', pkg).is_file()


@pytest.mark.parametrize('channel', [
    'spam',
    'http://spam.eggs.bacon/',
    f'{constants.DEFAULT_REPOSITORY}/spam/eggs',
])
def test_build_invalid_channel(channel, invoke, recipe_dir):
    with pytest.raises(AssertionError):
        result = invoke(['build', '--channel', channel, str(recipe_dir)])
        assert 'Invalid value for "--channel"' in result.output
        assert result.exit_code == 2
