# pylint: disable=line-too-long, protected-access,redefined-outer-name,unused-argument
import hashlib
import os.path

import requests
import pytest

from own_conda_tools import pypi


DUMMY_PKG_DIR = os.path.join(os.path.dirname(__file__), 'data')
DUMMY_PKG = os.path.join(DUMMY_PKG_DIR, 'aiomas.tar.gz')
DUMMY_PKG = open(DUMMY_PKG, 'rb').read()
EXPECTED_SHA256 = hashlib.sha256(DUMMY_PKG).hexdigest()
# Dummy PyPI reply for "aiomas"
REPLY = {
    "info": {
        "home_page": "https://aiomas.readthedocs.io",
        "license": "MIT",
        "name": "aiomas",
        "summary": "Asyncio-based, layered networking library providing request-reply channels, RPC, and multi-agent systems.",
        "version": "1.1.0b1"
    },
    "releases": {
        "1.0.0": [
            {
                "filename": "aiomas-1.0.0.7z",
                "packagetype": "sdist",
                "digests": {
                    "md5": "8aad766b1689bcfb36a6874ed30550e0",
                    "sha256": EXPECTED_SHA256
                },
                "url": "https://files.pythonhosted.org/packages/92/84/a7424e3f2e73a9e7155b2dd3d4ba7a1c704d708b7c9d8890359f42a5b771/aiomas-1.0.0.7z"
            }
        ],
        "1.0.1": [
            {
                "digests": {
                    "md5": "16f5b6d7a6f1cbad27557430c66f04d4",
                    "sha256": EXPECTED_SHA256
                },
                "filename": "aiomas-1.0.1-py2.py3-none-any.whl",
                "packagetype": "bdist_wheel",
                "url": "https://files.pythonhosted.org/packages/18/9a/54820bbdfd78065b320a25c8e90bde36667997cf0b56d744462bd5a6a99a/aiomas-1.0.1-py2.py3-none-any.whl"
            },
            {
                "filename": "aiomas-1.0.1.zip",
                "packagetype": "sdist",
                "digests": {
                    "md5": "8aad766b1689bcfb36a6874ed30550e0",
                    "sha256": EXPECTED_SHA256
                },
                "url": "https://files.pythonhosted.org/packages/92/84/a7424e3f2e73a9e7155b2dd3d4ba7a1c704d708b7c9d8890359f42a5b771/aiomas-1.0.1.zip"
            }
        ],
        "1.0.2": [
            {
                "digests": {
                    "md5": "8aad766b1689bcfb36a6874ed30550e0",
                    "sha256": EXPECTED_SHA256
                },
                "filename": "aiomas-1.0.2-py2.py3-none-any.whl",
                "packagetype": "bdist_wheel",
                "url": "https://files.pythonhosted.org/packages/92/84/a7424e3f2e73a9e7155b2dd3d4ba7a1c704d708b7c9d8890359f42a5b771/aiomas-1.0.2-py2.py3-none-any.whl"
            },
            {
                "filename": "aiomas-1.0.2.zip",
                "packagetype": "sdist",
                "digests": {
                    "md5": "8aad766b1689bcfb36a6874ed30550e0",
                    "sha256": EXPECTED_SHA256
                },
                "url": "https://files.pythonhosted.org/packages/92/84/a7424e3f2e73a9e7155b2dd3d4ba7a1c704d708b7c9d8890359f42a5b771/aiomas-1.0.2.zip"
            },
            {
                "digests": {
                    "md5": "c4ce68643d7c89cf3334f17c0b3a2a0e",
                    "sha256": EXPECTED_SHA256
                },
                "filename": "aiomas-1.0.2.tar.gz",
                "packagetype": "sdist",
                "url": "https://files.pythonhosted.org/packages/ec/78/e4e1f943dc188d368ec9a30d072608ed0dddb523fe78244031eca715d933/aiomas-1.0.2.tar.gz"
            }
        ],
        "1.1.0b1": [
            {
                "digests": {
                    "md5": "ed369d866b6f9139b3594a109a08b2e8",
                    "sha256": EXPECTED_SHA256
                },
                "filename": "aiomas-1.1.0b1-py2.py3-none-any.whl",
                "packagetype": "bdist_wheel",
                "url": "https://files.pythonhosted.org/packages/e5/0c/c47831973c4fd5d751a176c98da23c492867fffca7c86593c556067e5c7c/aiomas-1.1.0b1-py2.py3-none-any.whl"
            },
        ]
    },
}


@pytest.fixture
def mock_requests(monkeypatch):
    def requests_get(url):
        if 'aiomas' in url:
            pkg = 'aiomas.tar.gz'
        elif 'numpy' in url:
            pkg = 'numpy.tar.gz'
        else:
            pkg = None

        class Reply:
            status_code = 404 if pkg is None else 200
            if pkg is not None:
                content = open(os.path.join(DUMMY_PKG_DIR, pkg), 'rb').read()
            @staticmethod
            def json():
                return REPLY

        return Reply()

    monkeypatch.setattr(requests, 'get', requests_get)


def test_get_pkg_info(mock_requests):
    data = pypi.get_pkg_info('aiomas')
    assert data == REPLY


def test_get_pkg_info_error(mock_requests):
    pytest.raises(pypi.PypiError, pypi.get_pkg_info, 'spam')


@pytest.mark.parametrize('allow_pre, expected', [
    (True, '1.1.0b1'),
    (False, '1.0.2'),
])
def test_get_latest_version(allow_pre, expected, mock_requests):
    v = pypi.get_latest_version('aiomas', allow_pre)
    assert v == expected


@pytest.mark.parametrize('version, expected', [
    ('0.9', pypi.PypiError),  # No sdist available
    ('1.1.0b1', pypi.PypiError),  # No sdist available
    ('1.0.0', pypi.PypiError),  # No sdist available
    # ('1.0.1', {
    #     'version': '1.0.1',
    #     'fn': 'aiomas-1.0.1.zip',
    #     'url': 'https://files.pythonhosted.org/packages/92/84/a7424e3f2e73a9e7155b2dd3d4ba7a1c704d708b7c9d8890359f42a5b771/aiomas-1.0.1.zip',
    #     'sha256': EXPECTED_SHA256,
    #     'summary': 'Asyncio-based, layered networking library providing '
    #                'request-reply channels, RPC, and multi-agent systems.',
    #     'home': 'https://aiomas.readthedocs.io',
    #     'license': 'MIT',
    #     'install_requires': ['eggs', 'spam >=2'],
    #     'packages': ['aiomas', 'aiomas.channel'],
    #     'entry_points': [],
    # }),
])
def test_get_release(version, expected, mock_requests):
    if isinstance(expected, dict):
        v = pypi.get_release('aiomas', version)
        assert v == expected
    else:
        pytest.raises(expected, pypi.get_release, 'aiomas', version)


@pytest.mark.parametrize('allow_pre, expected', [
    (True, pypi.PypiError),  # No sdist available
    (False, {
        'version': '1.0.2',
        'fn': 'aiomas-1.0.2.tar.gz',
        'url': 'https://files.pythonhosted.org/packages/ec/78/e4e1f943dc188d368ec9a30d072608ed0dddb523fe78244031eca715d933/aiomas-1.0.2.tar.gz',
        'sha256': EXPECTED_SHA256,
        'summary': 'Asyncio-based, layered networking library providing '
                   'request-reply channels, RPC, and multi-agent systems.',
        'home': 'https://aiomas.readthedocs.io',
        'license': 'MIT',
        'build_requirements': [],
        'run_requirements': ['eggs', 'spam >=2'],
        'packages': ['aiomas', 'aiomas.channel'],
        'entry_points': [],
    }),
])
def test_get_latest_release(allow_pre, expected, mock_requests):
    if isinstance(expected, dict):
        v = pypi.get_latest_release('aiomas', allow_prerelease=allow_pre)
        assert v == expected
    else:
        pytest.raises(expected, pypi.get_latest_release, 'aiomas',
                      allow_prerelease=allow_pre)


@pytest.mark.parametrize('setup_kwargs, expected_ep', [
    ({}, []),
    ({'entry_points': {'spam': 'eggs'}}, []),
    ({'entry_points': '[spam]\neggs = yummy\n'}, []),
    ({'entry_points': {'console_scripts': ['spam = spam:main']}},
     ['spam = spam:main']),
    ({'entry_points': """
        [console_scripts]
        spam = spam:main
        """},
     ['spam = spam:main']),
])
def test_get_entry_points(setup_kwargs, expected_ep):
    ep = pypi._get_entry_points(setup_kwargs)
    assert ep == expected_ep
