# pylint: disable=redefined-outer-name
from collections import namedtuple as nt
import traceback

import click.testing
import pytest

import own_conda_tools.__main__
import own_conda_tools.constants as constants
import own_conda_tools.recipes

#
# Recipe data
#
R_PKG_A = """
{% set v = '1.2.3' %}

package:
  name: pkg-a
  version: {{ v }}

build:
  script: touch pkg-a.txt

requirements:
  build:
    - pip >=3.5
  run:
    - pkg-b-client

test:
  requires:
    - pytest
"""
R_PKG_B_SERVER = """
package:
  name: pkg-b-server
  version: '3.3'

build:
    script: touch pkg-b-server.txt
"""
R_PKG_B_CLIENT = """
package:
  name: pkg-b-client
  version: '3.2'

build:
  script: touch pkg-b-client.txt

requirements:
  run:
    - requests

test:
  requires:
"""

Recipe = nt('Recipe', 'dirname, pkgfile, meta')  # pylint: disable=invalid-name
RECIPES = {
    'pkg-a': Recipe(
        'pkg-a/conda', 'pkg-a-1.2.3-0.tar.bz2', R_PKG_A
    ),
    'pkg-b-client': Recipe(
        'pkg-b/conda/client', 'pkg-b-client-3.2-0.tar.bz2', R_PKG_B_CLIENT
    ),
    'pkg-b-server': Recipe(
        'pkg-b/conda/server', 'pkg-b-server-3.3-0.tar.bz2', R_PKG_B_SERVER
    ),
}


@pytest.fixture(autouse=True)
def in_pytest(monkeypatch):
    monkeypatch.setattr(constants, 'in_test', lambda: True)


@pytest.fixture
def recipe_dir(tmp_path):
    """Return the direcotry containing some Conda recipes."""
    # Copy recipes to temp. dir so that we do not pollute our working copy
    # with build artifacts:
    for recipe in RECIPES.values():
        tmp_path.joinpath(recipe.dirname).mkdir(parents=True, exist_ok=True)
        tmp_path.joinpath(recipe.dirname, 'meta.yaml').write_text(recipe.meta)
    return tmp_path


@pytest.fixture
def e_nodes(request, recipe_dir):
    """Update the "path" item of the nodes with the actual recipe dir."""
    nodes = request.param
    for node, data in nodes.items():
        if node in RECIPES:
            data['path'] = recipe_dir / RECIPES[node].dirname
            data['meta'] = own_conda_tools.recipes.load_yaml(RECIPES[node].meta)
    return nodes


@pytest.fixture
def expected_packages():
    """Return a list of the packages names.  See :func:`recipe_dir()`."""
    return [r.pkgfile for r in RECIPES.values()]


@pytest.fixture
def invoke():
    """Return a :func:`functools.partial()` for invoking a sub command."""
    runner = click.testing.CliRunner()

    def _invoke(*args, **kwargs):
        result = runner.invoke(own_conda_tools.__main__.cli, *args, **kwargs)
        if result.exit_code != 0:
            print('Invocation failed:')
            print('- Exit code:', result.exit_code)
            print('- Output:', result.output)
            print('- Exception:', result.exception)
            print('- Exc. info:', ''.join(traceback.format_exception(*result.exc_info)))
        assert result.exit_code == 0
        return result

    return _invoke
