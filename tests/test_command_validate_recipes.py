# pylint: disable=line-too-long,redefined-outer-name
import click
import pytest

from own_conda_tools.__main__ import cli
from own_conda_tools.recipes import load_recipes


RECIPES = [
    """

# Valid with single source
package:
  name: test
  version: '1.2'
source:
  url: url
  sha256: abc123
build:
  number: 0
  missing_dso_whitelist:
    - '*/libc.so.6'
about:
  home: 🏠
  license: MIT
valid: True
""",
    """

# Valid with multiple sources
package:
  name: test
  version: '1.2'
source:
  - url: url
    sha256: abc123
  - git_url: url
    git_rev: master
build:
  number: 0
about:
  home: 🏠
  license: MIT
valid: True
""",
    """

# Valid without sources
package:
  name: test
  version: '1.2'
build:
  number: 0
about:
  home: 🏠
  license: MIT
valid: True
""",
    """

# Invalid recipe with a lot of missing keys
valid: False
stdout: |-
  Missing keys:
  - package:name
  - package:version
  - build:number
  - about:home
  - about:license
  Version cannot be parsed

""",
    """

# Invalid with broken source
package:
  name: test
  version: '1.2'
source:
  sha256: abc123
build:
  number: 0
about:
  home: 🏠
  license: MIT
valid: False
stdout: |-
  Exactly one of the following keys is allowed per source: ['git_url', 'hg_url', 'url'].  Found: []
""",
    """

# Invalid with broken source
package:
  name: test
  version: '1.2'
source:
  url: a
  git_url: b
build:
  number: 0
about:
  home: 🏠
  license: MIT
valid: False
stdout: |-
  Exactly one of the following keys is allowed per source: ['git_url', 'hg_url', 'url'].  Found: ['git_url', 'url']
""",
    """

# Invalid with missing source hash
package:
  name: test
  version: '1.2'
source:
  - url: a
  - git_url: b
  - hg_url: c
build:
  number: 0
about:
  home: 🏠
  license: MIT
valid: False
stdout: |-
  Missing keys:
  - source:[0]:sha256
  - source:[1]:git_rev
  - source:[2]:hg_rev
""",
    """

# Invalid with invalid DSO
package:
  name: test
  version: '1.2'
build:
  number: 0
  missing_dso_whitelist:
    - '*/spam.so.1'
about:
  home: 🏠
  license: MIT
valid: False
stdout: |-
  DSO not in whitelist: spam.so.1
""",
    """

# Invalid with broken version
package:
  name: test
  version: 1.2
build:
  number: 0
about:
  home: 🏠
  license: MIT
valid: False
stdout: |-
  Version cannot be parsed
""",
]


@pytest.fixture()
def recipe(request, tmp_path):
    """Write recipe strings into yaml files."""
    tmp_path.joinpath('meta.yaml').write_text(request.param)
    return tmp_path


@pytest.mark.parametrize('recipe', RECIPES, indirect=['recipe'])
def test_validate(recipe):
    data, _meta_yaml_path = next(load_recipes(recipe))
    runner = click.testing.CliRunner()
    result = runner.invoke(cli, ['validate-recipes', str(recipe)])
    if data['valid']:
        assert result.stdout == ''
        assert result.exit_code == 0
    else:
        assert result.stdout == f'{recipe}\n{data["stdout"]}\n\n'
        assert result.exit_code == 1
