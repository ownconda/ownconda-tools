# pylint: disable=redefined-outer-name
import subprocess
import sys

import click
import pytest

import own_conda_tools.__main__


COMMANDS = {
    'build',
    'check-for-updates',
    'ci',
    'completion',
    'dep-graph',
    'develop',
    'gitlab',
    'lint',
    'make-docs',
    'prune-index',
    'pylintrc',
    'pypi-recipe',
    'sec-check',
    'show-updated-recipes',
    'test',
    'update-recipes',
    'upload',
    'validate-recipes',
}


@pytest.fixture(params=['click', 'subprocess'])
def main_output(request, invoke):
    if request.param == 'click':
        # Invoke __main__ via click
        result = invoke(['--help'])
        output = result.output
    else:
        # Invoke __main__ by executing the module
        output = subprocess.check_output(
            [sys.executable, '-m', 'own_conda_tools', '--help'],
            universal_newlines=True,
        )

    return iter(output.split('\n'))


def test_main(main_output):
    for line in main_output:
        if 'Commands:' in line:
            break

    commands_found = set()
    for line in main_output:
        cmd = line.strip().split(' ')[0]
        if cmd:
            commands_found.add(cmd)

    assert commands_found == COMMANDS


def test_main_unkown_command():
    runner = click.testing.CliRunner()
    result = runner.invoke(own_conda_tools.__main__.cli, 'spam')
    assert result.exit_code == 2
