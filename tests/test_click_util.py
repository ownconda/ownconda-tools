# pylint: disable=redefined-outer-name
import enum
import http.server
import os
import pathlib
import sys
import threading

import click
import pytest
import requests

from own_conda_tools import click_util, constants, util


def mock_get_channel(url):
    channels = [
        constants.STABLE_CHANNEL,
        constants.STAGING_CHANNEL,
        constants.TESTING_CHANNEL,
        constants.EXPERIMENTAL_CHANNEL,
    ]
    channels = [f'{constants.DEFAULT_REPOSITORY}/{c}' for c in channels]
    response = requests.Response()
    response.status_code = 200 if url in channels else 404
    return response


@pytest.fixture()
def http_server():
    class ServerRequestHandler(http.server.BaseHTTPRequestHandler):
        def do_POST(self):  # pylint: disable=invalid-name
            self.send_response(requests.codes.ok)  # pylint: disable=no-member
            self.end_headers()

    addr = ('127.0.0.1', util.get_unused_tcp_port())
    server = http.server.HTTPServer(addr, ServerRequestHandler)
    thread = threading.Thread(target=server.serve_forever)
    thread.setDaemon(True)
    thread.start()
    yield addr
    server.shutdown()
    thread.join()


@pytest.mark.parametrize('pipeline, testing, color, expected', [
    (False, False, None, None),
    (False, True, None, False),
    (True, False, None, True),
    (True, True, None, False),
    (False, False, 'spam', 'spam'),
    (False, True, 'spam', 'spam'),
    (True, False, 'spam', 'spam'),
    (True, True, 'spam', 'spam'),
])
def test_echo(pipeline, testing, color, expected, monkeypatch):
    def secho(_msg, **kwargs):
        if expected is None:
            assert 'color' not in kwargs
        else:
            assert kwargs['color'] == expected

    monkeypatch.setattr(click, 'secho', secho)
    monkeypatch.setattr(constants, 'in_pipeline', lambda: pipeline)
    monkeypatch.setattr(constants, 'in_test', lambda: testing)

    kwargs = {}
    if color is not None:
        kwargs['color'] = color
    click_util.echo('', **kwargs)


@pytest.mark.parametrize('err, expected', [
    (None, True),
    ('spam', 'spam'),
])
def test_status(err, expected, monkeypatch):
    def echo(_msg, **kwargs):
        assert kwargs['err'] == expected

    monkeypatch.setattr(click_util, 'echo', echo)

    kwargs = {}
    if err is not None:
        kwargs['err'] = err
    click_util.status('', **kwargs)


@pytest.mark.parametrize('func, expected', [
    (click_util.action, {'bold': True, 'nl': False}),
    (click_util.ok, {'bold': True, 'fg': 'green'}),
    (click_util.info, {'bold': True, 'fg': 'blue'}),
    (click_util.warning, {'bold': True, 'fg': 'yellow'}),
    (click_util.error, {'bold': True, 'fg': 'red'}),
])
def test_special_status(func, expected, monkeypatch):
    def mock(_msg, **kwargs):
        assert kwargs == expected

    monkeypatch.setattr(click_util, 'status', mock)
    func('spam')


def test_fatal_error(monkeypatch):
    exit_called = []

    def exit_mock(code):
        exit_called.append(code)

    def mock(_msg, **kwargs):
        assert kwargs == {'bold': True, 'fg': 'red'}

    monkeypatch.setattr(click_util, 'status', mock)
    monkeypatch.setattr(sys, 'exit', exit_mock)
    click_util.fatal_error('spam')
    assert exit_called == [1]


def test_action_cm(capsys):
    with click_util.Action('Here we go ...'):
        pass

    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'Here we go ... OK\n'


def test_action_cm_status(capsys):
    with click_util.Action('Go', nl=True) as action:
        action.progress()
        action.warning('warn')
        action.error('err')

    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'Go\n. warn\n err\n'


def test_action_cm_set_ok_message(capsys):
    with click_util.Action('Go', ok_message='ended'):
        pass

    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'Go ended\n'

    with click_util.Action('Go') as action:
        action.set_ok_message('stop')

    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'Go stop\n'


def test_action_cm_fatal_error(capsys):
    with pytest.raises(SystemExit):
        with click_util.Action('Go') as action:
            action.fatal_error('onoes')

    out, err = capsys.readouterr()
    assert out == ''
    assert err == 'Go onoes\n'


def test_action_cm_exception(capsys):
    with pytest.raises(SystemExit):
        with click_util.Action('Go'):
            raise ValueError('onoes')

    out, err = capsys.readouterr()
    err = err.strip().splitlines()
    assert out == ''
    assert err[0] == 'Go EXCEPTION OCCURRED:'
    assert err[-1] == 'ValueError: onoes'


def test_type_path():
    @click.command()
    @click.argument('path', type=click_util.Path())
    def cli(path):
        assert isinstance(path, pathlib.Path)

    runner = click.testing.CliRunner()
    result = runner.invoke(cli, '/spam')
    assert result.exit_code == 0


def test_type_enum():
    class Spam(enum.Enum):
        a = 1
        b = 2

    @click.command()
    @click.argument('choice', type=click_util.Enum(Spam))
    def cli(choice):
        assert choice is Spam.a

    runner = click.testing.CliRunner()
    result = runner.invoke(cli, 'a')
    assert result.exit_code == 0


@pytest.mark.parametrize('channel', [
    [],
    ['staging'],
    [f'file://{os.path.abspath(".")}'],
    [f'{constants.DEFAULT_REPOSITORY}/stable'],
    ['staging', f'{constants.DEFAULT_REPOSITORY}/stable'],
])
def test_channel_callback(channel, monkeypatch):
    monkeypatch.setattr(requests, 'get', mock_get_channel)
    result = click_util._channel_callback(None, None, channel)  # pylint: disable=protected-access
    expected = tuple(f'--channel={c}' for c in channel)
    assert result == expected


@pytest.mark.parametrize('channel', [
    ['spameggsbacon'],
    [f'file:///spameggsbacon'],
    ['https://spameggsbacon.com'],
    ['https://forge.services.own/conda/spameggsbacon'],
    ['staging', 'spammeggsbacon'],
])
def test_channel_callback_error(channel, monkeypatch):
    monkeypatch.setattr(requests, 'get', mock_get_channel)
    with pytest.raises(click.BadParameter):
        click_util._channel_callback(None, None, channel)  # pylint: disable=protected-access


def test_argument_paths():
    func = click_util.argument.paths(dir_okay=False)(lambda c: True)
    arg = func.__click_params__[0]
    assert arg.nargs == -1
    assert arg.type.dir_okay is False


def test_argument_recipe_root():
    func = click_util.argument.recipe_root()(lambda c: True)
    arg = func.__click_params__[0]
    assert arg.nargs == -1
    assert arg.type.dir_okay is True
    assert arg.type.file_okay is False


def test_option_artifacts_dir():
    func = click_util.option.artifacts_dir()(lambda c: True)
    opt = func.__click_params__[0]
    assert isinstance(opt.type, click.Path)
    assert opt.type.dir_okay is True
    assert opt.type.file_okay is False
    assert opt.default == constants.artifacts_dir()


@pytest.mark.parametrize('do, vals, staging', [
    (False, None, False),
    (False, ('branch', ''), False),
    (True, ('master', ''), False),
    (True, ('tag', 'tag'), False),
    (True, ('branch', ''), True),
])  # pylint: disable=invalid-name
def test_option_channel_auto_staging(do, vals, staging, monkeypatch):
    monkeypatch.delenv('CONDA_CHANNELS', raising=False)
    if vals is None:
        monkeypatch.setattr(constants, 'in_pipeline', lambda: False)
        monkeypatch.delenv('CI_COMMIT_REF_NAME', raising=False)
        monkeypatch.delenv('CI_COMMIT_TAG', raising=False)
    else:
        monkeypatch.setattr(constants, 'in_pipeline', lambda: True)
        monkeypatch.setenv('CI_COMMIT_REF_NAME', vals[0])
        monkeypatch.setenv('CI_COMMIT_TAG', vals[1])

    expected = ['staging'] if staging else []

    func = click_util.option.channel(auto_ci_staging=do)(lambda c: True)
    opt = func.__click_params__[0]
    assert opt.default == expected


def test_option_channel_autostaging_remove_duplicates(monkeypatch):  # pylint: disable=invalid-name
    monkeypatch.setenv('CONDA_CHANNELS', 'testing,staging')
    monkeypatch.setattr(constants, 'in_pipeline', lambda: True)
    monkeypatch.setenv('CI_COMMIT_REF_NAME', 'develop')
    monkeypatch.setenv('CI_COMMIT_TAG', '')

    func = click_util.option.channel(auto_ci_staging=True)(lambda c: True)
    opt = func.__click_params__[0]
    assert opt.default == ['testing', 'staging']


def test_option_channel_from_env(monkeypatch):
    monkeypatch.setenv('CONDA_CHANNELS', 'testing,staging')
    func = click_util.option.channel()(lambda c: True)
    opt = func.__click_params__[0]
    assert opt.default == ['testing', 'staging']


def test_option_dry_run():
    func = click_util.option.dry_run()(lambda c: True)
    opt = func.__click_params__[0]
    assert opt.is_flag
    assert opt.default is False


def test_otion_gui():
    func = click_util.option.gui()(lambda c: True)
    opt = func.__click_params__[0]
    assert opt.is_flag
    assert opt.default is False
    assert opt.callback(None, opt, False) == []
    assert opt.callback(None, opt, True)[0] == 'xvfb-run'


def test_option_python_single():
    func = click_util.option.python()(lambda c: True)
    opt = func.__click_params__[0]
    assert opt.multiple is False
    assert opt.default == constants.PYTHON_VERSION


def test_option_python_multi():
    func = click_util.option.python(multiple=True)(lambda c: True)
    opt = func.__click_params__[0]
    assert opt.multiple is True
    assert opt.default == constants.PYTHON_VERSIONS


def test_option_recipe_dir():
    func = click_util.option.recipe_dir()(lambda c: True)
    opt = func.__click_params__[0]
    assert isinstance(opt.type, click_util.Path)
    assert opt.type.dir_okay is True
    assert opt.type.file_okay is False
    assert opt.default == constants.DEFAULT_RECIPE_DIR


def test_option_url(http_server):
    host, port = http_server
    expected_url = f'http://{host}:{port}'

    @click.command()
    @click_util.option.url('spam')
    def cli(url):
        assert url == expected_url

    runner = click.testing.CliRunner()
    result = runner.invoke(cli)
    assert result.exit_code == 2
    assert 'Invalid value for "--url": Channel "spam" not available' in result.output

    runner = click.testing.CliRunner()
    result = runner.invoke(cli, f'--url={expected_url}')
    assert result.exit_code == 0


def test_option_url_default(http_server):
    host, port = http_server
    default_url = f'http://{host}:{port}'

    @click.command()
    @click_util.option.url(default_url)
    def cli(url):  # pylint: disable=unused-argument
        assert url == default_url

    runner = click.testing.CliRunner()
    result = runner.invoke(cli)
    assert result.exit_code == 0


def test_option_url_none_okay(http_server):
    host, port = http_server
    url = f'http://{host}:{port}'

    @click.command()
    @click_util.option.url(url)
    def failcli(url):  # pylint: disable=unused-argument
        pytest.fail('Click should have failed earlier')

    runner = click.testing.CliRunner()
    result = runner.invoke(failcli, '--url=None')
    assert result.exit_code == 2

    @click.command()
    @click_util.option.url(url, none_okay=True)
    def cli(url):
        assert url is None

    runner = click.testing.CliRunner()
    result = runner.invoke(cli, '--url=None')
    assert result.exit_code == 0
