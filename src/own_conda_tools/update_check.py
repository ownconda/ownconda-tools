"""
Update checks for Conda recipes.

Check if updates are available for the recipes within a given folder.

Every recipe must contain a file called ``update_check.py``.  This module must
at least define a checker: ``checker = '<checker-name>'``.  Depending on the
checker, you can/must define addional names.


Available checkers
------------------

- **pypi:** Check PyPI, useful for most Python packages

- **github:** Check a project's release page on GitHub

- **dirlisting:** Check a directory listing via *http(s)* or *ftp*

- **webbot:** Crawl a website for the latest version

- **custom:** Can be used for all remaining cases


pypi
^^^^

This checker fetches the lateset package version from the `Python Package Index
<https://pypi.python.org/pypi>`_.

By default, it looks for *package/name* defined in the recipe's ``meta.yaml``.
You can optionally override the package name if the spelling differs:

.. code-block:: python

    checker = 'pypi'

    # Optional:
    package_name = 'PyYAML'  # PyPI is case-sensitive
    allow_prereleases = True  # Allow pre-releases (like betas or RCs)


github
^^^^^^

This checker scans the project releases on GitHub for the release labled
*Latest Release*.

You need to provide a *url* and, optionally, a *regex* for the version string.
By default, versions must match the schema *major.minor[.patch]*:

.. code-block:: python

    checker = 'github'
    url = 'https://github.com/conda/conda/releases'

    # Optional:
    regex = r'(\\d+\\.\\d+\\.\\d+)'  # "match.group(1)" is used as version
    tag_timeline = True  # Use the "Tags" timeline instead of "Releases" timeline


dirlisting
^^^^^^^^^^

Scan an online directory listing (like `freetype <>`_ or `libxml2 <>_` for
the latest version.  Both, *http(s)* and *ftp*, are supported.

You need to provide a *url* and a regular expression *regex* for extracting
the version from a filename:

.. code-block:: python

    checker = 'dirlisting'
    url = 'ftp://xmlsoft.org/libxml2/'
    regex = r'libxml2-(\\d+\\.\\d+(\\.\\d+)?).tar.gz'


webbot
^^^^^^

Crawl a website for the latest version.

You need to specify a *url* and a function *find_version(soup)*.  That function
will receive a `BeautifulSoup
<https://www.crummy.com/software/BeautifulSoup/bs4/doc/>`_ instance with the
contents of the specified website.  It needs to return the version string:

.. code-block:: python

    # python.org makes it quite easy to extract the version number:
    checker = 'webbot'
    url = 'https://www.python.org/downloads/'

    def find_version(soup):
        text = soup.find('p', class_='download-buttons').find('a').text
        version = text.split(' ')[-1]
        return version

    # For "xz", it's a bit more complicated:
    checker = 'webbot'
    url = 'http://tukaani.org/xz/'

    import re

    def find_version(soup):
        # Find version from this paragraph:
        # <h3>Stable</h3>
        # <p>
        # 5.2.2 was released on 2015-09-29.
        # </p>
        h3 = soup.find('h3', string='Stable')
        for elem in h3.next_siblings:
            if elem.name == 'p':
            text = elem.text
            break

        version = re.search(r'(\\d+\\.\\d+\\.\\d+)', text).group(1)
        return version


custom
^^^^^^

For more complicated cases (e.g., when you have to scan multiple websites),
you implement a custom version checker.

You need to define a function (or coroutine) *get_latest_version()* which
returns the version string:

.. code-block:: python

    checker = 'custom'

    def get_latest_version():
        version = _find_version()
        return version

    # or
    # async def get_latest_version():
    #     version = await _find_version()
    #     return version

You should take a look at ``qt5/update_check.py`` and
``readline/update_check.py`` to get some inspiration.

"""
import ftplib
import functools
import importlib.util
import inspect
import re
import urllib.parse

import bs4
import requests
import trio

from . import pypi
from .version import parse as parse_version


# Helper to run sync. IO in a background thread
# pylint: disable=invalid-name
run_async = trio.run_sync_in_worker_thread
async_get = functools.partial(trio.run_sync_in_worker_thread, requests.get)
# pylint: enable=invalid-name


#
# Checkers - these functions implement the checkers described above.
#
# All checkers have the same signature:
#
# - *checker* is a recipe's "update_check" module.
# - *pkg_name* is the package name as defined in the recipe's "meta.yaml".
# - The return value is a "Version" instance returned by "parse_version()".
#
async def query_pypi(checker, pkg_name):
    """Query PyPI for the latest version."""
    # Override "pkg_name"?
    if hasattr(checker, 'package_name') and checker.package_name:
        pkg_name = checker.package_name

    pre = getattr(checker, 'allow_prereleases', False)
    latest_version = await run_async(pypi.get_latest_version, pkg_name, pre)
    if not latest_version:
        raise RuntimeError(f'Could not find a release for "{pkg_name}"') \
            from None

    latest_version = parse_version(latest_version)
    return latest_version


async def check_github(checker, pkg_name):  # pylint: disable=unused-argument
    """Extract version labeled as "Latest Release" from GitHub."""
    # Get version regex and compile it
    if hasattr(checker, 'regex'):
        regex = checker.regex
    else:
        regex = r'(\d+\.\d+(\.\d+)?)'
    re_version = re.compile(regex)

    # Get the project's release page
    r = await async_get(checker.url)
    soup = bs4.BeautifulSoup(r.text, 'html.parser')

    # Find the latest release and extract the version
    if getattr(checker, 'tag_timeline', False):
        # Simple release timeline without labels
        versions = []
        for tag in soup.find_all('h4', class_='commit-title'):
            link = tag.find('a')
            match = re_version.search(link.text)
            if match is not None:
                versions.append(parse_version(match.group(1)))
        latest_version = sorted(versions)[-1]
    else:
        # Use the "latest" label to find the latest release
        div_latest = soup.find('div', class_='label-latest')
        v_version = div_latest.find('ul').find('a')['title']  # e.g. v2.3.1
        latest_version = parse_version(re_version.search(v_version).group(1))

    return latest_version


async def search_directory_listing(checker, pkg_name):  # pylint: disable=unused-argument
    """Scan an online directory listing."""
    re_files = re.compile(checker.regex)
    url = urllib.parse.urlparse(checker.url)

    # First, get *all* filesnames from the web/FTP server:
    files = []
    if url.scheme == 'ftp':
        # Use "ftplib" for "ftp://"
        ftp = ftplib.FTP(url.netloc)
        ftp.login()
        ftp.cwd(url.path)
        ftp.retrlines('NLST', files.append)
    else:
        # Use "requests" for everything else
        r = await async_get(checker.url)
        soup = bs4.BeautifulSoup(r.text, 'html.parser')
        table = soup.find('table')
        if table:
            row_list = table('tr')
            for row in row_list:
                td = row('td')
                if not td:
                    continue  # Ignore header (th) rows

                # The filename should be in the second column, but it may also
                # be in the first one:
                try:
                    text = td[1].find('a').text
                except AttributeError:
                    text = td[0].find('a').text
                files.append(text)
        else:
            # Try simple Nginx directoy listings like
            # https://support./ftp/HDF5/current18/src/
            for entry in soup.find('ul')('li'):
                files.append(entry.find('a').text.strip())

    # Now filter the filenames and get the latest version number:
    latest_version = parse_version('0')
    for f in files:
        match = re_files.match(f)
        if match is not None:
            version = parse_version(match.group(1))
            latest_version = max(version, latest_version)

    return latest_version


async def search_website(checker, pkg_name):  # pylint: disable=unused-argument
    """Extract the version string from an arbitrary website."""
    # Retrieve the website and make a beautiful soup of it:
    r = await async_get(checker.url)
    soup = bs4.BeautifulSoup(r.text, 'html.parser')

    # Let the checker find the latest version:
    latest_version = parse_version(checker.find_version(soup))
    return latest_version


async def call_custom_func(checker, pkg_name):  # pylint: disable=unused-argument
    """When none of the above checkers work, this function provides maximum
    flexibility."""
    # Check whether "get_latest_version()" is a normal function or corouine
    # and run it appropriately:
    func = checker.get_latest_version
    if inspect.iscoroutinefunction(func):
        task = func()
    else:
        task = run_async(func)
    version = await task
    return parse_version(version)


async def no_checker_found(checker, pkg_name):  # pylint: disable=unused-argument
    return None


CHECKERS = {
    'pypi': query_pypi,
    'github': check_github,
    'dirlisting': search_directory_listing,
    'webbot': search_website,
    'custom': call_custom_func,
}


#
# Machinery - The remaining code is responsible for running all the checkers
#
class UpdateCheckFailed(Exception):
    def __init__(self, recipe, msg):
        super().__init__(recipe, msg)
        self.recipe = recipe
        self.msg = msg


async def run_check(meta, filename):
    """Perform the update check for a recipe and return the results.

    *meta* is the parsed ``meta.yaml``, *filename* the full path to that file.

    This task is run by :func:`check_for_updates()`.

    """
    recipe_name = filename.parent.name
    package_name = meta['package']['name']

    # Get current version
    try:
        current_version = parse_version(str(meta['package']['version']))
    except TypeError as e:
        raise TypeError(f'Cannot parse version for "{package_name}": '
                        f'{meta["package"]["version"]}') from e

    # Get latest version
    update_check = load_update_check(filename)
    checker_func = CHECKERS.get(update_check.checker, no_checker_found)
    try:
        latest_version = await checker_func(update_check, package_name)
    except (AttributeError, IndexError, OSError, RuntimeError, TypeError,
            requests.RequestException) as e:
        msg = str(e)
        raise UpdateCheckFailed(recipe_name, msg) from e

    return (recipe_name, latest_version, current_version)


def load_update_check(path):
    """Load the ``update_check.py`` module from a recipe's *path*."""
    # This function is used when a receipe does not provice an update_check:
    class DummyModule:
        checker = 'none'

    # Import the "update_check" module from its filename:
    dirname = path.parent
    module_suffix = str(dirname.name).replace('-', '_')
    module_name = f'update_check_{module_suffix}'
    module_filename = dirname / 'update_check.py'
    try:
        spec = importlib.util.spec_from_file_location(module_name, module_filename)
        update_check = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(update_check)
    except FileNotFoundError:
        return DummyModule

    return update_check
