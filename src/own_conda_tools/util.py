"""
Miscellaneous utility functions for commands.

"""
import json
import os
import shlex
import socket
import subprocess
import sys

import click

from . import click_util, recipes


__all__ = [
    'echo_cmd',
    'run',
    'install_dependencies',
    'install_packages',
]


def echo_cmd(cmd, cwd=None):
    """Print *cmd* in blue color."""
    if not isinstance(cmd, str):
        cmd = ' '.join(map(shlex.quote, cmd))

    if cwd is not None and cwd != '.':
        click_util.status(f'cd {cwd} && ', fg='blue', nl=False)

    click_util.status(cmd, fg='blue', bold=True)


def run(cmd, *, cwd=None, env=None, quiet=False, check=True, interactive=False):
    """Pretty-print *cmd* and pass it to :func:`subprocess.run()`.

    You can optionally set a *cwd* and pass a dict of *env* variables.

    If *quiet* is ``True``, the subprocess' output is only shown if its exit
    code is not 0.

    If *check* is ``True``, non-zero exit codes will abort the program.
    You can set *interactive* to ``True`` if you want to interactively re-run
    failed commands instead of aborting immediately.

    Return the command's exit code.

    """
    echo_cmd(cmd, cwd=cwd)
    stdout = subprocess.PIPE if quiet else None
    stderr = subprocess.PIPE if quiet else None
    while True:
        result = subprocess.run(
            cmd,
            cwd=cwd,
            env=env,
            stdout=stdout,
            stderr=stderr,
            universal_newlines=True,
        )
        if result.returncode == 0:
            break
        else:
            if quiet:
                click_util.echo('STDOUT:', fg='blue')
                click_util.echo(result.stdout)
                click_util.echo('STDERR:', fg='magenta')
                click_util.echo(result.stderr)
            if check or interactive:
                click_util.error(
                    f'Command "{" ".join(map(shlex.quote, cmd))}" returned '
                    f'non-zero exit status {result.returncode}'
                )
            if check and interactive:
                if click.confirm('Retry last command?', default=True):
                    continue

            if check:
                sys.exit(result.returncode)
            else:
                break
    return result


def get_unused_tcp_port():
    """Find an unused localhost TCP port from 1024-65535 and return it."""
    try:
        sock = socket.socket()
        sock.bind(('127.0.0.1', 0))  # pylint: disable=no-member
        return sock.getsockname()[1]  # pylint: disable=no-member
    finally:
        sock.close()


def install_dependencies(
        paths, *, conda_exe, dep_type, python=None, env_name=None, extras=None,
        extra_channels=None
):
    """Search all directories in *paths* for Conda recipes and install their
    requirements of type *dep_type* into the Conda env *env_name*

    Install all dependencies of *dep_type* listed in the *meta.yaml* files
    found under *path/conda* into the Conda env *env_name*.

    The packages ``conda`` and ``conda-*`` will only get installed into root
    environments.

    If a set of *extras* is provided, also install them.

    *extra_channels* can be a list of extra channels to pass to Conda, e.g.:
    ``['--channel=staging']``.

    """
    conda_args = ['--quiet', '--yes']
    if env_name:
        conda_args.append(f'--name={env_name}')
    if extra_channels:
        conda_args.extend(extra_channels)

    # Update everything in the env before installing additional packages
    cmd = [conda_exe, 'update', '--all']
    cmd.extend(conda_args)
    run(cmd)

    meta_yamls = recipes.load_recipes(paths)
    deps = set()
    for meta, _ in meta_yamls:
        deps |= recipes.read_deps(meta, dep_type)

    if extras:
        deps |= extras

    # Drop "conda" from deps if we do not install into a root env:
    explicit_root = env_name == 'root'
    implicit_root = env_name is None and 'CONDA_DEFAULT_ENV' not in os.environ
    if not (explicit_root or implicit_root):
        deps = [d for d in deps if d != 'conda' and not d.startswith('conda-')]

    cleaned_deps = []
    for dep in sorted(deps):
        parts = dep.strip().split()
        if python is not None and parts[0] == 'python':
            parts = ['python', '=', python]

        if len(parts) > 1 and not parts[1].startswith(('=', '<', '>', '!')):
            parts[1] = f'={parts[1]}'

        cleaned_deps.append(''.join(parts))

    cmd = [conda_exe, 'install']
    cmd.extend(conda_args)
    cmd.extend(cleaned_deps)
    run(cmd)


def install_packages(
        artifacts_path, recipe_dir, *, conda_exe, platform,
        python=None, extra_channels=None, dep_type=recipes.ALL,
):
    """Install all packages found in the local *artifacts_path*.

    *artifacts_path* must be a directory containing a platform sub-directory
    (e.g., :file:`linux-64`) with one or more Conda packages.

    These packages will be installed with ``--no-deps``.  Dependencies will
    instead be read from all recipes found in *recipe_dir*.  You can change
    the type of dependencies to be installed by overriding *dep_type*.

    Pass *extra_channels* to all Conda commands.

    """
    install_dependencies(
        [recipe_dir],
        conda_exe=conda_exe,
        python=python,
        dep_type=dep_type,
        extra_channels=extra_channels,
    )
    packages = _get_pkg_paths(platform, artifacts_path)

    cmd = [conda_exe, 'install', '--quiet', '--yes', '--channel', str(artifacts_path)]
    if extra_channels:
        cmd.extend(extra_channels)
    cmd.extend(sorted(packages))

    run(cmd)


def _get_pkg_paths(platform, channel):
    """Return a list with all package filenames found in the local *channel*.

    *channel* must be a directory containing a `linux-64/repodata.json`
    sub-directory/file.

    """
    platform_dir = channel / platform
    repodata_file = platform_dir / 'repodata.json'
    repodata = json.load(open(repodata_file))
    packages = {p['name'] for p in repodata['packages'].values()}
    return packages
