import subprocess

from .click_util import Action


def update(url, path, branch=None, rebase=None):
    """Clone or pull the repo at *url* to the local *path* and checkout
    *branch* (or master it the former does not exist).

    You can control Git's rebasing behavior by setting *rebase* to ``True`` or
    ``False``.

    """
    if path.is_dir():
        pull(path, branch, rebase)
    else:
        clone(url, path, branch)


def clone(url, path, branch=None):
    """Clone the repo at *url* to the local *path* and checkout
    *branch* (or master it the former does not exist).

    """
    dest, repo = path.parent, path.name
    dest.mkdir(parents=True, exist_ok=True)
    with Action(f'Cloning "{url}" into "{path}" ...'):
        git('clone', url, repo, cwd=dest)
    try_checkout(path, branch)


def pull(path, branch=None, rebase=None):
    """Pull the repo at the local *path* and checkout *branch* (or master it
    the former does not exist).

    You can control Git's rebasing behavior by setting *rebase* to ``True`` or
    ``False``.

    """
    try_checkout(path, branch)
    with Action(f'Pulling repo "{path}" ...'):
        cmd = ['pull']
        if rebase is not None:
            cmd.append('--rebase' if rebase else '--no-rebase')
        git(*cmd, cwd=path)


def try_checkout(path, branch):
    if branch is None:
        return

    with Action(f'Checking out "{branch}" ...') as action:
        git('checkout', branch, cwd=path, check=False)
        co_branch = current_branch(path)
        if co_branch == branch:
            pass
        elif co_branch == 'master':
            action.warning('branch not found, using "master"')
        else:
            action.warning(f'branch not found, using "{co_branch}"')


def current_branch(path):
    # Centos7 is too fucking old ..
    # result = git('status', '--porcelain=2', '--branch', cwd=path)
    # return re.search(r'# branch\.head (.+)', result.stdout).group(1)
    result = git('branch', cwd=path)
    lines = [line[2:] for line in result.stdout.splitlines() if line.startswith('* ')]
    return lines[0] if lines else 'master'


def describe(path):
    """Return a dict containing the latest git tag, number of commits since
    that tag and the abbreviated hash of the last commit in *path*, e.g.::

        {
            'GIT_DESCRIBE_TAG': '2',
            'GIT_DESCRIBE_NUMBER': '123',
            'GIT_DESCRIBE_HASH': 'beef42',
        }

    """
    try:
        result = git('describe', '--tags', '--long', 'HEAD', cwd=path).stdout
    except subprocess.CalledProcessError:
        # No tags.  Just return HEAD's hash.
        result = git('rev-parse', '--short', 'HEAD', cwd=path).stdout
        parts = [result.strip()]
        keys = ['GIT_DESCRIBE_HASH']
    else:
        # Parse "describe" results.
        parts = result.strip().split('-')
        assert len(parts) == 3
        parts[2] = parts[2][1:]  # The git hash is prefixed with "g".  Cut it.
        keys = ['GIT_DESCRIBE_TAG', 'GIT_DESCRIBE_NUMBER', 'GIT_DESCRIBE_HASH']

    return {k: v for k, v in zip(keys, parts)}


def git(*args, cwd, check=True):
    cmd = ('git',) + args
    return subprocess.run(
        cmd, cwd=cwd, check=check, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
        universal_newlines=True,
    )
