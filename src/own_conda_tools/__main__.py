"""
Main command for ``ownconda``.

Sub-commands are dynamically loaded from ``commands`` folder.  They must define
a :func:`click.command()` called ``cli``.

"""
import importlib
import pathlib
import sys
import typing

import attr
import click

from . import constants


CMD_MOD = 'own_conda_tools.commands.{}'
CMD_FOLDER = pathlib.Path(__file__).parent / 'commands'


class LazyCli(click.MultiCommand):
    """Lazily load commands from :data:`CMD_FOLDER`."""

    def list_commands(self, ctx):
        """Return a list of all files in the commands folder.

        Strip the *.py* extension and replace "_" with "-".

        """
        cmds = []
        for file in CMD_FOLDER.glob('*.py'):  # pylint: disable=no-member
            if file.name == '__init__.py':
                continue
            cmds.append(file.stem.replace('_', '-'))
        return sorted(cmds)

    def get_command(self, ctx, cmd_name):
        """Return the command object for a given cmd_name."""
        modname = CMD_MOD.format(cmd_name.replace('-', '_'))
        try:
            mod = importlib.import_module(modname)
        except ImportError:
            return
        mod.cli.name = cmd_name
        return mod.cli


@attr.s(auto_attribs=True, frozen=True)
class Info:
    conda_exe: str
    root_prefix: pathlib.Path
    default_prefix: pathlib.Path
    py_ver: str
    platform: str
    env_dir: str
    envs: typing.List[str]


@click.command(cls=LazyCli)
@click.pass_context
def cli(ctx):
    """Support tools for local development, CI/CD and Conda packaging."""
    conda_exe = constants.locate_conda()
    conda_info = constants.get_conda_info(conda_exe)
    info = Info(
        conda_exe=conda_exe,
        root_prefix=pathlib.Path(conda_info['root_prefix']),
        default_prefix=pathlib.Path(conda_info['default_prefix']),
        py_ver='.'.join(map(str, sys.version_info[:2])),
        platform=conda_info['platform'],
        env_dir=conda_info['envs_dirs'][0],
        envs=conda_info['envs'],
    )
    ctx.obj = info


if __name__ == '__main__':
    cli()  # pylint: disable=no-value-for-parameter
