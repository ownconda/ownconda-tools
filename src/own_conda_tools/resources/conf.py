{#- This file is a Jinja2 template.  "ownconda make-docs" will render it and #}
{#- write it into the doc source directory. #}
import datetime
import subprocess


# -- General configuration ------------------------------------------------

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
]

source_suffix = '.rst'
master_doc = 'index'
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
rst_prolog = """
.. role:: channel(strong)
.. role:: branch(file)
.. role:: cmd(literal)
"""

templates_path = ['_templates']
pygments_style = 'sphinx'

project = '{{ project_title }}'
slug = '{{ slug }}'
author = 'Own Company'
today_fmt = '%Y-%m-%d'
today = f'{datetime.date.today():{today_fmt}}'
year = '{{ year }}'
version = '{{ version }}'
release = '{{ release }}'
copyright = f'{year} {author} – {release} – {today}'

language = '{{ language }}'


# -- Options for HTML output ----------------------------------------------

import sphinx_rtd_theme
html_static_path = ['_static']
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'display_version': False,
}


# -- Options for LaTeX output ---------------------------------------------

latex_engine = 'lualatex'
latex_elements = {
    'papersize': 'a4paper',
    'pointsize': '11pt',
    'preamble': '',
    'figure_align': 'htbp',
}
latex_show_pagerefs = True

# We don't need to define this.  The "make-docs" command will copy
# all additional files to the correct location for us:
# latex_additional_files = []

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (
        master_doc,
        f'{slug}.tex',
        project,
        author.replace('&', '\\&'),
        'manual',
    ),
]


# -- Plug-ins -------------------------------------------------------------

# Intersphinx
intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    {%- for name, url in intersphinx.items() %}
    '{{ name }}': ('{{ url }}', None),
    {%- endfor %}
}

# Autodoc
autodoc_member_order = 'bysource'
