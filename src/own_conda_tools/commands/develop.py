"""
Install a package in editable/development mode.  Optionally create a new
Conda environment for it.

This command basically collects all dependencies (build, run, test) from all
*meta.yaml* files found in the provided directory (or directories) and then
does a ``pip install --no-deps -e .``:

.. code-block:: console

   $ cd <project>
   $ ownconda develop .
   Creating Conda env "<project>" ...
   Installing dependencies: own-<dep>, pip, pytest, pytest-cov, python >=3.5
   Installing "popeye" ...

   $ # Install multiple editable projects:
   $ ownconda develop . ../commongui
   Creating Conda env "popeye" ...
   Installing dependencies: own-commongui, pip, pytest, pytest-cov, python >=3.5
   Installing "popeye" ...

If you don't want to create a new Conda env, pass the ``--no-create-env`` or
``-n`` flag to the command.

If you are an iPython user, you need to install iPython into every Conda
environment from which you want to use it.  The ``develop`` command can
help you with this:

.. code-block:: console

    $ emconda develop --ipython .

You can place the following function in your :file:`.bash_aliases` as
a shortcut for :command:`ownconda develop .; . activate <env>`:

.. code-block:: bash

   dev() {
       ownconda develop [--ipython] . $@
       . activate $(basename $(pwd))
   }

"""
import os
import os.path
import pathlib

import click

from .. import click_util, git, recipes, util


def get_env_name(path):
    env_name = path.resolve().name
    return env_name


def create_conda_env(info, env_name, channels, python):
    """Create a Conda env named *basename(path)* if it not already exists."""
    conda_exe = info.conda_exe
    env_path = os.path.join(info.env_dir, env_name)
    create = True
    if env_name == 'base':
        click_util.warning('Installing into Coda base env!')
        return info.root_prefix

    if env_path in info.envs:
        if info.py_ver == python:
            msg = f'Conda env "{env_name}" already exists.'
            click_util.warning(msg)
            create = False
        else:
            msg = f'Conda env "{env_name}" already exist with Python {info.py_ver}'
            click_util.error(msg)
            recreate = click.confirm(f'Delete env and -recreate with Python {python}?')
            if recreate:
                cmd = [conda_exe, 'env', 'remove', '--yes', '--name', env_name]
                util.run(cmd)
            else:
                create = False

    if create:
        cmd = [conda_exe, 'create', '--yes', '--name', env_name, f'python {python}*']
        cmd += channels
        util.run(cmd)

    return env_path


def install_editable(paths, conda_exe, env_name, env_prefix):
    """Run ``pip install --no-deps -e <path>`` for each path in *paths*."""
    # Our package(s) might have been installed as "dependency" due to circular
    # references.  Uninstall them so that they don't interfer with the
    # editable installations.
    remove_packages = []
    local_recipes = recipes.load_recipes(path / 'conda' for path in paths)
    for recipe, _ in local_recipes:
        remove_packages.append(recipe['package']['name'])

    cmd = [conda_exe, 'uninstall', '--yes', '--name', env_name, '--force']
    cmd.extend(remove_packages)
    util.run(cmd, check=False)

    for path in paths:
        # Editable install w/o dependencies
        env = os.environ.copy()
        env.update(git.describe('.'))
        pip = os.path.join(env_prefix, 'bin', 'pip')
        cmd = [pip, 'install', '--no-deps', '-e', '.']
        util.run(cmd, cwd=path, env=env)


def override_default_channels(ctx, _param, value):
    """Update the default of the "--channel" option."""
    c_param = [p for p in ctx.command.params if p.name == 'channel'][0]
    if value:
        c_param.default.remove('staging')
    return value


@click.command()
@click.pass_obj
@click_util.argument.paths(file_okay=False)
@click_util.option.channel(default=['staging'])
@click.option(
    '--create-env/--no-create-env',
    default=True,
    show_default=True,
    help='If set, create a Conda env named like the basename of PATHS.',
)
@click.option(
    '--no-staging',
    is_flag=True,
    default=False,
    is_eager=True,
    expose_value=False,
    callback=override_default_channels,
    help="Don't use the staging channel by default.",
)
@click.option(
    '--ipython',
    is_flag=True,
    default=False,
    help='Install iPython into the env.',
)
@click.option(
    '--name',
    '-n',
    help='Use this env name instead of extracting it from PATHS.',
)
@click_util.option.python()
def cli(info, paths, channel, create_env, ipython, name, python):
    """Install PATHS in develop/editable mode.

    You can pass multiple PATHs to install multiple packages editable.

    If no PATHS are provided, use the CWD (".").

    """
    if not paths:
        paths = ('.',)
    paths = [pathlib.Path(p) for p in paths]
    env_name = name or get_env_name(paths[0])

    if create_env:
        env_prefix = create_conda_env(info, env_name, channel, python)
    else:
        if info.default_prefix == info.root_prefix:
            raise click.ClickException(
                "You must activate a Conda env if you don't create one!"
            )

        env_prefix = info.default_prefix

    extras = {'ipython'} if ipython else set()
    util.install_dependencies(
        paths,
        conda_exe=info.conda_exe,
        python=python,
        dep_type=recipes.ALL,
        env_name=env_name,
        extras=extras,
        extra_channels=channel,
    )
    install_editable(paths, info.conda_exe, env_name, env_prefix)

    msg = 'Run the following command to activate the Conda env:'
    click_util.info(msg)
    click_util.echo(f'conda activate {env_name}', fg='magenta')
