"""
Compare the version and build numbers of local recipes to these of the
corresponding packages of a Conda repository.

The path to recipes that are newer or not available in the Conda repo are
printed to stdout and can from there pipped to a Conda build script.

.. code-block:: console

   $ ownconda show-updated-recipes ../external-recipes/
   /data/ssd/external-recipes/python-beautifulsoup4
   /data/ssd/external-recipes/python-networkx
   /data/ssd/external-recipes/system

"""
import re

import click
import requests

from .. import click_util, constants, recipes, version


def load_repository(url, restrict_python):
    """Load repository data from *url* and return the dict of packages it
    contains.

    If *restrict_python* is not None, skip all Python packages of that version
    (e.g, ``'py36'``).

    """
    rep = requests.get(url)
    if rep.status_code != 200:
        raise RuntimeError(f'Could not retrieve "{url}"')

    repo_data = rep.json()
    pkgs = group_repo_packages(repo_data['packages'], restrict_python)
    return pkgs


def group_repo_packages(repo_pkgs, restrict_python):
    re_pypkg = re.compile(r'(py\d{2,})_.*')
    pkgs = {}
    for fname, meta in repo_pkgs.items():
        if restrict_python:
            # Skip Python packages for the wrong Python version:
            is_pypkg = re_pypkg.match(meta['build'])
            if is_pypkg and is_pypkg.group(1) != restrict_python:
                continue

        meta['filename'] = fname
        release_list = pkgs.setdefault(meta['name'], [])
        release_list.append(meta)

    for releases in pkgs.values():
        # Sort releases by (version, build)
        releases.sort(key=lambda r: (
            version.parse(r['version']),
            r['build_number'],
        ))

    return pkgs


def compare_versions(recipe_data, repo_pkgs, debug):
    for recipe, recipe_file in recipe_data:
        pkg_folder = recipe_file.parent.resolve().name
        pkg_name = recipe['package']['name']
        local_version = version.version_tuple(recipe)
        released = False
        released_versions = []
        if pkg_name in repo_pkgs:
            for release in repo_pkgs[pkg_name]:
                release_version = version.version_tuple(release)
                released_versions.append('-'.join(map(str, release_version)))
                if release_version == local_version:
                    released = True

        if released:
            if debug:
                msg = f'  {pkg_folder} {version.format_version(recipe)}'
                click_util.status(msg, fg='magenta')
        else:
            if debug:
                msg = '+ {} {} [{}]'.format(
                    pkg_folder,
                    version.format_version(recipe),
                    ', '.join(released_versions),
                )
                click_util.status(msg, fg='green')

            # "color=False" is required or it will fail in the pipeline:
            click_util.echo(str(recipe_file.parent.resolve()), color=False)


def validate_channel(_ctx, _param, value):
    if not value.startswith(('http://', 'https://')):
        value = f'{constants.DEFAULT_REPOSITORY}/{value}'
    url = f'{value}/linux-64/repodata.json'
    try:
        response = requests.get(url)
        response.raise_for_status()
    except requests.RequestException:
        raise click.BadParameter(f'Channel "{value}" not available')

    return url


@click.command()
@click_util.argument.recipe_root()
@click.option(
    '--channel',
    '-c',
    default=f'{constants.DEFAULT_REPOSITORY}/{constants.DEFAULT_CHANNEL}',
    show_default=True,
    callback=validate_channel,
    help='The Conda channel against that we check the versions',
)
@click.option(
    '--debug',
    '-d',
    is_flag=True,
    default=False,
    help='Print a formatted list of *all* packages found.',
)
@click.option(
    '--restrict-python',
    '-p',
    default=None,
    callback=lambda c, p, v: None if not v else f'py{v.replace(".", "")}',
    help='Restrict comparison to packages with the specified Python version, e.g, 3.6',
)
def cli(recipe_root, channel, debug, restrict_python):
    """Show updated recipes in RECIPE_ROOT.

    You can specify multiple recipe roots which are all searched recursively
    for recipes.

    The version and build numbers of the recipes are compared to the packages
    available at "repository/channel".

    """
    recipe_gen = recipes.load_recipes(recipe_root)
    repo_pkgs = load_repository(channel, restrict_python)

    compare_versions(recipe_gen, repo_pkgs, debug)
