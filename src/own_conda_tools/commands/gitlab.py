"""
The ``gitlab`` command offers various subcommands that allow you to
clone/update or modify multiple projects at once.

Before you can use this, you need to :ref:`configure-gitlab-access`.

The ``gitlab`` commands will mirror the group and project structure found in
GitLab.  You can specify a local base directory via the ``--base-dir`` option
or via the environment variable ``OWNCONDA_BASEDIR``.

You can also prefix the local group names (e.g., to get an :file:`own-group`
folder instead of :file:`group`).  This can be done with the ``--prefix``
option or the ``OWNCONDA_LOCAL_GROUP_PREFIX`` environment variable,
respectively.

You can select specific projects using the ``-p/--project`` or ``-g/--group``
options (e.g., ``-p group/project -p group/project2``


Print projects
--------------

The ``print-project`` command mainly serves debugging purposes and helps you to
get an overview of one or multiple groups' projects:

.. code-block:: console

    $ ownconda gitlab print-project
    apps / changelog-generator
    ...
    vendor / pyqtgraph

    $ ownconda gitlab -g ownconda print-project  # Print all ownconda projects
    ownconda / external-recipes
    ...
    ownconda / ownconda-tools


Work on groups (only)
---------------------

If you want to work on individual groups, you can pass the ``--groups`` flag:

.. code-block:: console

    $ ownconda gitlab --groups print-project
    apps
    apps / changelog-generator
    ...
    vendor
    vendor / pyqtgraph

If you want to work on groups exclusively, you also need to pass the
``--no-projects`` flag:

.. code-block:: console

    $ ownconda gitlab --groups --no-projects print-project
    apps
    ...
    vendor


Clone / update projects
-----------------------

The ``update`` command makes sure all selected projects are cloned onto your
computer and are up-to-date:

.. code-block:: console

    $ ownconda gitlab -g ownconda update


Prune projects
--------------

The ``prune`` commands deletes all local project clones for which no GitLab
project exists (normal files are not touched).  By default, it only prints the
directories it would delete.  You must pass the ``--force`` flag to actually
delete anything.

Furthermore, since this command only works on groups, you must use the
``--groups --no-projects`` flags.

.. code-block:: console

    $ ownconda gitlab -g apps --groups --no-projects prune
    .../apps/removed-project

    $ ownconda gitlab -g apps --groups --no-projects prune --force


Run bulk operations on multiple projects
----------------------------------------

You can execute (bash) scripts:

.. code-block:: console

    $ vim script.sh
    #!/bin/bash
    echo "ohai $(pwd)"

    $ chmod a+x script.sh
    $ ownconda gitlab -p ownconda/ownconda-tools run-script script.sh
    ownconda / ownconda-tools
    ohai /home/user/Projects/ownconda/ownconda-tools

You can also run Python scripts.  Python scripts need to define a ``task()``
function as entry point which will receive a *project* object:

.. code-block:: console

    $ vim script.py
    def task(project):
        print(project.fs_path)
        print(project.path_with_namespace)
        print(project.ssh_url_to_repo)

    $ ownconda gitlab -p ownconda/ownconda-tools run-py script.py
    ownconda / ownconda-tools
    /home/user/Projects/ownconda/ownconda-tools
    ownconda/ownconda-tools
    git@gitlab.com:ownconda/ownconda-tools.git

"""
import functools
import importlib
import os
import pathlib
import shutil
import subprocess

import click
import gitlab

from .. import click_util, constants, git


GROUPS = [
    'ownconda',
]


def iter_all_projects(gl, groups=None, *, yield_groups=False, yield_projects=True):
    if groups is None:
        groups = GROUPS
    for group_name in groups:
        group = gl.groups.get(group_name)
        if yield_groups:
            yield group

        if yield_projects:
            for gproject in group.projects.list(all=True, order_by='path', sort='asc'):
                if gproject.namespace['id'] != group.id:
                    # Skip projects that are linked into another group
                    continue
                project = gl.projects.get(gproject.id)
                yield project


def iter_projects(gl, project_names):
    for name in project_names:
        project = gl.projects.get(name)
        yield project


def add_fs_path(projects, base_dir, prefix):
    for project in projects:
        try:
            path = project.path_with_namespace
            project.is_project = True  # project
        except AttributeError:
            path = project.full_path
            project.is_project = False  # group
        project.fs_path = base_dir / f'{prefix}{path}'
        yield project


@click.group(help=f"""
Run a task on a number of GitLab projects.  If no project names are explicitly
passed, the task is performed on all FCS related projects:

    {", ".join(GROUPS)}

By default, commands will be executed for individual projects.  By passing
"--groups" you can also work on individual groups.

Before you can use this command, you need to configure *python-gitlab* as
described in:

\b
    https://forge.services.own/docs/ownconda/ownconda-tools/install.html#configure-gitlab-access

""")
@click.pass_context
@click.option(
    '-g',
    '--group',
    multiple=True,
    help='A GitLab group name (e.g., "ownconda").  '
         'Cannot be used together with "-p/--project".',
)
@click.option(
    '--groups/--no-groups',
    default=False,
    show_default=True,
    help='Work on individual groups.  Cannot be used together with "-p/--project".',
)
@click.option(
    '-p',
    '--project',
    multiple=True,
    help='A GitLab project name with namespace (e.g., "ownconda/ownconda-tools").  '
         'Cannot be used together with "-g/--group".',
)
@click.option(
    '--projects/--no-projects',
    default=True,
    show_default=True,
    help='Work on individual projects',
)
@click.option(
    '-d',
    '--base-dir',
    type=click.Path(exists=True, file_okay=False, writable=True),
    default=pathlib.Path(os.getenv('OWNCONDA_BASEDIR', '.')),
    show_default=True,
    callback=lambda _ctx, _param, value: pathlib.Path(value).resolve(),
    envvar='OWNCONDA_BASEDIR',
    help='Base directory for project groups and repos.  '
         'Default is read from $OWNCONDA_BASEDIR.',
)
@click.option(
    '--prefix',
    default=os.getenv('OWNCONDA_LOCAL_GROUP_PREFIX', ''),
    show_default=True,
    envvar='OWNCONDA_LOCAL_GROUP_PREFIX',
    help='Local prefix for group folders.  '
         'Default is read from $OWNCONDA_LOCAL_GROUP_PREFIX.',
)
@click.option(
    '--np',
    '--no-print',
    'do_print',
    flag_value=False,
    default=True,
    help='Do not print project and group names',
)
def cli(ctx, group, groups, project, projects, base_dir, prefix, do_print):  # pylint: disable=unused-argument
    """Run a task on a number of GitLab projects."""
    if project and group:
        raise click.UsageError(
            '"-p/--project" and "-g/--group" cannot be used together.'
        )
    if project and not projects:
        raise click.UsageError(
            '"-p/--project" and "--projects" cannot be used together.'
        )

    gl = gitlab.Gitlab.from_config(constants.GITLAB_CONFIG_SECTION)
    gl.auth()

    if project:
        projects = iter_projects(gl, project)
    else:
        if group == ():
            group = None  # Iterate over all groups
        projects = iter_all_projects(
            gl, group, yield_groups=groups, yield_projects=projects
        )

    projects = add_fs_path(projects, base_dir, prefix)

    ctx.obj = projects


def pass_project(task):
    @click.pass_context
    def run_for_project(ctx, *args, **kwargs):
        do_print = ctx.parent.params['do_print']
        for project in ctx.obj:
            if do_print:
                if project.is_project:
                    click_util.echo(
                        f'{project.path_with_namespace}', fg='blue', bold=True
                    )
                else:
                    click_util.echo(f'{project.full_path}', fg='magenta', bold=True)
            task(project, *args, **kwargs)

    # Fix how wrapped "task" function looks to click
    f = functools.update_wrapper(run_for_project, task)
    f.__name__ = f.__name__.replace('_', '-')
    return f


@cli.command()
@pass_project
def print_project(project):  # pylint: disable=unused-argument
    """Print project name with namespace."""
    return


@cli.command()
@pass_project
@click.option(
    '--branch',
    '-b',
    default='develop',
    show_default=True,
    help='Branch to checkout.  Falls back to "master" if branch does not exist.',
)
@click.option(
    '--rebase/--no-rebase',
    default=None,
    show_default=True,
    help='This flag is forwared to "git pull".',
)
def update(project, branch, rebase):
    """Pull updates for all projects.  Clone missing projects."""
    if not project.is_project:
        # Cannot update groups
        return

    git.update(project.ssh_url_to_repo, project.fs_path, branch, rebase)


@cli.command()
@pass_project
@click.option(
    '--force',
    '-f',
    'dry_run',
    flag_value=False,
    default=True,
    help='Actually delete the projects.',
)
def prune(group, dry_run):
    """Prune local project clones.

    Iterate the specified groups and check if there is a corresponding GitLab
    project for every local project folder.  Delete local folders without
    a GitLab project.  Do not touch files inside the group folders.

    Since this command only works on groups, you must pass the both, "--groups"
    and "--no-projects" to the "gitlab" command.

    By default, it only prints the folders it would delete.  Pass the
    "-f/--force" option to actually delete the projects.

    """
    if group.is_project:
        # Only work on groups
        return

    projects = [p.name for p in group.projects.list(all=True)]
    for directory in sorted(d for d in group.fs_path.iterdir() if d.is_dir()):
        if directory.name in projects:
            continue

        if dry_run:
            click_util.echo(str(directory))
        else:
            shutil.rmtree(directory)


@cli.command()
@pass_project
@click.argument(
    'python-file',
    type=click.Path(exists=True, dir_okay=False),
)
def run_py(project, python_file):
    """Import PYTHON_FILE and run it's "task()" function.

    The current Project instance is passed to "task()".

    The cwd is set to the project's directory.
    """
    python_file = pathlib.Path(python_file)
    try:
        modname = python_file.stem
        spec = importlib.util.spec_from_file_location(modname, python_file)
        mod = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(mod)
    except (AttributeError, ImportError):
        raise click.BadParameter(f'Cannot import {python_file}') from None
    try:
        task = mod.task
    except AttributeError as e:
        raise click.BadParameter(e) from e
    old_cwd = os.getcwd()
    os.chdir(project.fs_path)
    try:
        task(project)
    finally:
        os.chdir(old_cwd)



@cli.command()
@pass_project
@click.argument(
    'script-file',
    type=click.Path(exists=True, dir_okay=False, resolve_path=True),
)
def run_script(project, script_file):
    """Run the script SCRIPT_FILE with the cwd set to the project's directory.
    """
    subprocess.run([script_file], cwd=project.fs_path)
