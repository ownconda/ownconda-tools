"""
Run *pytest* with code coverage checks.  This command will forward pytest's
exit code to you.

Coverage produces both, console output and HTML output (located in
``htmlcov/``).

.. code-block:: console

   $ ownconda test

"""
import os

import click

from .. import click_util, constants, util


def test_package(paths, cov_paths, doctest_modules, xvfb_prefix):
    """Test all files/directories in *paths* and measure coverage for all
    directories in *cov_paths*.

    The list *xvfb_prefix* is either empty or contains a command to initialize
    a xvfb session.

    """
    cmd = xvfb_prefix
    cmd.extend([
        'pytest',
        '-vv',
        '--junit-xml=pytest-report.xml',
        '--ignore=setup.py',
        '--doctest-glob=*.rst',
    ])

    if doctest_modules:
        cmd.append('--doctest-modules')

    if constants.in_pipeline():
        cmd.append('--color=yes')
        cmd.append('--force-sugar')

    cmd.extend(f'--cov={path}' for path in cov_paths)
    cmd.append('--cov-report=term-missing')
    cmd.append('--cov-report=html')

    cmd.extend(paths)

    env = os.environ.copy()
    env['PYTHONPATH'] = 'src'

    util.run(cmd, env=env)


@click.command()
@click.pass_obj
@click_util.argument.paths()
@click_util.option.recipe_dir()
@click_util.option.python()
@click_util.option.channel(auto_ci_staging=True)
@click_util.option.artifacts_dir()
@click_util.option.gui()
@click.option(
    '--cov',
    multiple=True,
    default=['src'],
    show_default=True,
    type=click.Path(exists=True, file_okay=False),
    help='Measure coverage in this directory',
)
@click.option(
    '--doctest-modules',
    is_flag=True,
    default=False,
    help='Run doctests in all .py modules',
)
def cli(
        info, paths, recipe_dir, python, channel, artifacts_dir, gui, cov,
        doctest_modules,
):
    """Run tests in PATHS."""
    util.install_packages(
        artifacts_dir,
        recipe_dir,
        conda_exe=info.conda_exe,
        platform=info.platform,
        python=python,
        extra_channels=channel,
    )
    test_package(paths, cov, doctest_modules, xvfb_prefix=gui)
