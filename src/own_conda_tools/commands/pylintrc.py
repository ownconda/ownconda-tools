"""
Print the the built-in pylintrc or write it to a file.

If you want to run pylint locally with the same config that the GitLab CI uses,
you can use this command to view the build-in configuration:

.. code-block:: console

   $ ownconda pylintrc

Pylint reads its configuration from :file:`~/.pylintrc` and
:file:`~/.config/pylintrc`.  To write the pylintrc to one of these files, you
can either use the ``--output`` option or just pipe the output into the file:

.. code-block:: console

   $ ownconda pylintrc --output=~/.pylintrc
   $ ownconda pylintrc > ~/.config/pylintrc

"""
import click

from .. import click_util, constants


@click.command()
@click.option('--output', '-o', type=click.File('w'),
              help='Write contents to FILENAME instead of printing it')
def cli(output):
    """Print the built-in pylintrc to stdout.

    Pylint looks for "~/.pylintrc" and "~/.config/pylintrc".
    You can write these files by piping the output or by using
    the "--output" / "-o" option:

        ownconda pylintrc > ~/.config/pylintrc

        ownconda pylintrc --output=~/.pylintrc

    """
    pylintrc = open(constants.resource_file('pylintrc')).read()
    if output is None:
        click_util.echo(pylintrc, nl=False)
    else:
        output.write(pylintrc)
