"""
Prune packages in the given directory.

Apply the following rules:

- own-* packages:
  - Only keep the latest build for each version
  - Keep all packages of the last month
  - Keep one package per week for the last 6 months
  - Delete everything older
  - Keep at least one version of a package (even if too old)

- external packages:
  - ONly keep the lastest build for each version

..   - Delete if no recipe in external-recipes

.. code-block:: console

    $ ownconda prune-index stable/linux-64
    Keep  stable/linux/eggs-2.2-0.tar.bz2
    Prune stable/linux/spam-1.2-0.tar.bz2
    Keep  stable/linux/spam-1.2-1.tar.bz2
    1/3 would be deleted.  Pass the "--force" option to actually delete them.

    $ ownconda prune-index stable/linux-64 --force
    Deleted 1/3 packages

"""
import itertools
import json
import os
import pathlib

import click
import pendulum

from .. import click_util, recipes, util, version


def collect_pkgs(path):
    # Check if files in the FS and contents of repodata.json are in sync
    files_from_dir = sorted(f for f in path.glob('*.tar.bz2'))
    repodata = json.loads((path / 'repodata.json').read_text())
    files_from_repodata = sorted(path / fn for fn in repodata['packages'])
    if files_from_dir != files_from_repodata:
        raise click.ClickException('Filesystem and repodata.json are not in sync')

    files = files_from_repodata

    pkgs = []
    for file in files:
        data = repodata['packages'][file.name]
        data['filename'] = file
        data['internal'] = data['name'].startswith('own-')
        # Timestamps in repodata.json are in "ms", in repodata2.json its "s"
        try:
            data['datetime'] = pendulum.from_timestamp(data['timestamp'] // 1000)
        except KeyError:
            click_util.warning(f'WARNING: No timestamp set for "{file.name}"')
            data['datetime'] = pendulum.from_timestamp(0)
        pkgs.append(data)

    return pkgs


def prune_builds(pkgs):
    # Group by package name, version, and build string w/o number
    def group_key(p):
        if p['build'] == str(p['build_number']):
            return (p['name'], p['version'], '')
        else:
            # Strip num from build and only use the first part (e.g., "py37"):
            build_parts = p['build'].rsplit('_', 1)
            return (p['name'], p['version'], build_parts[0])

    pkgs = sorted(pkgs, key=group_key)
    groups = [
        sorted(group, key=lambda pkg: pkg['build_number'], reverse=True)
        for _, group in itertools.groupby(pkgs, group_key)
    ]

    fnames = set()
    for group in groups:
        keep = group[0]['build_number']
        delete = {pkg['filename'] for pkg in group if pkg['build_number'] != keep}
        fnames |= delete

    return fnames


def prune_outdated(pkgs, keep_after, prune_before, filter_fn=lambda p: True):
    """Prune old packages from *pkgs*.

    Keep all packages newer than :class:`datetime.datetime` *keep_after*.
    Delete all packages older than :class:`datetime.datetime` *prune_before*.
    Keep one package per week (as of Monday 00:00) for the time span between
    *prune_before* and *keep_after*.

    Only prune packages for which *filter_fn* returns ``True``.

    Make sure, at least one version of each packages remains.

    """
    # Filter packages
    pkgs = (pkg for pkg in pkgs if filter_fn(pkg))

    # Group by package name and build string w/o number
    def group_key(p):
        if p['build'] == str(p['build_number']):
            return (p['name'], '')
        else:
            # Strip num from build and only use the first part (e.g., "py37"):
            build_parts = p['build'].rsplit('_', 1)
            return (p['name'], build_parts[0])

    pkgs = sorted(pkgs, key=group_key)
    groups = [list(g) for _, g in itertools.groupby(pkgs, group_key)]

    fnames = set()
    for group in groups:
        # Keep at least one pkg!
        group = sorted(group, key=lambda p: p['datetime'])[:-1]

        weeks = {}
        for pkg in group:
            if pkg['datetime'] >= keep_after:
                # Keep!
                continue
            elif pkg['datetime'] < prune_before:
                # Delete!
                fnames.add(pkg['filename'])
            else:
                # Only keep one per week (see below)
                _, week_num, _ = pkg['datetime'].isocalendar()
                weeks.setdefault(week_num, []).append(pkg)

        # Only keep one per week (only applies to internal packages)
        for week_pkgs in weeks.values():
            week_pkgs = week_pkgs[:-1]  # Keep newest per week
            fnames |= {p['filename'] for p in week_pkgs if p['internal']}

    return fnames


def prune_deleted_external(pkgs, recipe_data):
    pkg_names = {recipe['package']['name'] for recipe, _ in recipe_data}
    fnames = set()
    for pkg in pkgs:
        if pkg['internal']:
            continue
        if pkg['name'] not in pkg_names:
            fnames.add(pkg['filename'])
    return fnames


@click.command()
@click.pass_obj
@click.argument(
    'path',
    type=click.Path(exists=True, file_okay=False),
    callback=lambda c, p, v: pathlib.Path(v),
)
@click.option(
    '--force',
    '-f',
    'dry_run',
    flag_value=False,
    default=True,
    help='Actually delete the packagages.',
)
@click.option(
    '--external-recipes',
    type=click_util.Path(exists=True, file_okay=False),
    help=(
        'Path to the external-recipes repository.  If provided, only keep'
        'packages that are listed there.'
    ),
)
@click.option(
    '--prune-internal-before',
    type=click.IntRange(min=0),
    default=6,
    show_default=True,
    help='Delete internal packages older than this amount of months.',
)
@click.option(
    '--prune-external-before',
    type=click.IntRange(min=0),
    default=12,
    show_default=True,
    help='Delete external packages older than this amount of months.',
)
def cli(
        info, path, dry_run, external_recipes, prune_internal_before,
        prune_external_before
):
    """Delete old packages from the local Conda index at PATH.

    By default, the command will only print what would be deleted.  You must
    explicitly pass the "--force" option in order to actually delete packages.

    """
    pkgs = collect_pkgs(path)
    pkgs = sorted(pkgs, key=lambda p: (p['name'], version.parse(p['version']),
                                       p['build_number']))

    to_delete = prune_builds(pkgs)

    now = pendulum.now(tz='UTC')
    keep_after = now.subtract(months=1)
    prune_internal_before = now.subtract(months=prune_internal_before)
    prune_external_before = now.subtract(months=prune_external_before)
    to_delete |= prune_outdated(
        pkgs,
        keep_after=keep_after,
        prune_before=prune_internal_before,
        filter_fn=lambda pkg: pkg['internal'],
    )
    to_delete |= prune_outdated(
        pkgs,
        keep_after=keep_after,
        prune_before=prune_external_before,
        filter_fn=lambda pkg: not pkg['internal'],
    )
    if external_recipes:
        recipe_data = recipes.load_recipes(external_recipes)
        to_delete |= prune_deleted_external(pkgs, recipe_data)

    if dry_run:
        for pkg in pkgs:
            fname = pkg['filename']
            if fname in to_delete:
                click_util.echo(f'Prune {fname}', fg='red')
            else:
                click_util.echo(f'Keep  {fname}', fg='green')
        click_util.info(
            f'{len(to_delete)}/{len(pkgs)} would be deleted.  Pass the "--force" '
            f'option to actually delete them.'
        )
    else:
        for fname in to_delete:
            click_util.status(f'Deleting {fname}')
            os.remove(fname)
        click_util.info(f'Deleted {len(to_delete)}/{len(pkgs)} packages')
        util.run([info.conda_exe, 'index', str(path)])
