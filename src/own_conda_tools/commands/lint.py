"""
Run *pylint* on all Python packages found in the provided directories and
forward pylint's exit code to you.

.. code-block:: console

   $ ownconda lint src/own_conda_tools tests

You can disable certain error classes:

.. code-block:: console

    $ ownconda lint --ignore=warning src/own_conda_tools

"""
import sys

import click

from .. import click_util, constants, util


# All error codes are listed here:
# https://pylint.readthedocs.io/en/latest/user_guide/run.html
#
# The max. error number is 32, so the bitmask for all errors is 64 - 1 = 63:
ALL_ERRORS = 63
IGNORABLE_ERRORS = {
    # 'fatal': 1,
    # 'error': 2,
    'warning': 4,
    'refactor': 8,
    'convention': 16,
    # 'usage': 32,
}


def lint_package(paths, ignore_errors, disable):
    """Lint all modules in *paths*.  Ignore errors in *ignore_errors*."""
    cmd = [
        'pylint',
        f'--rcfile={constants.resource_file("pylintrc")}',
    ]
    cmd += [f'--disable={d}' for d in disable]
    cmd += paths

    retcode = util.run(cmd, check=False).returncode

    bitmask = ALL_ERRORS
    for error in ignore_errors:
        bitmask ^= IGNORABLE_ERRORS[error]

    code = retcode & bitmask
    return code


@click.command()
@click.pass_obj
@click_util.argument.paths()
@click_util.option.recipe_dir()
@click_util.option.python()
@click_util.option.channel(auto_ci_staging=True)
@click_util.option.artifacts_dir()
@click.option(
    '-i',
    '--ignore-error',
    multiple=True,
    type=click.Choice(
        [e for e, _ in sorted(IGNORABLE_ERRORS.items(), key=lambda i: i[1])]
    ),
    help=(
        'Ignore the provided error class (but print its messages nonetheless).  This '
        'option can be used multiple times.'
    ),
)
@click.option(
    '-d',
    '--disable',
    multiple=True,
    metavar='MSG_ID',
    help=(
        'Completely disable the given message or checker (e.g., "-d duplicate-code").  '
        'This option is forwarded to pylint and can be used multiple times.'
    ),
)
def cli(
        info, paths, recipe_dir, python, channel, artifacts_dir, ignore_error,
        disable
):
    """Run pylint for PATHS."""
    # Filter .egg-info directories von editable pip intalls:
    paths = [p for p in paths if not p.endswith('.egg-info')]

    util.install_packages(
        artifacts_dir,
        recipe_dir,
        conda_exe=info.conda_exe,
        platform=info.platform,
        python=python,
        extra_channels=channel,
    )
    code = lint_package(paths, ignore_error, disable)
    sys.exit(code)
