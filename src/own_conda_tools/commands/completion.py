from click._bashcomplete import get_completion_script
import click


@click.command()
@click.pass_context
@click.option(
    '--shell',
    type=click.Choice(['bash', 'zsh']),
    default='bash',
    show_default=True,
    help='Get completion for this shell',
)
def cli(ctx, shell):
    """Print Bash or ZSH completion activation script.

    Put this into your ".bashrc":

    \b
        source <(ownconda completion)

    You can also write the output of this script to a file and then source
    that file in your ".bashrc".  This should improve the startup time for
    new shells:

    \b
        $ ownconda completion > ~/.ownconda-complete.sh
        $ echo 'source $HOME/.ownconda-complete.sh' >> ~/.bashrc

    """
    parent = ctx.parent
    prog_name = parent.info_name
    var = f'_{prog_name.upper().replace("-", "_")}_COMPLETE'
    script = get_completion_script(prog_name, var, shell)
    script = script.replace(
        'local COMPLETION_OPTIONS=""',
        'local COMPLETION_OPTIONS="-o default"',
    )
    script = script.replace(
        'COMPLETION_OPTIONS="-o nosort"',
        'COMPLETION_OPTIONS="-o default -o nosort"',
    )
    click.echo(script)
