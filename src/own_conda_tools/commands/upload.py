"""
Upload all Conda packages found in the provided directory to a Conda index.

.. code-block:: console

   $ ownconda upload artifacts

"""
import os
import pathlib
import urllib.parse

import click
import requests

from .. import click_util, constants, recipes


URL_STABLE = f'{constants.DEFAULT_UPLOAD}/{constants.DEFAULT_CHANNEL}'
URL_STAGING = f'{constants.DEFAULT_UPLOAD}/{constants.STAGING_CHANNEL}'
URL_TESTING = f'{constants.DEFAULT_UPLOAD}/{constants.TESTING_CHANNEL}'
URL_EXPERIMENTAL = f'{constants.DEFAULT_UPLOAD}/{constants.EXPERIMENTAL_CHANNEL}'
DEFAULT_URL = URL_STABLE
if constants.in_pipeline():
    if os.getenv('CI_COMMIT_REF_NAME') == 'master':
        DEFAULT_URL = URL_STABLE
    elif os.getenv('CI_COMMIT_REF_NAME') == 'develop':
        DEFAULT_URL = URL_STAGING
    else:
        DEFAULT_URL = None


def assert_upload_allowed():
    ci_project_url = os.getenv('CI_PROJECT_URL')
    if not ci_project_url:
        return

    conda_recipes = list(recipes.load_recipes(
        pathlib.Path(constants.DEFAULT_RECIPE_DIR)
    ))
    if not conda_recipes:
        # We might be in "external-recipies" or so ...
        return

    url1 = urllib.parse.urlparse(ci_project_url.rstrip('/'))
    # conda recipes is a list of (meta, filename) tuples:
    meta = conda_recipes[0][0]
    url2 = urllib.parse.urlparse(meta['about']['home'].rstrip('/'))
    if url1 != url2:
        raise click.UsageError('It looks like you are trying to upload '
                               'a package from a fork.  This is not '
                               'allowed!')


def find_packages(pkg_dir, platform, pattern):
    """Find files in *pkg_dir* matching *pattern*."""
    path = pkg_dir.joinpath(platform).resolve()
    return path.glob(pattern)


def upload(files, url):
    """Upload *files* to *url*.

    Raise a :exc:`ValueError` if *files* is an empty list.

    """
    if not files:
        raise ValueError('No files selected for uploading')

    with click_util.Action(f'Uploading to {url} ...', ok_message='OK', nl=True):
        for f in files:
            click_util.status(f'- {f}')
            file_obj = f.open('rb')
            reply = requests.post(url, files={'file1': file_obj})
            reply.raise_for_status()


def override_default_url(ctx, param, value):
    """Update the default of the "--url" option."""
    url_param = [p for p in ctx.command.params if p.name == 'url'][0]
    if value:
        if param.name == 'stable':
            url_param.default = URL_STABLE
        elif param.name == 'staging':
            url_param.default = URL_STAGING
        elif param.name == 'testing':
            url_param.default = URL_TESTING
        elif param.name == 'experimental':
            url_param.default = URL_EXPERIMENTAL
        else:
            raise ValueError(f'Invalid value "{value}"')
    return value


@click.command()
@click.pass_obj
@click.option(
    '--pkg-dir',
    default=constants.artifacts_dir(),
    show_default=True,
    type=click_util.Path(exists=True, file_okay=False),
    help='Search packages in this directory.',
)
@click.option(
    '--pkg-filter',
    default='*.tar.bz2',
    show_default=True,
    help=(
        'Only upload packages matching this glob pattern (e.g.: "*-py36_*.tar.bz2" '
        'will only upload Python 3 packages)'
    ),
)
@click_util.option.url(default=DEFAULT_URL)
@click.option(
    '--stable',
    is_flag=True,
    default=False,
    is_eager=True,
    expose_value=False,
    callback=override_default_url,
    help=f'Shortcut for "--url {URL_STABLE}"',
)
@click.option(
    '--staging',
    is_flag=True,
    default=False,
    is_eager=True,
    expose_value=False,
    callback=override_default_url,
    help=f'Shortcut for "--url {URL_STAGING}"',
)
@click.option(
    '--testing',
    is_flag=True,
    default=False,
    is_eager=True,
    expose_value=False,
    callback=override_default_url,
    help=f'Shortcut for "--url {URL_TESTING}"',
)
@click.option(
    '--experimental',
    is_flag=True,
    default=False,
    is_eager=True,
    expose_value=False,
    callback=override_default_url,
    help=f'Shortcut for "--url {URL_EXPERIMENTAL}"',
)
def cli(info, pkg_dir, pkg_filter, url):
    """Upload Conda packages in PKG_DIR."""
    assert_upload_allowed()
    files = find_packages(pkg_dir, info.platform, pkg_filter)
    upload(files, url)
