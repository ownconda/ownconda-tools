"""
Build a project's Sphinx_ documentation and upload it to our docs service.

It ignores the project's :file:`conf.py` and uses a bundled configuration
instead (to ensure common settings and appearance across all projects).

.. code-block:: console

   $ ownconda make-docs

.. _Sphinx: http://www.sphinx-doc.org

To disable the automatic uploading, pass ``--url=None`` – this is the
default when the command is invoked from a non-CI context.

You can set some general sphinx settings (language and intersphinx__
mappings) in the project's :file:`conda/meta.yaml`:

.. code-block:: yaml

    extra:
      sphinx:
        language: de
        intersphinx:
          click: http://click.palletsprojects.com/en/7.x/
          attrs: https://attrs.readthedocs.io/en/stable/

__ http://www.sphinx-doc.org/en/latest/ext/intersphinx.html

"""
import collections
import datetime
import json
import os
import pathlib
import re
import shutil
import subprocess
import sys
import tarfile
import tempfile

import click
import jinja2
import requests

from .. import click_util, constants, git, recipes, util


if os.getenv('CI') == 'true':
    UPLOAD_URL = constants.DEFAULT_DOCS_UPLOAD
    OPEN_DOC = False
else:
    UPLOAD_URL = 'None'
    OPEN_DOC = True


def get_project_meta(recipe_dir):
    # Set GIT_DESCRIBE_*, similarly to Conda
    os.environ.update(git.describe(recipe_dir))

    cwd = pathlib.Path.cwd()
    project_name = os.getenv('CI_PROJECT_NAME', cwd.name)
    group_name = os.getenv('CI_PROJECT_NAMESPACE', cwd.parent.name)
    group_slug = group_name.replace('/', '-')
    slug = f'{group_slug}-{project_name}' if group_slug else project_name

    found_recipes = list(recipes.load_recipes(recipe_dir))
    if len(found_recipes) != 1:
        raise click.UsageError('RECIPE_DIR must contain exactly one recipe.')
    recipe = found_recipes[0][0]
    project = recipe['package']['name']
    release = str(recipe['package']['version'])
    match = re.match(r'(\d+\.\d+)(\.\d+)?', release)
    if not match:
        raise click.ClickException(f'Invalid project version: {release}')
    version = match.group(1)
    year = _get_first_commit_year()

    sphinx = recipe.get('extra', {}).get('sphinx', {})
    return {
        'project_name': project_name,
        'group': group_name,
        'slug': slug,
        'project_title': project,
        'version': version,
        'release': release,
        'year': year,
        'language': sphinx.get('language', 'en'),
        'intersphinx': sphinx.get('intersphinx', {}),
    }


def _get_first_commit_year():
    """Return year of first commit."""
    try:
        # Get date of first commit, take year from it.
        year = subprocess.check_output(
            # %aI works with newer Git versions, but not on centos7
            # 'git log -1 --format=%aI $(git rev-list --max-parents=0 HEAD)',
            'git log -1 --format=%ai $(git rev-list --max-parents=0 HEAD)',
            shell=True,
        ).decode()[:4]
    except subprocess.CalledProcessError:
        year = datetime.date.today().year

    return year


def _get_commit_date() -> str:
    """Return the date of the Git HEAD or the current date in case ``git log``
    fails.

    """
    try:
        date = subprocess.check_output(
            # %aI works with newer Git versions, but not on centos7
            # 'git log -1 --format=format:%aI', shell=True,
            'git log -1 --format=format:%ai', shell=True,
        ).decode()[:10]
    except subprocess.CalledProcessError:
        date = f'{datetime.date.today():%Y-%m-%d}'  # pylint: disable=no-member
    return date


def _get_git_tags():
    """Return a list of all Git tags of the current project.

    """
    try:
        tags = sorted(subprocess.check_output(
            'git tag', shell=True, universal_newlines=True,
        ).splitlines())
    except subprocess.CalledProcessError:
        tags = []
    return tags


def build_docs(project_meta, source, link_check):
    """Run ``sphinx-build`` with the project configuration *project_meta*
    in *source* dir.

    Return the path to the build directory.

    """
    # Copy docs into a temp. directory so that we can overwrite
    # the project's conf.py:
    tmpdir = os.path.join(tempfile.mkdtemp(prefix='docs-'), 'docs')
    with click_util.Action(f'Copying docs into {tmpdir} ...'):
        shutil.copytree(source, tmpdir)
        source = tmpdir

        # Render "conf.py" template and write it to the source directory
        conf_py = open(constants.resource_file('conf.py')).read()
        template = jinja2.Template(conf_py)
        conf_py = template.render(project_meta)
        with open(os.path.join(source, 'conf.py'), 'w') as f:
            f.write(conf_py)

    dest = os.path.join(source, '_build')
    if os.path.isdir(dest):
        shutil.rmtree(dest)
    os.makedirs(dest)

    if link_check:
        _make_link_check(source, dest)

    _make_html(source, dest)

    return dest


def _make_html(source, dest):
    """Run Sphinx to build HTML."""
    cmd = [
        os.path.join(sys.prefix, 'bin', 'sphinx-build'),
        '-b', 'html',
        '-n',
        source,
        os.path.join(dest, 'html'),
    ]
    util.run(cmd)


def _make_link_check(source, dest):
    """Run Sphinx link check."""
    cmd = [
        os.path.join(sys.prefix, 'bin', 'sphinx-build'),
        '--color',
        '-b', 'linkcheck',
        '-n',
        source,
        dest,
    ]
    util.run(cmd, check=False)


def open_docs(build_dir):
    if sys.platform == 'linux':
        util.run(['xdg-open', build_dir])
    else:
        subprocess.run(['open', build_dir])


def pack_docs(project_meta, build_dir):
    """Create a tarball with the html docs from *build_dir* and a ``meta.json``
    file with *project_meta*.

    Return the archive file name.

    """
    archive_name = os.path.join(build_dir, 'docs.tar.gz')
    with click_util.Action('Creating archive ...'):
        html_dir = os.path.join(build_dir, 'html')
        assert os.path.isdir(html_dir)

        # Dump meta data into "html" dir:
        def ruamel_dump(obj):
            """Ruamel.yaml’s CommentedMap/-List are no subclasses of dict/list."""
            if isinstance(obj, collections.Mapping):
                return dict(obj)
            elif isinstance(obj, collections.Iterable):
                return list(obj)
            raise TypeError(f'Cannot encode "{obj}" of type "{type(obj)}"')

        json.dump(
            project_meta,
            open(os.path.join(html_dir, 'meta.json'), 'w'),
            default=ruamel_dump,
        )

        tf = tarfile.open(archive_name, 'w:gz')
        tf.add(html_dir, 'html')
        tf.close()

    return archive_name


def upload_docs(archive_name, url):
    """Upload *archive_name* to *url*."""
    with click_util.Action('Uploading archive ...'), open(archive_name, 'rb') as data:
        reply = requests.post(url, data=data)
        if not reply.status_code == 200:
            raise RuntimeError('Could not upload package')


@click.command()
@click.pass_obj
@click_util.option.recipe_dir()
@click_util.option.python()
@click_util.option.channel(auto_ci_staging=True)
@click_util.option.artifacts_dir()
@click.option(
    '--src',
    type=click.Path(exists=True, file_okay=False),
    default='docs',
    show_default=True,
    help='Source directory of the docs',
)
@click_util.option.url(default=UPLOAD_URL, none_okay=True)
@click.option(
    '--link-check/--no-link-check',
    default=True,
    show_default=True,
    help='Enable or disable the Sphinx link check.',
)
@click.option(
    '--open-doc/--no-open-doc',
    default=OPEN_DOC,
    show_default=True,
    help='Open doc when the build finished',
)
def cli(
        info, recipe_dir, python, channel, artifacts_dir, src, url, link_check, open_doc
):
    """Run sphinx-build and upload generated html documentation"""
    if artifacts_dir.is_dir():
        util.install_packages(
            artifacts_dir,
            recipe_dir,
            conda_exe=info.conda_exe,
            platform=info.platform,
            python=python,
            extra_channels=channel,
            dep_type=recipes.RUN,
        )

    project_meta = get_project_meta(recipe_dir)
    build_dir = build_docs(project_meta, src, link_check)
    if open_doc:
        open_docs(build_dir)
    archive_name = pack_docs(project_meta, build_dir)

    if url is not None:
        upload_docs(archive_name, url)
