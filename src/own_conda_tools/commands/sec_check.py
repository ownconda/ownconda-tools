"""
Run security checks on all Python packages found in the provided directories

.. code-block:: console

   $ ownconda sec-check src/* tests

You can disable certain error classes:

.. code-block:: console

   $ ownconda sec-check -d B321 -d B402 src/* tests

The option ``--list-bandit-tests`` prints all available tests:

.. code-block:: console

   $ ownconda sec-check --list-bandit-tests
   The following tests were discovered:
           B101    assert_used
           ...
           B702    use_of_mako_templates

"""
import sys

import click

from .. import click_util, util


def print_bandit_tests(ctx, _param, value):
    if not value or ctx.resilient_parsing:
        return
    from bandit.core import extension_loader
    mgr = extension_loader.MANAGER
    plugin_info = [f'{a[0]}\t{a[1].name}' for a in mgr.plugins_by_id.items()]
    blacklist_info = []
    for a in mgr.blacklist.items():
        for b in a[1]:
            blacklist_info.append(f'{b["id"]}\t{b["name"]}')

    click_util.echo('The following tests were discovered:')
    plugin_list = '\n\t'.join(sorted(set(plugin_info + blacklist_info)))
    click_util.echo(f'\t{plugin_list}')
    ctx.exit()


def run_bandit(paths, disable):
    """Check all modules in *paths*.  Skip tests in *disable*."""
    cmd = ['bandit', '-ll', '--recursive']
    if disable:
        cmd.append(f'--skip={",".join(disable)}')
    cmd += paths

    retcode = util.run(cmd, check=False).returncode
    return retcode


@click.command()
@click.pass_obj
@click_util.argument.paths()
@click_util.option.recipe_dir()
@click_util.option.python()
@click_util.option.channel(auto_ci_staging=True)
@click_util.option.artifacts_dir()
@click.option(
    '--list-bandit-tests',
    is_flag=True,
    is_eager=True,
    expose_value=False,
    callback=print_bandit_tests,
    help='Print all bandit tests and exit',
)
@click.option(
    '-d',
    '--disable',
    multiple=True,
    metavar='TEST_ID',
    help=(
        'Completely disable the given test (e.g., "-d B402").  This option is similar '
        'to "bandit --skip" but can be used multiple times.'
    ),
)
def cli(info, paths, recipe_dir, python, channel, artifacts_dir, disable):
    """Run some security checks for PATHS."""
    util.install_packages(
        artifacts_dir,
        recipe_dir,
        conda_exe=info.conda_exe,
        platform=info.platform,
        python=python,
        extra_channels=channel,
    )
    code = run_bandit(paths, disable)
    sys.exit(code)
