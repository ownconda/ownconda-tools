"""
Run the build (and optionally the test) stages of a GitLab CI pipeline locally.

There are two types of runners available:

**bash**
  This runner uses the current Conda environment.  It does not need to
  re-install dependencies for each step and is thus very fast.

  On the downside, it only supports a limited set of services because it cannot
  take full advanted of the Docker networking stack.  Mariadb based services
  are ususally working fine.

  The projects *contrib* folder contains the example script
  ``run_local_tests.sh`` that shows how you can create a clean, dedicated Conda
  environment for this runner.

**docker**
  This runner works very similar to a *real* GitLab pipeline or
  :command:`gitlab-runner exec docker`.

  For each step, a new container is created and for each step, all dependencies
  have to be reinstalled (which makes this runner a lot slower than the bash
  runner).

  It works with all kinds of services and can (in contrast to the official
  local GitLab runner) even transfer pipleine artifacts between different
  steps.

By default, this command will only run the pipeline until the build steps are
done.

By passing the ``--with-tests`` options, the command will also run all steps
of the *test* stage if one exists.

You can reduce the amount of output by passing  ``--quiet``.  This will
suppress the output of all sub-commands.  If a sub-command fails,
its output is printed and :command:`ownconda ci` exits.

"""
import contextlib
import functools
import os
import pathlib
import re
import shlex
import socket
import subprocess
import time
import urllib.parse

import click
import gitlab
import ruamel.yaml
import trio

from .. import click_util, git, constants, util


@click.command()
@click.argument(
    'project_root',
    type=click.Path(exists=True, file_okay=False),
    default='.',
)
@click.option(
    '--runner',
    '-r',
    default='bash',
    show_default=True,
    type=click.Choice(['bash', 'docker']),
    help='',
)
@click.option('--git-update', '-g', metavar='GROUP/PROJECT', help='Clone/pull from Git')
@click.option('--git-branch', '-b', metavar='BRANCH', help='Git branch to use')
@click.option('--with-tests', '-t', is_flag=True, default=False, help='Include tests')
@click.option('--quiet', '-q', is_flag=True, default=False, help='Be less verbose')
@click.option('--keep-artifacts', '-k', is_flag=True, default=False,
              help="Don't delete build artifacts")
def cli(
        project_root, runner, git_update, git_branch, with_tests, quiet, keep_artifacts
):
    """Run a GitLab CI pipeline locally.

    Locally run the "build" (and, optionally, "test") steps of a project's
    GitLab CI file (located at "PROJECT_ROOT/.gitlab-ci.yml").

    If "--git-update" is set, clone/pull the latest project version.  It also
    changes the expected location of the CI file to
    "PROJECT_ROOT/GROUP/PROJECT/.gitlab-ci.yaml".

    """
    project_root = pathlib.Path(project_root).resolve()
    if git_update:
        project_root = project_root / git_update

    click_util.status(
        f'Running pipeline for "{project_root}" via {runner} ...',
        fg='magenta',
        bold=True,
    )

    if git_update:
        git.update(get_gitlab_ssh_url(git_update), project_root, branch=git_branch)

    glci = project_root / '.gitlab-ci.yml'
    if not glci.is_file():  # pylint: disable=no-member
        raise click.BadParameter(
            f'Directory "{project_root}" contains no ".gitlab-ci.yml"',
            param_hint='PROJECT_ROOT',
        )

    old_cwd = pathlib.Path.cwd()
    os.chdir(project_root)

    artifacts_dir = str(project_root / 'artifacts')
    steps = parse_gitlab_ci_yaml(glci.read_text(), with_tests)  # pylint: disable=no-member
    runners = {
        'bash': exec_bash,
        'docker': exec_docker,
    }
    assert runner in runners
    try:
        for step in steps:
            exec_step(runners[runner], step, quiet, project_root)
    finally:
        if keep_artifacts:
            if old_cwd != project_root:
                subprocess.run(['rsync', '-a', artifacts_dir, f'{old_cwd}/'])
                subprocess.run(['rm', '-rf', artifacts_dir])
        else:
            subprocess.run(['rm', '-rf', artifacts_dir])


def get_gitlab_ssh_url(project):
    gl = gitlab.Gitlab.from_config(constants.GITLAB_CONFIG_SECTION)
    url = urllib.parse.urlparse(gl.url)
    ssh_url = f'git@{url.netloc}:{project}.git'
    return ssh_url


def parse_gitlab_ci_yaml(yaml, include_tests=False):
    """Parse the *yaml* string of a :file:`.gitlab-ci.yaml` and return a list
    of steps to execute.

    By default, only stops until (and including) the build stage are returned.

    If *include_tests* is ``True``, the list will also return steps of the
    ``test`` stage (if it exists).

    """
    ci = ruamel.yaml.safe_load(yaml)

    if 'build' not in ci['stages']:
        raise ValueError('Pipeline has no build stage!')

    # Collect stages
    collect_until = ci['stages'].index('build')
    if include_tests and 'test' in ci['stages']:
        collect_until = ci['stages'].index('test')

    # stages = {stage: [] for stage in ci['stages'][:collect_until + 1]}
    stages = ci['stages'][:collect_until + 1]
    steps = []
    if 'before_script' in ci:
        steps.append({
            'stage': '',
            'step': 'before_script',
            'script': ci['before_script']
        })

    # Collect steps
    for key, val in ci.items():
        if key.startswith('.'):
            continue

        if 'stage' not in val or val['stage'] not in stages:
            continue
        if 'script' not in val or not val['script']:
            continue
        if not isinstance(val['script'], list):
            val['script'] = [val['script']]
        val['step'] = key
        steps.append(val)

    return steps


def exec_step(runner, step, quiet, project_root):
    """Run all commands of *step*."""
    if runner is exec_docker and not step['stage'] and step['step'] == 'before_script':
        # This skips the :before_script
        return

    strstep = f'{step["stage"]}:{step["step"]}'
    click.secho(strstep, fg='cyan', bold=True)

    result = runner(step, quiet, project_root)
    if result.returncode != 0:
        if quiet:
            click_util.echo('STDOUT:', fg='blue')
            click_util.echo(result.stdout)
            click_util.echo('STDERR:', fg='magenta')
            click_util.echo(result.stderr)
        click_util.fatal_error(f'Pipeline failed for "{project_root}"')


def exec_docker(step, quiet, project_root, artifacts_dirname='artifacts'):
    # In order to transfer build artifacts between different steps, we mount
    # the local/host project directory into docker container.
    # From there, we can copy artifacts to/from the builds directory.
    docker_project_dir = pathlib.Path('/project')
    docker_project_artifacts_dir = docker_project_dir / artifacts_dirname
    docker_build_dir = pathlib.Path('/builds/project-0')
    docker_build_artfacts_dir = docker_build_dir / artifacts_dirname

    # Copy artifacts into container if they exist
    pre_build_script = (
        f'[[ -d {docker_project_artifacts_dir} ]] && '
        f'cp -r {docker_project_artifacts_dir} {docker_build_dir}'
    )

    # Copy build artifacts into project dir and update permissions so that they
    # can be modified on the host (the container runs as root!)
    post_build_script = (
        f'[[ -d {docker_build_artfacts_dir} ]] && '
        f'cp -rf {docker_build_artfacts_dir} {docker_project_dir}; '
        f'chown -R {os.getuid()}:{os.getgid()} {docker_project_artifacts_dir}'
    )

    cmd = [
        'gitlab-runner',
        'exec',
        'docker',
        step['step'],
        '--docker-volumes', f'{project_root}:{docker_project_dir}',
        '--pre-build-script', pre_build_script,
        '--post-build-script', post_build_script,
    ]

    stdout = stderr = subprocess.PIPE if quiet else None
    return subprocess.run(
        cmd, cwd=project_root, stdout=stdout, stderr=stderr, universal_newlines=True
    )


def exec_bash(step, quiet, project_root):
    variables = step.get('variables', {})
    services, variables = collect_services(step.get('services', []), variables)
    with start_services(services, variables):
        env = os.environ.copy()
        env.update(get_gitlab_vars(project_root))
        env.update(variables)
        runfunc = functools.partial(
            run_cmds, step['script'], capture_output=quiet, cwd=project_root, env=env
        )
        result = trio.run(runfunc)
        return result


def collect_services(services, variables):
    """Collect and prepare services from *services* and update *variables*
    with fixed hostnames and ports.

    Return a tuple with a list of service dicts and the updated variables dict.

    """
    mysql_vars = ('MYSQL_HOST', 'MYSQL_PORT', {
        'MYSQL_HOST': 'mariadb',
        'MYSQL_PORT': '3306',
        'DB_URL': 'mysql://root:$MYSQL_ROOT_PASSWORD@$MYSQL_HOST:$MYSQL_PORT/$MYSQL_DATABASE'  # pylint: disable=line-too-long
    })
    keycloak_vars = ('KEYCLOAK_HOST', 'KEYCLOAK_PORT', {
        'KEYCLOAK_HOST': 'keycloak',
        'KEYCLOAK_PORT': '8080',
    })
    override_vars = {
        'mariadb:latest': mysql_vars,
        'forge.services.own/centos7-keycloak:latest': keycloak_vars,
    }

    collected_services = []
    for service in services:
        if isinstance(service, dict):
            image = service['name']
            hostname = service['alias']
        else:
            image = service
            hostname = service.split(':', 1)[0].replace('/', '__')

        try:
            host_var, port_var, update_vars = override_vars[image]
        except KeyError:
            click_util.warning(f'Cannot properly update variables for image "{image}"')
            host_port = container_port = None
        else:
            host_port = str(util.get_unused_tcp_port())
            container_port = update_vars[port_var]
            update_vars[host_var] = '127.0.0.1'
            update_vars[port_var] = host_port
            variables.update(update_vars)

        collected_services.append({
            'image': image,
            'container_name': f'ownconda_ci_service_{hostname}',
            'container_port': container_port,
            'host_port': host_port,
        })
    variables = expandvars(variables)
    return collected_services, variables


def expandvars(variables, keep_unknown=False):
    """Expand environment variables of form $var and ${var} in *text*.

    All escaped variable references (i.e., preceded by backslashes) are
    skipped.

    Unknown variables are replaced by an empty string by default.
    Set *keep_unknown* to ``True`` to leave them unchanged.

    """
    variables = dict(variables)

    def replace_var(m):
        default = m.group(0) if keep_unknown else ''
        return variables.get(m.group(2) or m.group(1), default)

    skip_escaped = r'(?<!\\)'
    re_var = re.compile(skip_escaped + r'\$(\w+|\{([^}]*)\})')

    for k, v in variables.items():
        variables[k] = re_var.sub(replace_var, str(v))

    return variables


@contextlib.contextmanager
def start_services(services, variables):
    """If *step* contains a ``service`` section, yield a command prefix
    that starts a docker container.

    When the context manager exists, stop the started container.

    """
    for service in services:
        start_service(variables=variables, **service)

    try:
        yield
    finally:
        for service in services:
            stop_service(service['container_name'])


def start_service(*, image, container_name, container_port, host_port, variables):
    cmd = ['docker', 'run', '--rm']
    for k, v in variables.items():
        cmd.append(f'--env')
        cmd.append(f'{k}={v}')
    cmd.append('--detach')
    cmd.append(f'--name={container_name}')
    cmd.append(f'--publish={host_port}:{container_port}')
    cmd.append(image)
    click_util.action(f'Starting service "{image}" ...', nl=True)
    util.run(cmd, quiet=True)

    with click_util.Action('Waiting for server ') as action:
        while True:
            try:
                sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                sock.connect(('127.0.0.1', int(host_port)))
            except ConnectionRefusedError:
                action.progress()
                time.sleep(1)
            else:
                sock.close()
                break


def stop_service(container_name):
    with click_util.Action('Stopping container ... '):
        util.run(['docker', 'stop', container_name], quiet=True)


def get_gitlab_vars(project_root):
    env = {
        'CI_PROJECT_DIR': str(project_root),
    }
    try:
        git_stat_out = subprocess.check_output(
            ['git', 'status', '--porcelain=2', '--branch'],
            cwd=project_root,
            stderr=subprocess.PIPE,
        ).decode()
    except subprocess.CalledProcessError:
        # centos7 is just too old
        git_stat_out = subprocess.check_output(
            ['git', 'status', '--porcelain', '--branch'], cwd=project_root
        ).decode()
        git_stat_out = git_stat_out.replace('## ', '# branch.head ')

    for line in git_stat_out.splitlines():
        if line[0] != '#':
            continue
        _, key, val = line.split(' ', 2)
        if key == 'branch.oid':
            env['CI_COMMMIT_SHA'] = val
        elif key == 'branch.head':
            env['CI_COMMIT_REF_NAME'] = val
            env['CI_COMMIT_REF_SLUG'] = slugify(val)
    return env


def slugify(text):
    """Slugify text in GitLab style.

    https://gitlab.com/gitlab-org/gitlab-ce/blob/master/lib/gitlab/utils.rb#L24

    """
    text = text.lower()
    text = re.sub('[^a-z0-9]', '-', text)[:63]
    return re.sub(r'(\A-+|-+\Z)', '', text)


async def run_cmds(commands, *, capture_output=False, **options):
    options['stdin'] = subprocess.PIPE
    options['stdout'] = subprocess.PIPE if capture_output else None
    options['stderr'] = subprocess.PIPE if capture_output else None

    _commands = []
    for cmd in commands:
        cmd_echo = click.style(shlex.quote(f'$ {cmd}'), fg="green", bold=True)
        _commands.append(f'echo -e {cmd_echo}')
        _commands.append(cmd)
    commands_input = ('\n'.join(_commands) + '\n').encode()

    stdout_chunks = []
    stderr_chunks = []

    async with trio.Process(['/bin/bash', '-e'], **options) as proc:
        async def feed_input():
            async with proc.stdin:
                try:
                    await proc.stdin.send_all(commands_input)
                except trio.BrokenResourceError:
                    pass

        async def read_output(stream, chunks):
            async with stream:
                while True:
                    chunk = await stream.receive_some(32768)
                    if not chunk:
                        break
                    chunks.append(chunk)

        async with trio.open_nursery() as nursery:
            if proc.stdin is not None:
                nursery.start_soon(feed_input)
            if proc.stdout is not None:
                nursery.start_soon(read_output, proc.stdout, stdout_chunks)
            if proc.stderr is not None:
                nursery.start_soon(read_output, proc.stderr, stderr_chunks)
            await proc.wait()

    stdout = b''.join(stdout_chunks).decode() if proc.stdout is not None else None
    stderr = b''.join(stderr_chunks).decode() if proc.stderr is not None else None

    return subprocess.CompletedProcess(proc.args, proc.returncode, stdout, stderr)
