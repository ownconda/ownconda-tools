"""
Create dependency graph for all projects in a given directory:

.. code-block:: console

   $ ownconda dep-graph --out=dot ../git-projects/ ../external-recipes/ > deps.dot
   $ dot -Tsvg deps.dot > deps.svg

Create dependency graph for a given project and print them as list in
installation order:

.. code-block:: console

   $ ownconda dep-graph --requirements=scipy -o pkg ../external-recipes
   zlib
   openssl
   sqlite
   python
   openblas
   numpy
   scipy

You can use this to build a certain package and all of its requirements:

.. code-block:: console

  $ ownconda dep-graph ../external-recipes --requirements=scipy -o path | \\
  > xargs ownconda build


You can also ask why a given package is needed:

.. code-block:: console

   $ ownconda dep-graph --why=numpy ../external-recipes
   numpy
   pandas
   scipy
   h5py
   matplotlib

Get all internal dependencies of a project (ignoring third-party packages):

.. code-block:: console

   $ ownconda dep-graph --requirements=own-conda-tools --no-third-party -o dot ./ > deps.dot

You can also explicitly exclude and include packages and define the dependency
type (build, run, test).  See ``ownconda dep-graph --help`` for more
information.

"""
import fnmatch
import functools
import operator
import urllib.parse

import click
import networkx as nx

from .. import click_util, recipes


NODE = {
    'fontcolor': '0 0 .9',
    'fontsize': 18,
    'color': '0 0 0.7',
    'fillcolor': '0 0 0.7',
    'style': 'filled',
}
EDGE = {
    'color': '0 0 0.7',
    # 'style': 'solid',
    'penwidth': 2,
}
FILTERS = [
    # Try top-down and apply all filters that match a package's name:
    # ('own-*', {'color': '0.1 1 .8', 'fillcolor': '.1 1 .8'}),
]
# EXCLUDE_PKGS = {
#     'python',
#     'python >=3.5',
#     'pip',
#     'setuptools',
# }
EXCLUDE_PKGS = set()


def filter_graph(
        graph, *, requirements=None, why=None, implicit=False,
        exclude=set(), include=set(), no_third_party=False
):
    """Filter packages from *graph*.

    Remove all packages that are in the :class:`set` *exclude* and, if
    *no_third_party* is ``True``, that are external packages.  Ignore (don't
    remove) packages that are listed in the :class:`set` *include*.

    If *requirements* and *why* are both ``None``, apply no furth filtering.

    If *requirements* is not ``None``, show only the specified package and
    its direct and indirect requirements.

    If *why* is not ``None``, show only the specified packages and all packages
    that directly or indirectly depend on it.

    You can provide both, *requirements* and *why*, at the same time.  They
    don't need to specify the same package.

    """
    exclude = EXCLUDE_PKGS | set(exclude)
    if no_third_party:
        third_party = {node for node in graph if not node.startswith('own-')}
        exclude |= third_party

    # Resolve exclude and include patterns
    exclude_pkgs = set()
    for pattern in exclude:
        exclude_pkgs.update(fnmatch.filter(graph, pattern))
    include_pkgs = set()
    for pattern in include:
        include_pkgs.update(fnmatch.filter(graph, pattern))

    exclude_pkgs |= {
        node for node in graph if not graph.nodes[node].get('primary', False)
    }

    # Remove re-included pkgs from list and remove remaining nodes from graph
    exclude_pkgs -= include_pkgs
    graph.remove_nodes_from(exclude_pkgs)

    if requirements is not None or why is not None:
        # Only apply this filtering step if *requirements* or *why* are
        # specified.  We than build a list of packages to keep and remove
        # all nodes from the graph that are not in the "keep" set:
        keep = set()
        for pkg_name, func in [(requirements, find_requirements),
                               (why, find_dependents)]:
            if pkg_name is None:
                continue

            try:
                keep |= func(graph, pkg_name, implicit)
            except nx.NetworkXError:
                if pkg_name in exclude_pkgs:
                    msg = 'You have already excluded the package "{}"'
                else:
                    msg = 'The package "{}" does not exist.'
                raise click.BadParameter(msg.format(pkg_name)) from None

        remove = set(graph) - keep
        graph.remove_nodes_from(remove)


def find_requirements(graph, package, implicit):
    """Return a :class:`set` containing *package* and all of its requirements.
    """
    nodes = {package}
    to_visit = [n for n in graph.successors(package) if n not in nodes]
    while to_visit:
        node = to_visit.pop()
        nodes.add(node)

        if implicit:
            children = graph.successors(node)
            to_visit.extend([n for n in children if n not in nodes])

    return nodes


def find_dependents(graph, package, implicit):
    """Return a :class:`set` containing *package* and all of its dependents."""
    nodes = {package}
    to_visit = list(graph.predecessors(package))
    while to_visit:
        node = to_visit.pop()
        nodes.add(node)

        if implicit:
            parents = graph.predecessors(node)
            to_visit.extend([n for n in parents if n not in nodes])

    return nodes


def colorize(graph):
    n_own_nodes = len([n for n, d in graph.node.items() if d.get('primary')])
    i = 0
    for node, node_data in graph.node.items():
        if node_data.get('primary'):
            color = i / n_own_nodes
            style = {
                'color': f'{color} .8 .7',
                'fillcolor': f'{color} .8 .7',
            }
            i += 1
        else:
            style = {}

        for pattern, styles in FILTERS:
            if fnmatch.fnmatch(node, pattern):
                style.update(styles)

        node_data.update(style)


def echo_dot(graph):
    """Echo a *dot* graph for *graph*.

    *print_path* is ignored.  Instead, always the package name is used..

    """
    def attrs(attrs):
        """Format attribute dict for dot graphs."""
        attrs = ','.join(f'{k}="{v}"'
                         for k, v in attrs.items()
                         if v is not None)
        if not attrs:
            return ''

        attrs = f' [{attrs}]'
        return attrs

    click_util.echo('digraph {')
    click_util.echo(f'    node{attrs(NODE)};')
    click_util.echo(f'    edge{attrs(EDGE)};')

    for node, node_data in graph.node.items():
        click_util.echo(f'    "{node}"{attrs(node_data)};')

    for source, target in graph.edges:
        e = {
            'color': graph.node[source].get('color'),
        }
        click_util.echo(f'    "{source}"->"{target}"{attrs(e)};')

    click_util.echo('}')


def echo_pkg(graph):
    """Perform a topological sort on *graph* and print all package names."""
    items = []
    for p in recipes.sort_graph(graph):
        items.append(p)

    for item in items:
        click_util.echo(item)


def echo_path(graph):
    """Perform a topological sort on *graph* and print all project paths."""
    items = []
    for p in recipes.sort_graph(graph):
        item = graph.node[p]['path']
        # Search project root dir (strip recipe dir names):
        for path in [item] + list(item.parents):
            if path.joinpath('.gitlab-ci.yml').is_file():
                item = path
                break
        else:
            continue

        if item not in items:
            items.append(item)

    for item in items:
        click_util.echo(str(item))


def echo_project(graph):
    """Perform a topological sort on *graph* and print all GitLab project names."""
    items = []
    for p in recipes.sort_graph(graph):
        meta = graph.node[p]['meta']
        item = urllib.parse.urlparse(meta['about']['home']).path.lstrip('/')
        if item not in items:
            items.append(item)

    for item in items:
        click_util.echo(item)


OUT_FORMATS = {
    'dot': echo_dot,
    'pkg': echo_pkg,
    'path': echo_path,
    'project': echo_project,
}
DEP_TYPES = {
    'build': recipes.BUILD,
    'run': recipes.RUN,
    'test': recipes.TEST,
    'all': recipes.ALL,
}


@click.command()
@click_util.argument.recipe_root()
@click.option(
    '--dep-type',
    '-t',
    multiple=True,
    type=click.Choice(sorted(DEP_TYPES)),
    default=['all'],
    show_default=True,
    help='Only used dependencies of specified types.',
)
@click.option(
    '--ignore-versions/--no-ignore-versions',
    is_flag=True,
    default=True,
    show_default=True,
    help='Ignore version numbers of dependencies',
)
@click.option(
    '--requirements',
    '-r',
    metavar='PACKAGE',
    help=(
        'Only show the direct requirements of the given package.  '
        'Add "--implicit" to also show implicit dependencies.'
    ),
)
@click.option(
    '--why',
    '-w',
    metavar='PACKAGE',
    help=(
        'Only show the packages that directly depend on the given package.  '
        'Add "--implicit" to also show implicit dependants.'
    ),
)
@click.option(
    '--implicit',
    is_flag=True,
    default=False,
    help=(
        'Used with "-r/--requirements" and "-w/--why".  Also show implicit '
        'dependencies or dependants.'
    ),
)
@click.option(
    '--exclude',
    '-x',
    multiple=True,
    metavar='PATTERN',
    help='Exclude all packages mathing the provided Glob pattern.',
)
@click.option(
    '--include',
    '-i',
    multiple=True,
    metavar='PATTERN',
    help='Re-include previously excluded packages.',
)
@click.option(
    '--no-third-party',
    is_flag=True,
    default=False,
    help="Shortcut to \"-x '*' -i 'own-*'\"",
)
@click.option(
    '--out',
    '-o',
    type=click.Choice(sorted(OUT_FORMATS)),
    default='pkg',
    show_default=True,
    help='Output format.',
)
def cli(recipe_root, dep_type, ignore_versions, requirements, why, implicit,
        exclude, include, no_third_party, out):
    """Create a dependency graph from a number of Conda packages.

    You can specify multiple recipe roots which are all searched recursively
    for recipes.

    """
    recipe_data = recipes.load_recipes(recipe_root)
    dep_type = functools.reduce(operator.or_, (DEP_TYPES[t] for t in dep_type))
    dep_graph = recipes.get_dep_graph(recipe_data, ignore_versions, dep_type)
    filter_graph(
        dep_graph,
        requirements=requirements,
        why=why,
        implicit=implicit,
        exclude=exclude,
        include=include,
        no_third_party=no_third_party,
    )
    colorize(dep_graph)

    try:
        OUT_FORMATS[out](dep_graph)
    except nx.NetworkXException as e:
        click_util.fatal_error(str(e))
