"""
Check if the recipes in one or more folders are valid and contain a minimum
amount of information (like name, version and build number).

.. code-block:: console

   $ ownconda validate-recipes ../external-recipes/
   ../external-recipes/python-django
   Missing keys:
   - source:sha256
   Version cannot be parsed

   ../external-recipes/zlib
   Missing keys:
   - build:number
"""
import os.path
import sys

import click


from .. import click_util, recipes, version


KEYS = [
    'package:name',
    'package:version',
    'build:number',
    'about:home',
    'about:license',
]
SOURCE_KEYS = {
    'url': 'sha256',
    'git_url': 'git_rev',
    'hg_url': 'hg_rev',
}
DSO_WHITELIST = {
    # Use "yum|dnf provides libxy.so.1" to get the providing package
    '*': {
        'ld-linux-x86-64',          # glibc
        'ld-linux-x86-64.so.2',     # glibc
        'libc.so.6',                # glibc
        'libcrypt.so.1',            # glibc
        'libdl.so.2',               # glibc
        'libm.so.6',                # glibc
        'libnsl.so.1',              # glibc
        'libpthread.so.0',          # glibc
        'libresolv.so.2',           # glibc
        'librt.so.1',               # glibc
        'libutil.so.1',             # glibc
        'libgcc_s.so.1',            # libgcc
        'libstdc++.so.6',           # libstdc++
        'libglib-2.0.so.0',         # glib2
        'libgobject-2.0.so.0',      # glib2
        'libgthread-2.0.so.0',      # glib2
    },
    'krb5': {
        'libkeyutils.so.1',         # keyutils (keyutils-libs)
    },
    'mariadb-connector-c': {
        'libcurl.so.4',             # curl (libcurl)
    },
    'openblas': {
        'libgfortran.so.4',         # libgfortran
    },
    'pyqt5': {
        'libGL.so.1',               # mesa-libGL
    },
    'qt5': {
        'libasound.so.2',           # alsa-lib
        'libcups.so.2',             # cups-libs
        'libdbus-1.so.3',           # dbus-libs
        'libexpat.so.1',            # expat
        'libjasper.so.1',           # jasper (jasper-libs)
        'libICE.so.6',              # libICE
        'libSM.so.6',               # libSM
        'libX11-xcb.so.1',          # libX11
        'libXss.so.1',              # libXScrnSaver
        'libXcomposite.so.1',       # libXcomposite
        'libXcursor.so.1',          # libXcursor
        'libXdamage.so.1',          # libXdamage
        'libX11.so.6',              # libXext (libX11)
        'libXext.so.6',             # libXext
        'libXfixes.so.3',           # libXfixes
        'libXi.so.6',               # libXi
        'libXrandr.so.2',           # libXrandr
        'libXrender.so.1',          # libXrender
        'libXtst.so.6',             # libXtst
        'libdrm.so.2',              # libdrm
        'libxcb-glx.so.0',          # libxcb
        'libxcb-xfixes.so.0',       # libxcb
        'libxcb.so.1',              # libxcb
        'libxkbcommon.so.0',        # libxkbcommon
        'libxkbcommon-x11.so.0',    # libxkbcommon-x11
        'libEGL.so.1',              # mesa-libEGL
        'libGL.so.1',               # mesa-libGL
        'libnspr4.so',              # nspr
        'libplc4.so',               # nspr
        'libplds4.so',              # nspr
        'libnss3.so',               # nss
        'libsmime3.so',             # nss
        'libnssutil3.so',           # nss (nss-util)
        'libpci.so.3',              # pciutils-libs
        'libpulse.so.0',            # pulseaudio-libs
        'libpulse-mainloop-glib.so.0',  # pulseaudio-libs-glib2
    },
}


@click.command()
@click_util.argument.recipe_root()
def cli(recipe_root):
    """Check if recipes in RECIPE_ROOT are valid.

    You can specify multiple recipe roots which are all searched recursively
    for recipes.

    """
    recipe_gen = recipes.load_recipes(recipe_root)
    problem_found = validate_recipes(recipe_gen)
    sys.exit(int(problem_found))  # Return "1" if problems were found


def validate_recipes(recipe_data):
    """Validate *recipe_data*.  Return a bool indicating whether problems were
    found or not."""
    problems_found = False
    for recipe, filename in recipe_data:
        if not validate_recipe(recipe, filename):
            problems_found = True

    return problems_found


def validate_recipe(recipe, filename):
    """Validate *recipe* and return ``True`` if it is valid."""
    problems = []
    problems += _check_keys(recipe)
    problems += _check_dso_whitelist(recipe)
    problems += _check_version(recipe)

    if problems:
        click_util.echo(os.path.dirname(filename), bold=True)
        for problem in problems:
            click_util.echo(problem)
        click_util.echo('')
        return False

    return True


def _check_keys(recipe):
    """Check if *recipe* contains all required keys."""
    problems = []
    missing_keys = []
    for key in KEYS:
        if not _keypath_in_dict(key, recipe):
            missing_keys.append(key)

    if 'source' in recipe:
        # "source" can be a dict describing a single source or
        # a list of multiple sources.
        sources = recipe['source']
        if isinstance(sources, dict):
            sources = [sources]
        for i, source in enumerate(sources):
            source_union = set(source) & set(SOURCE_KEYS)
            if len(source_union) != 1:
                problems.append(
                    f'Exactly one of the following keys is allowed per source: '
                    f'{sorted(SOURCE_KEYS)}.  Found: {sorted(source_union)}'
                )
                continue
            missing_keys.extend(_check_source(i, source))

    if missing_keys:
        msg = '\n'.join(['Missing keys:'] + [f'- {mk}' for mk in missing_keys])
        problems.append(msg)

    return problems


def _keypath_in_dict(keypath, dict_):
    sub_keys = keypath.split(':')
    try:
        for key in sub_keys:
            dict_ = dict_[key]
    except KeyError:
        return False

    return True


def _check_source(i, source):
    missing_keys = []
    for stype, key in SOURCE_KEYS.items():
        if stype in source and key not in source:
            missing_keys.append(f'source:[{i}]:{key}')
    return missing_keys


def _check_dso_whitelist(recipe):
    """Check if *recipe*'s DSO whitelist only contains officially approved DSOs."""
    problems = []
    pkg_name = recipe.get('package', {'name': None}).get('name')
    dso_whitelist = DSO_WHITELIST['*'] | DSO_WHITELIST.get(pkg_name, set())
    if 'build' not in recipe:
        return problems

    for dso in recipe['build'].get('missing_dso_whitelist', []):
        dso = os.path.basename(dso)
        if dso not in dso_whitelist:
            problems.append(f'DSO not in whitelist: {dso}')

    return problems


def _check_version(recipe):
    """Check if *recipe* has a valid version."""
    try:
        version.parse(recipe['package']['version'])
        return []
    except (KeyError, TypeError):
        return ['Version cannot be parsed']
