"""
This module contains shared constants and functions that return the paths
to various important locations.

"""
import json
import os
import pathlib
import subprocess
import sys


__all__ = [
    'DEFAULT_RECIPE_DIR',
    'DEFAULT_REPOSITORY', 'DEFAULT_UPLOAD', 'DEFAULT_CHANNEL',
    'STABLE_CHANNEL', 'STAGING_CHANNEL', 'TESTING_CHANNEL', 'EXPERIMENTAL_CHANNEL',
    'PYTHON_VERSION', 'PYTHON_VERSIONS',
    'locate_conda', 'get_conda_info',
    'in_pipeline',
    'artifacts_dir',
    'resource_dir',
    'resource_file',
]


#: Default Conda repository
DEFAULT_REPOSITORY = 'https://forge.services.own/conda'
#: Default URL for uploading Conda packages
DEFAULT_UPLOAD = 'https://forge.services.own/upload'
#: Default URL for uploading Sphinx docs
DEFAULT_DOCS_UPLOAD = 'https://forge.services.own/upload-docs'
#: Config section in ``~/.python-gitlab.cfg` that python-gitlab should read from
GITLAB_CONFIG_SECTION = 'own'

STABLE_CHANNEL = 'stable'
STAGING_CHANNEL = 'staging'
TESTING_CHANNEL = 'testing'
EXPERIMENTAL_CHANNEL = 'experimental'
DEFAULT_CHANNEL = STABLE_CHANNEL

ARTIFACTS_DIR = 'artifacts'  #: Artifacts directory in CI pipelines
DEFAULT_RECIPE_DIR = 'conda'  #: Conda meta.yaml location inside a project

#: Python version of current interpreter
PYTHON_VERSION = '.'.join(map(str, sys.version_info[:2]))
#: Python versions to build packages for.
#  Only modify the set after the "|", rember to use "set()" for ø!
# PYTHON_VERSIONS = sorted({PYTHON_VERSION} | {'3.6', '3.7'})
PYTHON_VERSIONS = sorted({PYTHON_VERSION} | set())


def locate_conda():
    """Return the conda executable from ``$CONDA_EXE``, fall back to
    ``sys.prefix``.

    Raise a :exc:`RuntimeError` if conda cannot be found.

    """
    # Create a list of paths to search
    conda = os.getenv('CONDA_EXE')
    if conda is None:
        conda = os.path.join(sys.prefix, 'bin', 'conda')

    if not os.path.isfile(conda):
        raise RuntimeError(f'Conda not found at {conda}')

    return conda


def get_conda_info(conda_exe):
    """Return the parsed output of ``conda info --json``."""
    out = subprocess.check_output(
        [conda_exe, 'info', '--json'], universal_newlines=True
    ).strip()
    return json.loads(out)


def in_pipeline():
    """Return ``True`` if ``$CI_PROJECT_DIR`` is set, else ``False``."""
    return 'CI_PROJECT_DIR' in os.environ


def in_test():
    """Return ``True`` if the code is being executed by pytest, else ``False``."""
    # This function is monkeypatch by tests/conftest.py to return True
    return False


def artifacts_dir(prefix=None):
    """Return absolute path to a local `artifacts` directory.  If no *prefix*
    is given, use ``$CI_PROJECT_DIR`` as prefix."""
    if not prefix:
        prefix = os.getenv('CI_PROJECT_DIR', '.')

    return pathlib.Path(prefix, ARTIFACTS_DIR).resolve()


def resource_dir():
    """Return the path to the config file directory."""
    return pathlib.Path(__file__).parent / 'resources'


def resource_file(filename):
    """Return the path to the config file *filename* located in
    :func:`resource_dir()`."""
    return resource_dir() / filename
