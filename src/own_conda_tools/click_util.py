"""
Click utilities, options and arguments shared my multiple commands.

"""
import os
import pathlib
import sys
import traceback

import click
import requests

from . import constants


def echo(message, **kwargs):
    """Alias for :func:`click.secho()` with enforced color output in pipelines
    and disabled color output in tests.

    """
    # "in_test()" has a higher priority, so it needs to come first:
    if constants.in_test():
        kwargs.setdefault('color', False)
    if constants.in_pipeline():
        kwargs.setdefault('color', True)
    click.secho(message, **kwargs)


def status(message, **kwargs):
    """Alias for :func:`click.secho()` with ``err=True`` by default."""
    kwargs.setdefault('err', True)
    echo(message, **kwargs)


def action(message, nl=False):
    status(message, nl=nl, bold=True)


def ok(message=' OK'):  # pylint: disable=invalid-name
    status(message, fg='green', bold=True)


def info(message):
    status(message, fg='blue', bold=True)


def warning(message):
    status(message, fg='yellow', bold=True)


def error(message):
    status(message, fg='red', bold=True)


def fatal_error(message):
    error(message)
    sys.exit(1)


class Action:
    """Context manager for executing an action.

    It prints status information and the action's progress.

    Usage::

        >>> with Action('Doing stuff ') as action:
        >>>     do_stuff(1)
        >>>     action.progress()
        >>>     do_stuff(2)
        >>>     action.progress()
        ...
        Doing stuff .. OK
        >>>
        >>> with Action('Doing stuff ...', nl=True) as action:
        >>>     do_stuff(1)
        >>>     action.warning('This is a warning!')
        >>>     do_stuff(2)
        >>>     action.error('This is a non-fatal error!)
        >>>     raise ValueError('onoes')
        Doing stuff ...
         This is a warning!
         This is a non-fatal erro!
         EXCEPTION OCCURED: ValueError('onoes')

    """
    def __init__(self, message, ok_message='OK', nl=False):
        self.message = message
        self.ok_message = f' {ok_message}'
        self.nl = nl
        self.errors = []
        self._suppress_exception = False

    def __enter__(self):
        action(self.message, nl=self.nl)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        if exc_type is None:
            if not self.errors:
                ok(self.ok_message)
            return

        if self._suppress_exception:
            return

        tb = ''.join(traceback.format_exception(exc_type, exc_val, exc_tb))
        fatal_error(f' EXCEPTION OCCURRED:\n{tb}')

    def progress(self):  # pylint: disable=no-self-use
        status('.', nl=False)

    def set_ok_message(self, message):
        self.ok_message = f' {message}'

    def warning(self, message):
        self.errors.append(message)
        warning(f' {message}')

    def error(self, message):
        self.errors.append(message)
        error(f' {message}')

    def fatal_error(self, message):
        self.errors.append(message)
        # Avoid printing "EXCEPTION OCCURRED: -1" on exit:
        self._suppress_exception = True
        fatal_error(f' {message}')


class Path(click.Path):
    """Like :class:`click.Path` but returns a :class:`pathlib.Path'."""
    def convert(self, value, param, ctx):
        path = super().convert(str(value), param, ctx)
        return pathlib.Path(path)


class Enum(click.Choice):
    """*Click* parameter type for representing enums."""
    def __init__(self, enum_type):
        self.__enum = enum_type
        super().__init__(enum_type.__members__)

    def convert(self, value, param, ctx):
        return self.__enum[super().convert(value, param, ctx)]


def _channel_callback(_ctx, _param, value):
    """Validate ``--channel`` values and return a list of channel options.

    *values* is an iterable containg simple channel names (``'staging'``) or
    urls (``file://...'``, ``'https://...'``).

    If a channel does not exist, raise a :exc:`click.BadParameter` exception.

    Return a list with all channels prefixed with ``--channel=`` (e.g.,
    ``'--channel=staging'``).

    """
    if not value:
        return ()

    channels = []
    for channel in value:
        available = True
        if channel.startswith('file://'):
            available = os.path.isdir(channel[len('file://'):])
        else:
            if not channel.startswith(('http://', 'https://')):
                url = f'{constants.DEFAULT_REPOSITORY}/{channel}'
            else:
                url = channel
            try:
                response = requests.get(url)
                response.raise_for_status()
            except requests.RequestException:
                available = False

        if available:
            channels.append(f'--channel={channel}')
        else:
            raise click.BadParameter(f'Channel "{channel}" not available.')

    return tuple(channels)


class argument:  # pylint: disable=invalid-name
    """Namespace for common arguments."""

    @staticmethod
    def paths(**path_kwargs):
        """Argument for an arbitrary number of existing paths (directories or
        files)."""
        return click.argument(
            'paths',
            nargs=-1,
            type=click.Path(exists=True, **path_kwargs),
        )

    @staticmethod
    def recipe_root():
        """Argument for an arbitrary numer of directory names that should
        contain Conda recipes."""
        return click.argument(
            'recipe_root',
            nargs=-1,
            type=Path(exists=True, file_okay=False),
        )


class option:  # pylint: disable=invalid-name
    """Namespace for common options."""

    @staticmethod
    def artifacts_dir():
        """Option for the ``ownconda build`` artifacts directory."""
        return click.option(
            '--artifacts-dir',
            type=click.Path(file_okay=False),
            default=constants.artifacts_dir(),
            show_default=True,
            help='Path to the artifacts dir -- the directory'
                 ' where the "build" command placed the packages).',
        )

    @staticmethod
    def channel(default=None, auto_ci_staging=False):
        """Option for an arbitrary numer of Conda channel names.

        The existence of each channel is tested.

        The default is usually an empty list.  If a GitLab CI pipeline is
        detected and if it runs on a non-master branch, the *staging* channel
        is added as default.

        """
        default = default or []

        env_defaults = os.getenv('CONDA_CHANNELS')
        if env_defaults:
            default = env_defaults.split(',') + default

        if auto_ci_staging and constants.in_pipeline():
            is_not_master = os.getenv('CI_COMMIT_REF_NAME') != 'master'
            is_not_tag = not os.getenv('CI_COMMIT_TAG')
            if is_not_master and is_not_tag:
                default.append('staging')

        # Remove duplicates but keep list in the same order:
        unique_default = []
        for c in default:
            if c not in unique_default:
                unique_default.append(c)
        default = unique_default

        return click.option(
            '--channel',
            '-c',
            default=default,
            show_default=bool(default),
            multiple=True,
            callback=_channel_callback,
            help='Additional Conda channel to use.  Can be used multiple times.',
        )

    @staticmethod
    def dry_run():
        """Return a dry-run boolean flag."""
        return click.option(
            '--dry-run', '-n',
            is_flag=True,
            default=False,
            help='Don’t write the recipe, only print it to stdout.',
        )

    @staticmethod
    def gui():
        """Return a boolean flag for indicating whether a GUI app is being
        built or tested.

        If the flag is set, a command list to run :command:` xvfb` via
        :func:`subprocess.run()` is returned.  If the flag is not set, an empty
        list is returned.  You can prepend this list to the list with the
        actual command to be run.

        """
        def callback(_ctx, _param, value):
            if value:
                # The string '--server-args="-screen 0 AxBxC"' does not work.
                # xvfb-run would extract an argument '"-screen' (with leading
                # ") and refuse to work.  Splitting server-args and its value
                # into two strings works:
                cmd = [
                    'xvfb-run',
                    '-e', '/dev/stderr',
                    '--server-args', '-screen 0 1920x1080x24',
                ]
            else:
                cmd = []
            return cmd

        return click.option(
            '--gui', '-g',
            is_flag=True,
            default=False,
            callback=callback,
            help='Run tests (e.g., of QT apps) in a virtual X server (xvfb)',
        )

    @staticmethod
    def python(multiple=False):
        """Return an option for setting the Python version(s)."""
        default = constants.PYTHON_VERSION
        help_ = 'Compile Python packages against the specified versions.'
        if multiple:
            default = constants.PYTHON_VERSIONS
            help_ += '  Can be used multiple times.'

        return click.option(
            '--python',
            '-p',
            multiple=multiple,
            default=default,
            show_default=True,
            help=help_,
        )

    @staticmethod
    def recipe_dir():
        """Return the path to a single directory containing a Conda recipe."""
        return click.option(
            '--recipe-dir', '-r',
            type=Path(exists=True, file_okay=False),
            default=constants.DEFAULT_RECIPE_DIR,
            show_default=True,
            help='Directory containing the project’s Conda recipe.',
        )

    @staticmethod
    def url(default, none_okay=False):
        """Option for a URL to upload data to (via ``POST``).

        If a ``POST`` request to the provided url fails, raise
        a :exc:`click.BadParameter` exception.

        """
        def callback(_ctx, _param, value):
            if none_okay and value == 'None':
                return None

            try:
                response = requests.post(value)
                response.raise_for_status()
            except requests.RequestException:
                raise click.BadParameter(f'Channel "{value}" not available')

            return value

        if none_okay:
            help_ = 'Upload URL.  Pass "None" to disable uploading.'
        else:
            help_ = 'Upload URL.'
        return click.option(
            '--url',
            default=default,
            show_default=True,
            callback=callback,
            help=help_,
        )
