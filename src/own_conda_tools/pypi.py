"""
Helpers to query PyPI packages.

"""
import configparser
import pathlib
import distutils
import hashlib
import io
import multiprocessing
import os
import re
import runpy
import sys
import tarfile
import tempfile
import zipfile

from pip._vendor.distlib import metadata, wheel
import requests
import setuptools

from . import version


__all__ = [
    'PypiError',
    'get_pkg_info', 'get_latest_version', 'get_latest_release', 'get_release',
]


PYPI_URL = 'https://pypi.org/pypi/{pkg_name}/json'
SDIST_PRIORITIES = {
    '.bz2': 0,
    '.gz': 1,
    '.zip': 2,
}


class PypiError(Exception):
    pass


def get_pkg_info(pkg):
    """Return all data from PyPI for *pkg* as JSON object.

    Raise a :exc:`PypiError` if *pkg* could not be found.

    """
    url = PYPI_URL.format(pkg_name=pkg)
    rep = requests.get(url)
    if rep.status_code != 200:
        raise PypiError(f'Could not fetch data from "{url}"')
    return rep.json()


def get_latest_version(pkg, allow_prerelease=False):
    """Return the latest version of *pkg* as string.

    Ignore pre-releases (like beta versions) by default. Set *allow_prerelease*
    to ``True`` to include them.

    Raise a :exc:`PypiError` if *pkg* could not be found.

    """
    pkg_info = get_pkg_info(pkg)
    latest = _find_latest_release(pkg_info, allow_prerelease)
    return latest


def get_latest_release(pkg, *, allow_prerelease=False, use_wheel=False):
    """Return a dict with information about the latest release of *pkg*.

    See :func:`get_release()` for details.

    """
    pkg_info = get_pkg_info(pkg)
    latest = _find_latest_release(pkg_info, allow_prerelease)
    return get_release(pkg_info, latest, use_wheel)


def get_release(pkg, pkg_version, use_wheel=False):
    """Return a dict with information about the release *pkg_version* of *pkg*.

    By default, source distributions will be looked up.  Set *use_wheel* to
    ``True`` to use a Wheel instead.

    The dict will have the following keys:

    - **version:** the version string of the latest release

    - **fn:** the sdist's filename

    - **url:** the download url for the sdist

    - **sha256:** the sdist's sha256 hash

    - **summary:** short pkg description

    - **home:** url to home page

    - **license:** license name

    *pkg* can be a string with the package name or a package info dict as
    returned by :func:`get_pkg_info()`.

    raise a :exc:`pypierror` if the version does not exist or if no source
    distribution can be found for the package.

    """
    if isinstance(pkg, str):
        pkg_info = get_pkg_info(pkg)
    else:
        pkg_info = pkg

    info = pkg_info['info']
    try:
        release = pkg_info['releases'][pkg_version]
    except KeyError:
        raise PypiError('No release "{pkg_version}" for package "{pkg}" found')

    dist_info = _get_dist_info(info, release, use_wheel)

    return {
        'version': pkg_version,
        'fn': dist_info['filename'],
        'url': dist_info['url'],
        'sha256': dist_info['digests']['sha256'],
        'entry_points': dist_info['entry_points'],
        'build_requirements': dist_info['build_requirements'],
        'run_requirements': dist_info['run_requirements'],
        'packages': dist_info['packages'],
        'summary': info['summary'],
        'home': info['home_page'],
        'license': info['license'],
    }


def _find_latest_release(pkg_info, allow_prerelease):
    """Return the latest stable version from *pkg_info* as string."""
    latest = pkg_info['info']['version']

    if allow_prerelease or not version.parse(latest).is_prerelease:
        # If we allow pre-releases or the latest version is stable,
        # we are done:
        return latest

    # Search list of release for the latest stable release:
    versions = (version.parse(v) for v in pkg_info['releases'])
    versions = (v for v in versions if not v.is_prerelease)
    versions = sorted(versions)
    if not versions:
        raise ValueError(f'No version found for "{pkg_info["info"]["name"]}"')
    return str(versions[-1])


def _get_dist_info(info, release, use_wheel):
    if use_wheel:
        dist_info = _find_wheel(release)
    else:
        dist_info = _find_sdist(release)

    if dist_info is None:
        pkgtype = 'compatible wheel' if use_wheel else 'sdist'
        raise PypiError(f'No {pkgtype} found for "{info["name"]}"')

    try:
        dist_dir, sha256 = _download(dist_info)
    except (zipfile.BadZipFile, tarfile.TarError, RuntimeError) as e:
        raise PypiError(e) from e

    if use_wheel:
        extra_dist_info = _meta_from_wheel(dist_dir, sha256)
    else:
        extra_dist_info = _meta_from_sdist(dist_dir, sha256)
    dist_info.update(extra_dist_info)

    return dist_info


def _find_wheel(release):
    """Find a wheel package in the given *release* dict and return a dict
    with the filename, its download url and a sha256, or ``None``."""
    files = []
    for file in release:
        if (file['packagetype'] == 'bdist_wheel'
                and wheel.Wheel(file['filename']).is_compatible()):
            files.append(file)

    if not files:
        return None
    else:
        # I guess there should only be one compatible wheel
        assert len(files) == 1
        return files[0]


def _find_sdist(release):
    """Find a source package in the given *release* dict and return a dict
    with the filename, its download url and a sha256, or ``None``."""
    candidates = []
    for file in release:
        if file['packagetype'] != 'sdist':
            continue

        _, ext = os.path.splitext(file['filename'])
        if ext not in SDIST_PRIORITIES:
            continue

        priority = SDIST_PRIORITIES[ext]
        candidates.append((priority, file))

    if not candidates:
        return None

    # Sort candidates by priority and return the first one
    return next(candidate for _, candidate in sorted(candidates))


def _download(dist_info):
    # Download archive
    r = requests.get(dist_info['url'])
    if r.status_code != 200:
        return RuntimeError(f'Could not download from {dist_info["url"]}')

    # Verify archive
    sha256 = hashlib.sha256(r.content).hexdigest()
    # if sha256 != dist_info['digests']['sha256']:
    #     raise RuntimeError(f'Checksum {sha256} != '
    #                        f'{dist_info["digests"]["sha256"]}')

    # Extract archive
    tmp = pathlib.Path(tempfile.mkdtemp(prefix='pypi-'))
    fname = pathlib.Path(dist_info['filename'])
    if fname.suffix in {'.zip', '.whl'}:
        archive = zipfile.ZipFile(io.BytesIO(r.content))
    else:
        assert fname.suffixes[-2] == '.tar'
        assert fname.suffixes[-1] in SDIST_PRIORITIES
        archive = tarfile.open(mode='r', fileobj=io.BytesIO(r.content))

    archive.extractall(tmp)
    subdirs = list(tmp.iterdir())
    if len(subdirs) == 1 and subdirs[0].is_dir():
        dist_dir = subdirs[0]
    else:
        dist_dir = tmp

    # The extracted dist is in the only sub-directory of "tmp":
    return dist_dir, sha256


def _meta_from_wheel(dist_dir, sha256):
    dist_info_dir = next(dist_dir.glob('*.dist-info'))
    metadata_file = dist_info_dir / 'METADATA'
    assert metadata_file.is_file()

    m = metadata.Metadata(str(metadata_file))
    run_requirements = _get_requirements(m, 'run')
    build_requirements = _get_requirements(m, 'build')

    ep_file = dist_info_dir / 'entry_points.txt'
    if ep_file.is_file():
        entry_points = _get_entry_points_from_ini(ep_file.read_text())
    else:
        entry_points = []

    rel_paths = (entry.relative_to(dist_dir) for entry in dist_dir.rglob('*.py'))
    no_suffix = (entry.with_suffix('') for entry in rel_paths)
    packages = sorted(
        '.'.join(entry.parent.parts if entry.name == '__init__' else entry.parts)
        for entry in no_suffix
    )
    return {
        'run_requirements': run_requirements,
        'build_requirements': build_requirements,
        'entry_points': entry_points,
        'packages': packages,
        'digests': {'sha256': sha256},
    }


def _get_requirements(md, rtype):
    reqs = md.get_requirements(getattr(md, f'{rtype}_requires'))
    reqs = [r.replace('(', '').replace(')', '') for r in reqs]
    return reqs


def _meta_from_sdist(dist_dir, sha256):
    # We need to run setup.py in a subprocess to avoid problems with imports
    # and other nasty stuff that might happen in a setup.py:
    result_queue = multiprocessing.Queue()
    proc = multiprocessing.Process(target=_task_run_setup_py,
                                   args=(dist_dir, result_queue))
    proc.start()
    proc.join()
    result = result_queue.get()
    if isinstance(result, Exception):
        raise result
    result['digests'] = {'sha256': sha256}
    return result


def _task_run_setup_py(dist_dir, result_queue):
    """Task performed by :func:`_extend_pkginfo()`."""
    # Change to "dist_dir" and add it to our path
    try:
        setup_kwargs = _extract_from_setuppy(dist_dir)

        regex = re.compile(r'(.*?) *(~=|==|!=|<=|>=|<|>) *(.*)')

        setup_requires = setup_kwargs.get('setup_requires', [])
        setup_requires = [regex.sub(r'\1 \2\3', r.lower())
                          for r in setup_requires]

        install_requires = setup_kwargs.get('install_requires', [])
        install_requires = [regex.sub(r'\1 \2\3', r.lower())
                            for r in install_requires]

        packages = setup_kwargs.get('packages', [])

        entry_points = _get_entry_points(setup_kwargs)

        # Update candidate
        extra_info = {
            # Something went wrong if we could not extract package names
            'build_requirements': sorted(setup_requires),
            'run_requirements': sorted(install_requires),
            'entry_points': sorted(entry_points),
            'packages': sorted(packages),
        }
        result_queue.put(extra_info)
    except Exception as e:  # pylint: disable=broad-except
        result_queue.put(e)


def _extract_from_setuppy(dist_dir):
    """Actual task to separate handling the task's return value and the
    *result_queue*."""
    setup_kwargs = {}

    # Patch setuptools and import/run setup.py
    def mock_setup(*_, **kw):
        setup_kwargs.update(kw)

    # No need to use unittest.mock.  We are in a subprocess here.
    distutils.setup = mock_setup
    setuptools.setup = mock_setup
    sys.path.insert(0, str(dist_dir))
    cwd = os.getcwd()
    os.chdir(dist_dir)
    try:
        # "runpy" is more reliable in practice but fails during
        # testing, so we try a normal import first:
        import setup  # flake8: noqa; pylint: disable=import-error,unused-variable,unused-import
        if not setup_kwargs:
            runpy._run_module_as_main('setup')  # pylint: disable=protected-access
    except:  # pylint: disable=bare-except
        # All kind of stuff might go wrong when trying to import a setup.py ...
        pass
    finally:
        os.chdir(cwd)

    return setup_kwargs


def _get_entry_points(setup_kwargs):
    """Extract entry points from dict *setup_kwargs*.

    Entry points can either be sotred in a dict or in a ini-Style multiline
    string.

    """
    if 'entry_points' not in setup_kwargs:
        return []

    raw_ep = setup_kwargs['entry_points']
    if isinstance(raw_ep, dict):
        entry_points = raw_ep.get('console_scripts', [])
    else:
        entry_points = _get_entry_points_from_ini(raw_ep)

    return entry_points


def _get_entry_points_from_ini(text):
    """Extract entry points from an *ini* style config *text*."""
    cp = configparser.ConfigParser()
    cp.read_string(text)
    cs = cp['console_scripts'] if 'console_scripts' in cp else {}
    entry_points = [f'{k} = {v}' for k, v in cs.items()]
    return entry_points
