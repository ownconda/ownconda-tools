import io
import os
import pathlib

import jinja2
import networkx as nx
import ruamel.yaml


__all__ = [
    'BUILD', 'RUN', 'TEST', 'ALL',
    'load_recipes', 'get_dep_graph',
]


BUILD = 1  #: Build dependencies
RUN = 2  #: Run-time dependencies
TEST = 4  #: Test dependencies
ALL = BUILD | RUN | TEST  #: All kinds of dependencies


def load_recipes(paths):
    """Return a generator that yields tuples of loaded Conda recipes and their
    respective filesnames from *path*.

    *path* can be a single path or multiple paths separated by ``:``.

    Raises a :exc:`ruamel.yaml.parser.ParserError` if a `meta.yaml` cannot be
    parsed.

    """
    if isinstance(paths, pathlib.Path):
        paths = [paths]
    for file in _collect_meta_yaml(paths):
        yield (load_yaml(file.read_text()), file)


def get_dep_graph(recipes, ignore_versions, collect=ALL):
    """Return a :class:`networkx.DiGraph` for all recipes found in *root_dir*.

    If *ignore_versions* is ``True`` ignore the version specs in the
    requirements lists.

    By default, *build*, *run*, and *test* dependencies are collect.  You can
    change that by passing :data:`BUILD`, :data:`RUN`, :data:`TEST` or any
    logical-or-combination of them (e.g., `BUILD | RUN`) to the *collect*
    argument.

    """
    dep_graph = nx.DiGraph()
    for meta, path in recipes:
        pkg = meta['package']['name']
        # *primary* packages are the packages we actively collected:
        dep_graph.add_node(pkg, primary=True, path=path.parent, meta=meta)

        deps = read_deps(meta, collect)
        if ignore_versions:
            # Versions in meta.yaml: "pkg_name >=x.y"
            deps = [d.split(' ', 1)[0] for d in deps]
        dep_graph.add_edges_from((pkg, dep) for dep in deps)

    return dep_graph


def sort_graph(graph):
    """Return the nodes of *graph* as sorted list.

    Dependencies come before depending packages.

    """
    try:
        return list(reversed(list(nx.topological_sort(graph))))
    except nx.NetworkXUnfeasible:
        msg = ['Graph contains one or more cycles:']
        for cycle in nx.simple_cycles(graph):
            msg.append(f'- {cycle}')
        # raise type(e)('\n'.join(msg)) from None
        raise nx.NetworkXUnfeasible('\n'.join(msg))


def load_yaml(meta_yaml):
    """Load the string *meta_yaml* and return its parsed contents."""
    template = jinja2.Template(meta_yaml)
    yaml = ruamel.yaml.YAML(typ='rt')  # rt is also safe
    meta = yaml.load(template.render(environ=os.environ))  # nosec
    return meta


def dump_yaml(meta_yaml):
    """Dump the dict *meta_yaml* to a YAML string and return it."""
    out = io.StringIO()
    yaml = ruamel.yaml.YAML(typ='rt')
    yaml.block_seq_indent = 2
    yaml.dump(meta_yaml, out)
    return out.getvalue()


def read_deps(meta, dep_type):
    """Read dependencies from a parsed meta.yaml *meta* belonging to
    the specified dependency type *dep_type*.

    *dep_type* can be :data:`BUILD`, :data:`RUN`, :data:`TEST`, :data:`ALL` or
    any bit-wise combination of them.

    Return a set of all matching dependencies.

    """
    deps = set()

    if dep_type & BUILD == BUILD:
        try:
            deps |= set(meta['requirements']['build'])
        except (KeyError, TypeError):
            pass

    if dep_type & RUN == RUN:
        try:
            deps |= set(meta['requirements']['run'])
        except (KeyError, TypeError):
            pass

    if dep_type & TEST == TEST:
        try:
            deps |= set(meta['test']['requires'])
        except (KeyError, TypeError):
            pass

    return deps


def _collect_meta_yaml(paths):
    """Recursively find ``meta.yaml`` files in all *paths* and return them
    as a sorted list of path names."""
    files = []
    for p in paths:
        for currentdir, _, filenames in os.walk(p):
            if 'meta.yaml' in filenames:
                files.append(pathlib.Path(currentdir, 'meta.yaml').resolve())

    return sorted(files)
