"""
Functions for version parsing and formatting.

"""
from packaging.version import parse


__all__ = ['parse', 'format_version', 'version_tuple']


def format_version(pkg):
    """Return a string "version-buildnum" for *pkg*."""
    version, build_num = _version_tuple(pkg)
    return f'{version}-{build_num}'


def version_tuple(pkg):
    """Make a tuple *(version, build_number)* from *pkg*.

    *pkg* can be recipe meta data or package data from a Conda repo.

    """
    v, b = _version_tuple(pkg)
    v = parse(v)
    return (v, b)


def _version_tuple(data):
    """Extract version and build number von *data*.

    *data* can either be the contents of a ``meta.yaml`` or the package
    meta data from a Conda repo/channel (``repodata.json``).

    """
    try:
        # Try meta.yaml format
        v, b = data['package']['version'], data['build'].get('number', 0)
    except KeyError:
        # Try Conda repo format
        v, b = data['version'], data['build_number']

    return v, b
