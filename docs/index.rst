============================================
Welcome to the ownconda-tools documentation!
============================================

Support tools for local development, CI/CD and Conda packaging.

Contents:

.. toctree::
   :maxdepth: 2

   install
   commands


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
