import os
import datetime

# -- General configuration ------------------------------------------------
extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.viewcode',
]

templates_path = ['_templates']

source_suffix = '.rst'
master_doc = 'index'
language = 'en'

slug = 'ownconda-tools'
project = 'ownconda-tools'
version = os.environ.get('SPHINX_VERSION', '')
release = 'v{} ({}) – status: internal'.format(
    os.environ.get('SPHINX_RELEASE', ''),
    os.getenv("GIT_DESCRIBE_HASH", ''),
)
author = 'Stefan Scherfke'
date = datetime.date.today()
today_fmt = '%Y-%m-%d'
copyright = f'2016 {author} – {release} – {date:{today_fmt}}'

exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']
pygments_style = 'sphinx'
todo_include_todos = False


# -- Options for HTML output ----------------------------------------------

import sphinx_rtd_theme
html_static_path = ['_static']
html_theme_path = [sphinx_rtd_theme.get_html_theme_path()]
html_theme = 'sphinx_rtd_theme'
html_theme_options = {
    'display_version': False,
}


# -- Options for LaTeX output ---------------------------------------------

latex_engine = 'lualatex'
latex_elements = {
    'papersize': 'a4paper',
    'pointsize': '11pt',
    'preamble': '',
    'figure_align': 'htbp',
}

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title,
#  author, documentclass [howto, manual, or own class]).
latex_documents = [
    (
        master_doc,
        f'{slug}.tex',
        project,
        author.replace('&', '\\&'),
        'manual',
    ),
]


# -- Plug-ins -------------------------------------------------------------

# Intersphinx
intersphinx_mapping = {
    'python': ('https://docs.python.org/3/', None),
    'click': ('http://click.pocoo.org/6/', None),
}

# Autodoc
autodoc_member_order = 'bysource'
