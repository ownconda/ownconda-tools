======================
Installation and Setup
======================


Installation
============

The *ownconda-tools* should be installed into the root Conda env:

.. code-block:: console

   $ conda install -n root own-conda-tools


Bash completion
===============

To enable Bash completion, copy the Script `ownconda-complete.sh`__ from the
repository to your machine (e.g., into your :file:`$HOME`) and source it from
your :file:`.bashrc`, e.g.:

.. code-block:: bash

   source $HOME/ownconda-complete.sh

__ https://gitlab.com/ownconda/ownconda-tools/blob/master/ownconda-complete.sh


.. _configure-gitlab-access:

Configure GitLab Access
=======================

The :ref:`cmd-gitlab` commands acces GitLab via the `python-gitlab API`__ which
you need to configure first.

__ https://python-gitlab.readthedocs.io/

#. Create an access token for GitLab:

   - Go to `User Settings / Access Tokens`__.

   - Enter a name for your token (e.g., *python-gitlab*).

   - An expiry date is not necessary.

   - Check the scope :guilabel:`api`.

   - Click :guilabel:`Create personal access token` and copy your token.

   .. warning::

      You can only view this token now.  If you forget to copy it and close the
      page, your token is gone forever and you need to create a new one.

   __ https://gitlab.com/profile/personal_access_tokens

#. Create a file :file:`~/.python-gitlab.cfg` with the following configuration:

   .. code-block:: ini

      [global]
      default = own
      ssl_verify = True

      [own]
      url = https://gitlab.com
      private_token = your-access-token
      api_version = 4
