==============================
Commands for local development
==============================

``develop`` – Install a package in editable/development mode
============================================================

.. automodule:: own_conda_tools.commands.develop


``pylintrc`` – Print the built-in *pylintrc* to *stdout*
========================================================

.. automodule:: own_conda_tools.commands.pylintrc


.. _cmd-gitlab:

``gitlab`` – Update or modifiy multiple GitLab projectes at once
================================================================

.. automodule:: own_conda_tools.commands.gitlab

==========================================
Commands for recipe and package management
==========================================

``pypi-recipe`` – Create or update recipes for PyPI packages
============================================================

.. automodule:: own_conda_tools.commands.pypi_recipe


``check-for-updates`` – Check if updates are available for external packages
============================================================================

.. automodule:: own_conda_tools.commands.check_for_updates


``update-recipes`` – Update one or more recipes
===============================================

.. automodule:: own_conda_tools.commands.update_recipes


``dep-graph`` – Create a dependency graph for a number of recipes
=================================================================

.. automodule:: own_conda_tools.commands.dep_graph


``validate-recipes`` – Check if recipes in folder are valid
===========================================================

.. automodule:: own_conda_tools.commands.validate_recipes


``show-updated-recipes`` – List recipes that are new than packages in our index
===============================================================================

.. automodule:: own_conda_tools.commands.show_updated_recipes


``prune-index`` – Delete old packages from the local Conda index at PATH
========================================================================

.. automodule:: own_conda_tools.commands.prune_index


===============================
Commands for the CI/CD pipeline
===============================

``build`` – Build a number of recipes in the right order.
=========================================================

.. automodule:: own_conda_tools.commands.build


``test`` – Wrapper for our test runner
======================================

.. automodule:: own_conda_tools.commands.test


``lint`` – Wrapper for our linter
=================================

.. automodule:: own_conda_tools.commands.lint


``sec-check`` – Run some security checks
========================================

.. automodule:: own_conda_tools.commands.sec_check


``upload`` – Upload Conda packages to the Conda repository
==========================================================

.. automodule:: own_conda_tools.commands.upload


``make-docs`` – Build and upload Sphinx documentation
=====================================================

.. automodule:: own_conda_tools.commands.make_docs
