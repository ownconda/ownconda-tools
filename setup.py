import os

from setuptools import setup, find_packages

#
# NOTE: Package meta data has to be sync'ed manually with conda/meta.yaml!
#
setup(
    name='own-conda-tools',
    version='{}.{}'.format(
        os.getenv('GIT_DESCRIBE_TAG', 0),
        os.getenv('GIT_DESCRIBE_NUMBER', 0),
    ),
    description='Utilities for creating and maintaining our Conda packages',
    url='https://gitlab.com/ownconda/ownconda-tools',
    author='Stefan Scherfke',
    author_email='stefan@sofa-rockers.org',
    install_requires=[
        'attrs',
        'click',
        'trio',
        # Recipe and dependency management
        'beautifulsoup4',
        'jinja2',
        'networkx',
        'packaging',
        'pip',
        'requests',
        'ruamel.yaml',
        'setuptools',
        # Testing
        'pytest',
        'pytest-cov',
        'pytest-sugar',
        # Linting
        'astroid',
        'pylint',
        # Sec-Check
        'bandit',
        # Docs
        'sphinx',
        'sphinx-rtd-theme',
        # GitLab
        'python-gitlab',
        # Prune
        'pendulum',
    ],
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'ownconda = own_conda_tools.__main__:cli',
        ],
    },
    classifiers=[
        'Private :: Do Not Upload',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3 :: Only',
    ],
)
